// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/XiGameInstance.h"

bool UXiGameInstance::GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	if (WeaponInfoTable)
	{
		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(NameWeapon, "", false);
		if (WeaponInfoRow)
		{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetWeaponInfoByName - WeaponTable -Null"));
	}

	return bIsFind;
}

bool UXiGameInstance::GetForciblyWeaponInfoByWeaponType(EWeaponType TypeWeapon, FWeaponInfo& OutInfo, FName& OutWeaponName)
{
	bool bIsFind = false;
	FWeaponInfo* WeaponInfoRow;
	if (WeaponInfoTable)
	{
		TArray<FName> RowTableNames;
		RowTableNames = WeaponInfoTable->GetRowNames();
		int32 j = 0;
		while (bIsFind)
		{

		WeaponInfoRow = WeaponInfoTable->FindRow<FWeaponInfo>(RowTableNames[j], "", false);
		if (WeaponInfoRow)
		{
			if (WeaponInfoRow->WeaponType == TypeWeapon)
			{
			bIsFind = true;
			OutInfo = *WeaponInfoRow;
			OutWeaponName = RowTableNames[j];
			}
		}
			j++;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetForciblyWeaponInfoByWeaponType - WeaponTable -Null"));
	}

	return bIsFind;
}

bool UXiGameInstance::GetDropItemInfoByWeaponName(FName WeaponName, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		TArray<FName>RowNames = DropItemInfoTable->GetRowNames();

		int8 i = 0;
		while (i < RowNames.Num() && !bIsFind)
		{
			DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(RowNames[i], "");
			if (DropItemInfoRow->WeaponInfo.NameItem == WeaponName)
			{
				OutInfo = (*DropItemInfoRow);
				bIsFind = true;
			}
			i++;//fix
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetDropItemInfoByWeaponName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}

bool UXiGameInstance::GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo)
{
	bool bIsFind = false;

	if (DropItemInfoTable)
	{
		FDropItem* DropItemInfoRow;
		DropItemInfoRow = DropItemInfoTable->FindRow<FDropItem>(NameItem, "");
		if (DropItemInfoRow)
		{
			OutInfo = (*DropItemInfoRow);
			bIsFind = true;
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetDropItemInfoByName - DropItemInfoTable -NULL"));
	}

	return bIsFind;
}

bool UXiGameInstance::GetWeaponModulesNumber(int32& WeaponModulesNumber)
{
	bool result = false;

	if (WeaponModulesInfoTable)
	{
		int32 NumberTableWeaponModules = 0;
		NumberTableWeaponModules = WeaponModulesInfoTable->GetRowNames().Num();
		
		WeaponModulesNumber = NumberTableWeaponModules;
		result = true;
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetWeaponModulesNumber - WeaponModulesInfoTable -NULL"));
	}

	return result;
}

bool UXiGameInstance::GetAllWeaponModules(TArray<FWeaponModulesInfo>& AllWeaponModules)
{
	bool result = true;
	TArray<FWeaponModulesInfo*> CopyAllWeaponModules;
	if (WeaponModulesInfoTable)
	{
		WeaponModulesInfoTable->GetAllRows<FWeaponModulesInfo>("", CopyAllWeaponModules);
		
		if (AllWeaponModules.IsValidIndex(0))
		{
			int8 j = 0;
			for (j = 0; j < AllWeaponModules.Num(); j++)
			{
				if (CopyAllWeaponModules.IsValidIndex(j))
				{
					AllWeaponModules[j] = *CopyAllWeaponModules[j];
				}
			}
		}
		
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiGameInstance::GetAllWeaponModules - WeaponModulesInfoTable -NULL"));
		result = false;
	}

	return result;
}
