// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "InputUserSettings.generated.h"

UENUM(BlueprintType)
enum class EInputType : uint8
{
	EIT_MouseSensitivity		UMETA(DisplayName = "Mouse Sensitivity"),
	EIT_SystemMouse		UMETA(DisplayName = "System Mouse"),
	EIT_MovableAimCursor UMETA(DisplayName = "Movable Aim Cursor"),
};

USTRUCT(BlueprintType)
struct FCharacterMouseInputSettings
{
	GENERATED_USTRUCT_BODY()


		/** ���������������� ���� */
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MouseInput")
		float MouseCursorSensitivity = 10.f;

	/** ����������� ������ ������������ � Aim */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MouseInput")
		bool bIsOffsetOpticalAimCursor = true;

	/** ��������� ���������������� ���� */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MouseInput")
		bool bSystemMouseSensitivity = false;

};

/**
 * 
 */
UCLASS(config = GameUserSettings, configdonotcheckdefaults)
class XI_API UInputUserSettings : public UObject
{
	GENERATED_UCLASS_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		static UInputUserSettings* GetInputUserSettings();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		void SetDefaultAllInput();

	static UInputUserSettings* DirtyMainInputUserSettings;

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		virtual void SaveSettings();

	/** Loads the user settings from persistent storage */
	UFUNCTION(BlueprintCallable, Category = InputSettings)
		virtual void LoadSettings(bool bForceReload = false);

	/** Applies all current user settings to the game and saves to permanent storage (e.g. file), optionally checking for command line overrides. */
	UFUNCTION(BlueprintCallable, Category = InputSettings)
		virtual void ApplySettings();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		FCharacterMouseInputSettings GetNewInputSetttings();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		float GetMouseSensitivity();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		bool GetIsOffsetOpticalAimCursor();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		bool GetIsSystemMouseSensitivity();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		void SetMouseSensitivity(float NewMouseSensitivity);

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		void SetIsOffsetOpticalAimCursor(bool bIsOffsetOpticalAim);

	UFUNCTION(BlueprintCallable, Category = InputSettings)
		void SetIsSystemMouseSensitivity(bool bIsSystemMouse);

protected:

	virtual void BeginDestroy() override;

	/** ���������������� ���� */
	UPROPERTY(config)
		float MouseCursorSensitivity;

	/** ����������� ������ ������������ � Aim */
	UPROPERTY(config)
		bool bIsOffsetOpticalAimCursor;

	/** ��������� ���������������� ���� */
	UPROPERTY(config)
		bool bSystemMouseSensitivity;

};
