// Copyright Epic Games, Inc. All Rights Reserved.

#include "../Game/XiGameMode.h"
#include "XiPlayerController.h"
#include "../Character/XiCharacter.h"
#include "UObject/ConstructorHelpers.h"

AXiGameMode::AXiGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AXiPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprint/Character/BP_Character"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}