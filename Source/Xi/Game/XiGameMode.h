// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "XiGameMode.generated.h"

UCLASS(minimalapi)
class AXiGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	AXiGameMode();
};



