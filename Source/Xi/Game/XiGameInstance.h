// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"

#include "../FuncLibrary/Types.h"
#include "Engine/DataTable.h"
#include "../Weapon/WeaponDefault.h"

#include "XiGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class XI_API UXiGameInstance : public UGameInstance
{
	GENERATED_BODY()
	
public:
	//table
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	UDataTable* WeaponInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = " WeaponSetting ")
	UDataTable* DropItemInfoTable = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSetting")
	UDataTable* WeaponModulesInfoTable = nullptr;
	UFUNCTION(BlueprintCallable)
	bool GetWeaponInfoByName(FName NameWeapon, FWeaponInfo& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetForciblyWeaponInfoByWeaponType(EWeaponType TypeWeapon, FWeaponInfo& OutInfo, FName& OutWeaponName); //Return first found weapon
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByWeaponName(FName WeaponName, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetDropItemInfoByName(FName NameItem, FDropItem& OutInfo);
	UFUNCTION(BlueprintCallable)
	bool GetWeaponModulesNumber(int32& WeaponModulesNumber);
	UFUNCTION(BlueprintCallable)
	bool GetAllWeaponModules(TArray<FWeaponModulesInfo>& AllWeaponModules);
};
