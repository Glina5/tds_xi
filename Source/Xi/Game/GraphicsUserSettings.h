// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameUserSettings.h"
#include "UObject/NoExportTypes.h"
#include "GenericPlatform/GenericApplication.h"

#include "GraphicsUserSettings.generated.h"

USTRUCT(BlueprintType)
struct FMyWindowPosition
{
	GENERATED_USTRUCT_BODY()
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 X_Position = -1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 Y_Position = -1;
};

USTRUCT(BlueprintType)
struct FMyMonitorInfo
{
	GENERATED_USTRUCT_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorIndex = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		FString MonitorName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		FString MonitorID;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorNativeWidth;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorNativeHeight;
	FPlatformRect MonitorDisplayRect;
	FPlatformRect MonitorWorkArea;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorWorkAreaLeft;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorWorkAreaTop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		bool bIsPrimaryMonitor;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MonitorSettings")
		int32 MonitorDPI = 0;
};

/**
 * 
 */
UCLASS(config = GameUserSettings, configdonotcheckdefaults)
class XI_API UGraphicsUserSettings : public UGameUserSettings
{
	
	GENERATED_UCLASS_BODY()
	// GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable, Category = GraphicsSettings)
		static UGraphicsUserSettings* GetGraphicsUserSettings();

//	virtual void SetToDefaults() override;
	UFUNCTION(BlueprintCallable, Category = GraphicsSettings)
	void SetGraphicsToDefault();

	static UGraphicsUserSettings* DirtyMainGraphicsSettings;

	UFUNCTION(BlueprintCallable, Category = GraphicsSettings)
		TArray<FMyMonitorInfo> GetMyAttachedMonitorInfo();

	UFUNCTION(BlueprintCallable, Category = GraphicsSettings)
		int32 SetSelectedMonitor(int32 MonitorIndex, FString MonitorName);

	UFUNCTION(BlueprintPure, Category = GraphicsSettings)
	int32 GetCurrentMonitorIndex();

	UFUNCTION(BlueprintPure, Category = GraphicsSettings)
	FString GetCurrentMonitorName();

	UFUNCTION(BlueprintPure, Category = GraphicsSettings)
	FMyWindowPosition GetCurrentWindowPosition();


	void GetMonitorsFullName(TArray<FString>& OutMonitorName);


private:

	void ApplyWindowPosition(int32 MonitorIndex, FString MonitorName);

//	FDelegateHandle DelegateHandleViewportCreated;

public:

	/** dsds */
	UPROPERTY(config)
		int32 CurrentMonitorIndex;

	UPROPERTY(config)
		FString CurrentMonitorName;
	
};
