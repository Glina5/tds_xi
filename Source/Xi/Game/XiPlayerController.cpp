// Copyright Epic Games, Inc. All Rights Reserved.

// DEBUG Message
//	if (GEngine)
//GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, TEXT("Some debug message!"));

#include "../Game/XiPlayerController.h"
#include "Blueprint/AIBlueprintHelperLibrary.h"
#include "Runtime/Engine/Classes/Components/DecalComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"

#include "../Game/InputUserSettings.h"

#include "../Game/GraphicsUserSettings.h"

#include "../Character/XiCharacter.h"
#include "Engine/World.h"

AXiPlayerController::AXiPlayerController()
{
	bShowMouseCursor = true;
	DefaultMouseCursor = EMouseCursor::Crosshairs;
//	UE_LOG(LogTemp, Warning, TEXT("PlayerController Constract"));
}

void AXiPlayerController::PlayerTick(float DeltaTime)
{
	Super::PlayerTick(DeltaTime);

	// keep updating the destination every tick while desired
	if (bMoveToMouseCursor)
	{
		MoveToMouseCursor();
	}
}

void AXiPlayerController::SetupInputComponent()
{
	// set up gameplay key bindings
	Super::SetupInputComponent();

	InputComponent->BindAction("SetDestination", IE_Pressed, this, &AXiPlayerController::OnSetDestinationPressed);
	InputComponent->BindAction("SetDestination", IE_Released, this, &AXiPlayerController::OnSetDestinationReleased);

	// support touch devices 
	InputComponent->BindTouch(EInputEvent::IE_Pressed, this, &AXiPlayerController::MoveToTouchLocation);
	InputComponent->BindTouch(EInputEvent::IE_Repeat, this, &AXiPlayerController::MoveToTouchLocation);
	//�������
	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AXiPlayerController::OnResetVR);

	// ���������� ������������ ��� ������ �� BindAxis
	InputComponent->BindAxis("MoveForward", this, &AXiPlayerController::InputAxisX);
	InputComponent->BindAxis("MoveRight", this, &AXiPlayerController::InputAxisY);

	// ���������� ��������
	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Pressed, this, &AXiPlayerController::InputAttackPressed);
	InputComponent->BindAction(TEXT("FireEvent"), EInputEvent::IE_Released, this, &AXiPlayerController::InputAttackReleased);
	InputComponent->BindAction(TEXT("ReloadEvent"), EInputEvent::IE_Released, this, &AXiPlayerController::TryReloadWeapon);

	InputComponent->BindAction(TEXT("AlternativeFireEvent"), EInputEvent::IE_Pressed, this, &AXiPlayerController::InputAlternativeAttackPressed);
	InputComponent->BindAction(TEXT("AlternativeFireEvent"), EInputEvent::IE_Released, this, &AXiPlayerController::InputAlternativeAttackReleased);

	//������������ ������
	InputComponent->BindAction(TEXT("SwitchNextWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthNextWeapon);
	InputComponent->BindAction(TEXT("SwitchPreviosWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwitchPreviosWeapon);

	//������
	InputComponent->BindAxis("MouseCursorX", this, &AXiPlayerController::InputMouseCursorX);
	InputComponent->BindAxis("MouseCursorY", this, &AXiPlayerController::InputMouseCursorY);

	//���� �����
	InputComponent->BindAction(TEXT("PauseMenu"), EInputEvent::IE_Released, this, &AXiPlayerController::PauseMenu);

	TArray<FKey> HotKeys;
	HotKeys.Add(EKeys::Zero);
	HotKeys.Add(EKeys::One);
	HotKeys.Add(EKeys::Two);
	HotKeys.Add(EKeys::Three);
	HotKeys.Add(EKeys::Four);
	HotKeys.Add(EKeys::Five);
	HotKeys.Add(EKeys::Six);
	HotKeys.Add(EKeys::Seven);
	HotKeys.Add(EKeys::Eight);
	HotKeys.Add(EKeys::Nine);

	InputComponent->BindKey(HotKeys[1], IE_Pressed, this, &AXiPlayerController::TKeyPressed<0>);
	InputComponent->BindKey(HotKeys[2], IE_Pressed, this, &AXiPlayerController::TKeyPressed<1>);
	InputComponent->BindKey(HotKeys[3], IE_Pressed, this, &AXiPlayerController::TKeyPressed<2>);
	InputComponent->BindKey(HotKeys[4], IE_Pressed, this, &AXiPlayerController::TKeyPressed<3>);
	InputComponent->BindKey(HotKeys[5], IE_Pressed, this, &AXiPlayerController::TKeyPressed<4>);
	InputComponent->BindKey(HotKeys[6], IE_Pressed, this, &AXiPlayerController::TKeyPressed<5>);
	InputComponent->BindKey(HotKeys[7], IE_Pressed, this, &AXiPlayerController::TKeyPressed<6>);
	InputComponent->BindKey(HotKeys[8], IE_Pressed, this, &AXiPlayerController::TKeyPressed<7>);
	InputComponent->BindKey(HotKeys[9], IE_Pressed, this, &AXiPlayerController::TKeyPressed<8>);
	InputComponent->BindKey(HotKeys[0], IE_Pressed, this, &AXiPlayerController::TKeyPressed<9>);

	//InputComponent->BindAction(TEXT("SwitchOneWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthOneWeapon);
	//InputComponent->BindAction(TEXT("SwitchTwoWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthTwoWeapon);
	//InputComponent->BindAction(TEXT("SwitchThreeWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthThreeWeapon);
	//InputComponent->BindAction(TEXT("SwitchFourWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthFourWeapon);
	//InputComponent->BindAction(TEXT("SwitchFiveWeapon"), EInputEvent::IE_Released, this, &AXiPlayerController::EventSwicthFiveWeapon);

	//������ ������ � �������
	InputComponent->BindAction(TEXT("PickUpItem"), EInputEvent::IE_Released, this, &AXiPlayerController::EventPickUpItem);

	//����������� � ��������� ������
	InputComponent->BindAction(TEXT("ChangeSpringArmUp"), EInputEvent::IE_Released, this, &AXiPlayerController::EventChangeSpringArmUp);
	InputComponent->BindAction(TEXT("ChangeSpringArmDown"), EInputEvent::IE_Released, this, &AXiPlayerController::EventChangeSpringArmDown);
	//InputComponent->BindAction(TEXT("ChangeSpringArm"), EInputEvent::IE_Released, this, &AXiPlayerController::EventChangeSpringArm);
}

void AXiPlayerController::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void AXiPlayerController::MoveToMouseCursor()
{
	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	{
		if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
		{
			if (MyPawn->GetCursorToWorld())
			{
				UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, MyPawn->GetCursorToWorld()->GetComponentLocation());
			}
		}
	}
	else
	{
		// Trace to see what is under the mouse cursor
		FHitResult Hit;
		GetHitResultUnderCursor(ECC_Visibility, false, Hit);

		if (Hit.bBlockingHit)
		{
			// We hit something, move there
			SetNewMoveDestination(Hit.ImpactPoint);
		}
	}
}

void AXiPlayerController::MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	FVector2D ScreenSpaceLocation(Location);

	// Trace to see what is under the touch location
	FHitResult HitResult;
	GetHitResultAtScreenPosition(ScreenSpaceLocation, CurrentClickTraceChannel, true, HitResult);
	if (HitResult.bBlockingHit)
	{
		// We hit something, move there
		SetNewMoveDestination(HitResult.ImpactPoint);
	}
}

void AXiPlayerController::SetNewMoveDestination(const FVector DestLocation)
{
	APawn* const MyPawn = GetPawn();
	if (MyPawn)
	{
		float const Distance = FVector::Dist(DestLocation, MyPawn->GetActorLocation());

		// We need to issue move command only if far enough in order for walk animation to play correctly
		if ((Distance > 120.0f))
		{
			UAIBlueprintHelperLibrary::SimpleMoveToLocation(this, DestLocation);
		}
	}
}

void AXiPlayerController::OnSetDestinationPressed()
{
	// set flag to keep updating destination until released
	bMoveToMouseCursor = true;
}

void AXiPlayerController::OnSetDestinationReleased()
{
	// clear flag to indicate we should stop updating the destination
	bMoveToMouseCursor = false;
}

void AXiPlayerController::OnUnPossess()
{
	Super::OnUnPossess();
}

FIntPoint AXiPlayerController::GetPlayerScreenResolution()
{
	UGraphicsUserSettings* CurrentGraphicsUserSettings;
	CurrentGraphicsUserSettings = UGraphicsUserSettings::GetGraphicsUserSettings();
	return CurrentGraphicsUserSettings->GetScreenResolution();
}

void AXiPlayerController::InputMouseCursorX(float Value)
{
	if (Value != 0.f && GetMovableCursor())
	{
		GetViewportSize(CameraViewportWidth, CameraViewportHeight);
		if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
		{
			MyPawn->OffsetMouseCursorX(Value, CameraViewportWidth);
		}
	}
}

void AXiPlayerController::InputMouseCursorY(float Value)
{
	if (Value != 0.f && GetMovableCursor())
	{
		GetViewportSize(CameraViewportWidth, CameraViewportHeight);
		if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
		{
			MyPawn->OffsetMouseCursorY(Value, CameraViewportHeight);
		}
	}
}

// �������� �� ���������
void AXiPlayerController::InputAxisX(float Value)
{
//	AxisX = Value;
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AddMovementInput(FVector(1.0f, 0.0f, 0.0f), Value);
	}
}

// �������� �� �����������
void AXiPlayerController::InputAxisY(float Value) 
{
//	AxisY = Value;
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AddMovementInput(FVector(0.0f, 1.0f, 0.0f), Value);
	}
}

// �������� ��� ������� (��������)
void AXiPlayerController::InputAttackPressed()
{
	if(AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AttackCharEvent(true);
	}
}

// �������� ��� ���������� (��������)
void AXiPlayerController::InputAttackReleased()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AttackCharEvent(false);
	}
}

void AXiPlayerController::TryReloadWeapon()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->ReloadWeaponEvent();
	}
}

void AXiPlayerController::InputAlternativeAttackPressed()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AlternativeAttackCharEvent(true);
	}
}

void AXiPlayerController::InputAlternativeAttackReleased()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->AlternativeAttackCharEvent(false);
	}
}

void AXiPlayerController::EventSwicthNextWeapon()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TrySwicthNextWeapon();
	}
}

void AXiPlayerController::EventSwitchPreviosWeapon()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TrySwitchPreviosWeapon();
	}
}

bool AXiPlayerController::GetMovableCursor()
{
	return bIsMovableCursor;
}

void AXiPlayerController::SetMovableCursor(bool NewCursorMovableState)
{
	bIsMovableCursor = NewCursorMovableState;
}

void AXiPlayerController::PauseMenu()
{
	if (bIsInGame)
	{
		PauseMenu_BP();
	}
}

void AXiPlayerController::PauseMenu_BP_Implementation()
{
	// in BP
}

void AXiPlayerController::SetbIsInGame(bool NewGameState)
{
	bIsInGame = NewGameState;
}

//UPlayerInput* AXiPlayerController::GetPlayerInput() // todo debug sensetivity
//{
//	UPlayerInput* NewPlayerInput;
//	if (PlayerInput)
//	{
//		NewPlayerInput = PlayerInput;
//		return NewPlayerInput;
//	}
//	return NULL;
//}
//
//float AXiPlayerController::GetMouseSensitivityX()
//{
//	if (PlayerInput)
//	{
//		return PlayerInput->GetMouseSensitivityX();
//	}
//	return 0.f;
//}
//
//float AXiPlayerController::GetMouseSensitivityY()
//{
//	if (PlayerInput)
//	{
//		return PlayerInput->GetMouseSensitivityX();
//	}
//	return 0.f;
//}

void AXiPlayerController::EventSwicthToSelectWeapon(int32 IdKey)
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TrySwitchToSelectWeapon(IdKey);
	}
}

//void AXiPlayerController::EventSwicthOneWeapon()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TrySwitchToSelectWeapon((int8)0);
//	}
//}
//
//void AXiPlayerController::EventSwicthTwoWeapon()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TrySwitchToSelectWeapon((int8)1);
//	}
//}
//
//void AXiPlayerController::EventSwicthThreeWeapon()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TrySwitchToSelectWeapon((int8)2);
//	}
//}
//
//void AXiPlayerController::EventSwicthFourWeapon()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TrySwitchToSelectWeapon((int8)3);
//	}
//}
//
//void AXiPlayerController::EventSwicthFiveWeapon()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TrySwitchToSelectWeapon((int8)4);
//	}
//}

void AXiPlayerController::EventPickUpItem()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TryPickUpItem();
	}
}

void AXiPlayerController::EventChangeSpringArmUp()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TryChangeSpringArmUp();
	}
}

void AXiPlayerController::EventChangeSpringArmDown()
{
	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
	{
		MyPawn->TryChangeSpringArmDown();
	}
}

FCharacterMouseInputSettings AXiPlayerController::GetPlayerInputUserSettings()
{
	PlayerInputUserSettings = UInputUserSettings::GetInputUserSettings();
	FCharacterMouseInputSettings NewPlayerInputSetttings;
	if (PlayerInputUserSettings)
	{
		NewPlayerInputSetttings = PlayerInputUserSettings->GetNewInputSetttings();
		NewPlayerInputSetttings.MouseCursorSensitivity = PlayerInputUserSettings->GetMouseSensitivity();
		NewPlayerInputSetttings.bSystemMouseSensitivity = PlayerInputUserSettings->GetIsSystemMouseSensitivity();
		NewPlayerInputSetttings.bIsOffsetOpticalAimCursor = PlayerInputUserSettings->GetIsOffsetOpticalAimCursor();
	}
	return NewPlayerInputSetttings;
}

void AXiPlayerController::SetNewMouseInputSettings()
{
	PlayerInputUserSettings = UInputUserSettings::GetInputUserSettings();
	FCharacterMouseInputSettings NewPlayerInputSetttings;
	if (PlayerInputUserSettings)
	{
		NewPlayerInputSetttings = PlayerInputUserSettings->GetNewInputSetttings();
		if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
		{
			MyPawn->SetCharacterNewMouseInputSettings(NewPlayerInputSetttings);
		}
	}
}

//void AXiPlayerController::EventChangeSpringArm()
//{
//	if (AXiCharacter* MyPawn = Cast<AXiCharacter>(GetPawn()))
//	{
//		MyPawn->TryChangeSpringArm();
//	}
//}

