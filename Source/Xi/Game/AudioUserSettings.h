// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "AudioUserSettings.generated.h"

UENUM(BlueprintType)
enum class EVolumeType : uint8
{
	EVT_Master		UMETA(DisplayName = "Master Volume"),
	EVT_Music		UMETA(DisplayName = "Music Volume"),
	EVT_Effect		UMETA(DisplayName = "Effect Volume"),
	EVT_UI			UMETA(DisplayName = "UI Volume"),
	EVT_Ambient		UMETA(DisplayName = "Ambient Volume"),
	EVT_Voice		UMETA(DisplayName = "Voice Volume"),
};

/**
 * 
 */
UCLASS(config = GameUserSettings, configdonotcheckdefaults)
class XI_API UAudioUserSettings : public UObject
{
	GENERATED_UCLASS_BODY()
	
public:

	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		static UAudioUserSettings* GetAudioUserSettings();

	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		void SetDefaultAllVolume();

	static UAudioUserSettings* DirtyMainGameUserSettings;


	/** Gets the Master Volume value. */

	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		virtual void SaveSettings();


	/**
	* Sets the volume value for the given category for audio output.
	* @Param	VolumeCategory	Category to set volume for.
	* @Param	NewVolume		The new value for the volume.
	*/
	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		void SetAudioCategoryVolume(EVolumeType VolumeCategory, float NewVolume);

	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		float GetAudioCategoryVolume(EVolumeType VolumeCategory);

	/** Gets the Master Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetMasterVolume() const { return MasterVolume; }

	/** Gets the Music Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetMusicVolume() const { return MusicVolume; }

	/** Gets the Effect Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetEffectVolume() const { return EffectVolume; }

	/** Gets the UI Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetUIVolume() const { return UIVolume; }

	/** Gets the Ambient Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetAmbientVolume() const { return AmbientVolume; }

	/** Gets the Voice Volume value. */
	UFUNCTION(BlueprintPure, Category = AudioSettings)
		float GetVoiceVolume() const { return VoiceVolume; }

	/** Loads the user settings from persistent storage */
	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		virtual void LoadSettings(bool bForceReload = false);

	/** Applies all current user settings to the game and saves to permanent storage (e.g. file), optionally checking for command line overrides. */
	UFUNCTION(BlueprintCallable, Category = AudioSettings)
		virtual void ApplySettings();

protected:

	virtual void BeginDestroy() override;

	/** Master volume value */
	UPROPERTY(config)
		float MasterVolume;

	/** Music volume value */
	UPROPERTY(config)
		float MusicVolume;

	/** Effect volume value */
	UPROPERTY(config)
		float EffectVolume;

	/** UI volume value */
	UPROPERTY(config)
		float UIVolume;

	/** Ambient volume value */
	UPROPERTY(config)
		float AmbientVolume;

	/** Voice volume value */
	UPROPERTY(config)
		float VoiceVolume;
};
