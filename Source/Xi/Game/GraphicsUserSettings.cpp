// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/GraphicsUserSettings.h"
#include "Widgets/SWindow.h"
#include "Windows/AllowWindowsPlatformTypes.h"
#include "Windows.h"
#include "Winuser.h"
#include "Windows/HideWindowsPlatformTypes.h"
#include "GameFramework/GameUserSettings.h"

UGraphicsUserSettings* UGraphicsUserSettings::DirtyMainGraphicsSettings = NULL;

UGraphicsUserSettings::UGraphicsUserSettings(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	//SetToDefaults();
	SetGraphicsToDefault();
}

UGraphicsUserSettings* UGraphicsUserSettings::GetGraphicsUserSettings()
{
	if (UGraphicsUserSettings::DirtyMainGraphicsSettings == NULL)
	{
		UGameUserSettings::LoadConfigIni();

		UGraphicsUserSettings::DirtyMainGraphicsSettings = NewObject<UGraphicsUserSettings>(GetTransientPackage(), UGraphicsUserSettings::StaticClass());

		UGraphicsUserSettings::DirtyMainGraphicsSettings = Cast<UGraphicsUserSettings>(GEngine->GetGameUserSettings());
		//UGraphicsUserSettings::DirtyMainGraphicsSettings = Cast<UGraphicsUserSettings>(UGameUserSettings::GetGameUserSettings());
		//UGraphicsUserSettings::DirtyMainGraphicsSettings->SetGraphicsToDefault();
		//UGraphicsUserSettings::DirtyMainGraphicsSettings->LoadConfig(UGraphicsUserSettings::DirtyMainGraphicsSettings->GetClass(), *GGameUserSettingsIni);
	}
		return UGraphicsUserSettings::DirtyMainGraphicsSettings;
}

TArray<FMyMonitorInfo> UGraphicsUserSettings::GetMyAttachedMonitorInfo() //TArray<FMonitorInfo>
{
	TArray<FMyMonitorInfo> arrayMyMonitorInfo;
	FDisplayMetrics Display;
	FDisplayMetrics::RebuildDisplayMetrics(Display);
	FMyMonitorInfo item;
	TArray<FString> arrayAttachedMonitor;
	GetMonitorsFullName(arrayAttachedMonitor);
	int32 i = 0;
	while (Display.MonitorInfo.IsValidIndex(i))
	{
		item.MonitorIndex = i;
		if (arrayAttachedMonitor.IsValidIndex(i))
		{
			item.MonitorName = arrayAttachedMonitor[i];
		}
		else
		{
			item.MonitorName = Display.MonitorInfo[i].Name;
		}
		item.MonitorID = Display.MonitorInfo[i].ID;
		item.MonitorNativeWidth = Display.MonitorInfo[i].NativeWidth;
		item.MonitorNativeHeight = Display.MonitorInfo[i].NativeHeight;
		item.MonitorDisplayRect = Display.MonitorInfo[i].DisplayRect;
		item.MonitorWorkArea = Display.MonitorInfo[i].WorkArea;
		item.MonitorWorkAreaLeft = Display.MonitorInfo[i].WorkArea.Left;
		item.MonitorWorkAreaTop = Display.MonitorInfo[i].WorkArea.Top;
		item.bIsPrimaryMonitor = Display.MonitorInfo[i].bIsPrimary;
		item.MonitorDPI = Display.MonitorInfo[i].DPI;
		arrayMyMonitorInfo.Add(item);
		i++;
	}
	return arrayMyMonitorInfo;

}

int32 UGraphicsUserSettings::SetSelectedMonitor(int32 MonitorIndex, FString MonitorName)
{
	//	return MonitorNumber;

	FDisplayMetrics Display;
	FDisplayMetrics::RebuildDisplayMetrics(Display);

	if (Display.MonitorInfo.IsValidIndex(MonitorIndex))
	{
		if (MonitorIndex == CurrentMonitorIndex)
		{
			FString Message = "_____ This monitor has already been selected as the main one for the game.";
			UE_LOG(LogTemp, Display, TEXT("%s"), *Message);
			return MonitorIndex;
		}
		else
		{
			const FMonitorInfo Monitor = Display.MonitorInfo[MonitorIndex];
			// check if UserSettings.ResolutionSizeX > Monitor.NativeWidth || UserSettings.ResolutionSizeY > Monitor.NativeHeight
			// if true then change your game resolution to Monitor.NativeWidth, Monitor.NativeHeight

			const FIntPoint Res = GetScreenResolution();
			if (Res.X > Monitor.NativeWidth || Res.Y > Monitor.NativeHeight)
			{
				SetScreenResolution(FIntPoint(Monitor.NativeWidth, Monitor.NativeHeight));
			}

			SetWindowPosition(Monitor.WorkArea.Left, Monitor.WorkArea.Top);
			if (GEngine && GEngine->GameViewport)
			{
				ApplyWindowPosition(MonitorIndex, MonitorName);
			}
			/*	else
				{
					DelegateHandleViewportCreated = UGameViewportClient::OnViewportCreated().AddUObject(this, &UGraphicsUserSettings::ApplyWindowPosition);
				}*/



				//const int32 ModernWindowPosX = Monitor.WorkArea.Left;
				//const int32 ModernWindowPosY = Monitor.WorkArea.Top;
				//const FVector2D Position(static_cast<float>(ModernWindowPosX), static_cast<float>(ModernWindowPosY));

				//if (GEngine && GEngine->GameViewport)
				//{
				//	TSharedPtr<SWindow> Window = GEngine->GameViewport->GetWindow();

				//	Window->MoveWindowTo(Position);
				//	Window->SetOnWindowMoved(FOnWindowMoved::CreateUObject(this, &UGameEngine::OnGameWindowMoved)); //��� ���������� ���������� �� �����

				//}

		}
	}
	else
	{
		FString Message = "_____ Incorrect monitor index, check that the screen is still connected.";
		UE_LOG(LogTemp, Warning, TEXT("%s"), *Message);
		return -1;
	}
	return MonitorIndex;
}

int32 UGraphicsUserSettings::GetCurrentMonitorIndex()
{
	return CurrentMonitorIndex;
}

FString UGraphicsUserSettings::GetCurrentMonitorName()
{
	return CurrentMonitorName;
}

FMyWindowPosition UGraphicsUserSettings::GetCurrentWindowPosition()
{
	FMyWindowPosition CurrentWindowPosition;
	CurrentWindowPosition.X_Position = GetWindowPosition().X;
	CurrentWindowPosition.Y_Position = GetWindowPosition().Y;
	return CurrentWindowPosition;
}

void UGraphicsUserSettings::SetGraphicsToDefault()
{
	CurrentMonitorIndex = 0;
	CurrentMonitorName.Empty();
}

//void UGraphicsUserSettings::SetToDefaults()
//{
//	Super::SetToDefaults();
//	CurrentMonitorIndex = 0;
//	CurrentMonitorName.Empty();
//}

//void GetMyMonitorsInfo(TArray<FMonitorInfo>& OutMonitorInfo)
void UGraphicsUserSettings::GetMonitorsFullName(TArray<FString>& OutMonitorName)
{
	DISPLAY_DEVICE DisplayDevice;
	DisplayDevice.cb = sizeof(DisplayDevice);
	DWORD DeviceIndex = 0; // device index

//	FMonitorInfo* PrimaryDevice = nullptr;
	OutMonitorName.Empty(2); // Reserve two slots, as that will be the most common maximum

	FString DeviceID;
	while (EnumDisplayDevices(0, DeviceIndex, &DisplayDevice, 0))
	{
		if ((DisplayDevice.StateFlags & DISPLAY_DEVICE_ATTACHED_TO_DESKTOP) > 0)
		{
			DISPLAY_DEVICE Monitor;
			ZeroMemory(&Monitor, sizeof(Monitor));
			Monitor.cb = sizeof(Monitor);
			DWORD MonitorIndex = 0;

			while (EnumDisplayDevices(DisplayDevice.DeviceName, MonitorIndex, &Monitor, 0))
			{
				if (Monitor.StateFlags & DISPLAY_DEVICE_ACTIVE &&
					!(Monitor.StateFlags & DISPLAY_DEVICE_MIRRORING_DRIVER))
				{
					//FMonitorInfo Info;
					FString FullMonitorName;


					TArray<FString> parts;
					FString deviceId = Monitor.DeviceID;
					deviceId.ParseIntoArray(parts, TEXT("\\"), true);
					if (parts.Num() > 1) {
						FullMonitorName = FString(Monitor.DeviceString) + " - " + parts[1];
					}
					else {
						FullMonitorName = Monitor.DeviceString;
					}

					OutMonitorName.Add(FullMonitorName);
				}
				MonitorIndex++;

				ZeroMemory(&Monitor, sizeof(Monitor));
				Monitor.cb = sizeof(Monitor);
			}
		}

		ZeroMemory(&DisplayDevice, sizeof(DisplayDevice));
		DisplayDevice.cb = sizeof(DisplayDevice);
		DeviceIndex++;
	}
}

void UGraphicsUserSettings::ApplyWindowPosition(int32 MonitorIndex, FString MonitorName)
{
	TSharedPtr<SWindow> window = GEngine->GameViewport->GetWindow();
	window->MoveWindowTo(GetWindowPosition());
	window->Resize(GetScreenResolution());
	//window->SetWindowMode(EWindowMode::Fullscreen);

	//mGameUserSettings->SetFullscreenMode(EWindowMode::Fullscreen);
	//mGameUserSettings->ApplyResolutionSettings(skCheckForCommandLineOverrides);
	CurrentMonitorIndex = MonitorIndex;
	CurrentMonitorName = MonitorName;
	//if (DelegateHandleViewportCreated.IsValid())
	//{
	//	UGameViewportClient::OnViewportCreated().Remove(DelegateHandleViewportCreated);
	//	DelegateHandleViewportCreated.Reset();
	//}
	//ToDo Add save
	SaveSettings();
}