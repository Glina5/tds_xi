// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/AudioUserSettings.h"
#include "GameFramework/GameUserSettings.h"

UAudioUserSettings* UAudioUserSettings::DirtyMainGameUserSettings = NULL;

UAudioUserSettings::UAudioUserSettings(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SetDefaultAllVolume();
}

UAudioUserSettings* UAudioUserSettings::GetAudioUserSettings()
{
	//class UAudioUserSettings* AudioUserSettings;
	if (UAudioUserSettings::DirtyMainGameUserSettings == NULL)
	{

		////FConfigCacheIni::LoadGlobalIniFile(GGameUserSettingsIni, TEXT("GameUserSettings"), nullptr, false, false, true, true, *FPaths::GeneratedConfigDir()); // UE_4.27.2
		//bool bForceReload = false;
		//// Load .ini, allowing merging
		//FConfigCacheIni::LoadGlobalIniFile(GGameUserSettingsIni, TEXT("GameUserSettings"), nullptr, bForceReload, false, true, *FPaths::GeneratedConfigDir()); //UE_4.25.2 // *FConfigCacheIni::GetGameUserSettingsDir() - ���� ����� �� � ��������

		UGameUserSettings::LoadConfigIni();

		UAudioUserSettings::DirtyMainGameUserSettings = NewObject<UAudioUserSettings>(GetTransientPackage(), UAudioUserSettings::StaticClass());

		UAudioUserSettings::DirtyMainGameUserSettings->SetDefaultAllVolume();
		UAudioUserSettings::DirtyMainGameUserSettings->LoadConfig(UAudioUserSettings::DirtyMainGameUserSettings->GetClass(), *GGameUserSettingsIni);

	}
	return UAudioUserSettings::DirtyMainGameUserSettings;
}

void UAudioUserSettings::SetDefaultAllVolume()
{
	MasterVolume = 0.5f;
	MusicVolume = 0.5f;
	EffectVolume = 0.5f;
	UIVolume = 0.5f;
	AmbientVolume = 0.5f;
	VoiceVolume = 0.5f;
}

void UAudioUserSettings::SaveSettings()
{
	SaveConfig(CPF_Config, *GGameUserSettingsIni);
}

void UAudioUserSettings::SetAudioCategoryVolume(EVolumeType VolumeCategory, float NewVolume)
{
	switch (VolumeCategory)
	{
	case EVolumeType::EVT_Master:
		MasterVolume = NewVolume;
		break;
	case EVolumeType::EVT_Music:
		MusicVolume = NewVolume;
		break;
	case EVolumeType::EVT_Effect:
		EffectVolume = NewVolume;
		break;
	case EVolumeType::EVT_UI:
		UIVolume = NewVolume;
		break;
	case EVolumeType::EVT_Ambient:
		AmbientVolume = NewVolume;
		break;
	case  EVolumeType::EVT_Voice:
		VoiceVolume = NewVolume;
		break;
	}
}

float UAudioUserSettings::GetAudioCategoryVolume(EVolumeType VolumeCategory)
{
	float CurrentVolume = 0;
	switch (VolumeCategory)
	{
	case EVolumeType::EVT_Master:
		CurrentVolume = MasterVolume;
		break;
	case EVolumeType::EVT_Music:
		CurrentVolume = MusicVolume;
		break;
	case EVolumeType::EVT_Effect:
		CurrentVolume = EffectVolume;
		break;
	case EVolumeType::EVT_UI:
		CurrentVolume = UIVolume;
		break;
	case EVolumeType::EVT_Ambient:
		CurrentVolume = AmbientVolume;
		break;
	case  EVolumeType::EVT_Voice:
		CurrentVolume = VoiceVolume;
		break;
	}
	return CurrentVolume;
}

void UAudioUserSettings::LoadSettings(bool bForceReload /*= false*/)
{
	if (bForceReload)
	{
		UGameUserSettings::LoadConfigIni(bForceReload);
	}
	LoadConfig(GetClass(), *GGameUserSettingsIni);
}

void UAudioUserSettings::ApplySettings()
{
	//something else...

	SaveSettings();
}


void UAudioUserSettings::BeginDestroy()
{
	UAudioUserSettings::DirtyMainGameUserSettings = NULL;
	Super::BeginDestroy();
}
