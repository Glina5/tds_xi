// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"

#include "../Game/InputUserSettings.h"

#include "XiPlayerController.generated.h"


UCLASS()
class AXiPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	AXiPlayerController();

protected:
	/** True if the controlled character should navigate to the mouse cursor. */
	uint32 bMoveToMouseCursor : 1;

	// Begin PlayerController interface
	virtual void PlayerTick(float DeltaTime) override;
	virtual void SetupInputComponent() override;
	// End PlayerController interface

	/** Resets HMD orientation in VR. */
	void OnResetVR();

	/** Navigate player to the current mouse cursor location. */
	void MoveToMouseCursor();

	/** Navigate player to the current touch location. */
	void MoveToTouchLocation(const ETouchIndex::Type FingerIndex, const FVector Location);
	
	/** Navigate player to the given world location. */
	void SetNewMoveDestination(const FVector DestLocation);

	/** Input handlers for SetDestination action. */
	void OnSetDestinationPressed();
	void OnSetDestinationReleased();

	//���������� �� �����, �������� ����� ������
	virtual void OnUnPossess() override;

	template<int32 Id> 
	void TKeyPressed()
	{
		EventSwicthToSelectWeapon(Id);
	}

	bool bIsMovableCursor = true;

	int32 CameraViewportWidth = 0;
	int32 CameraViewportHeight = 0;

	UInputUserSettings* PlayerInputUserSettings;

	bool bIsInGame = false;

public:
	UFUNCTION()
		FIntPoint GetPlayerScreenResolution();
	//������
	UFUNCTION()
		void InputMouseCursorX(float Value);
	UFUNCTION()
		void InputMouseCursorY(float Value);


	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	//��������
	UFUNCTION()
		void InputAttackPressed();
	UFUNCTION()
		void InputAttackReleased();
	UFUNCTION()
		void TryReloadWeapon();
	//����������������������
	UFUNCTION()
		void InputAlternativeAttackPressed();
	UFUNCTION()
		void InputAlternativeAttackReleased();

	//������������ ������
	UFUNCTION()
		void EventSwicthNextWeapon();
	UFUNCTION()
		void EventSwitchPreviosWeapon();

	UFUNCTION(BlueprintCallable)
		bool GetMovableCursor();

	UFUNCTION(BlueprintCallable)
		void SetMovableCursor(bool NewCursorMovableState);

	UFUNCTION(BlueprintCallable)
		void PauseMenu();
	UFUNCTION(BlueprintNativeEvent)
		void PauseMenu_BP();

	UFUNCTION(BlueprintCallable)
		void SetbIsInGame(bool NewGameState);
	//UFUNCTION(BlueprintCallable)
	//	UPlayerInput* GetPlayerInput(); // // todo debug sensetivity
	//
	//UFUNCTION(BlueprintCallable)
	//	float GetMouseSensitivityX();
	//
	//UFUNCTION(BlueprintCallable)
	//	float GetMouseSensitivityY();

//	UFUNCTION()

	UFUNCTION()
		void EventSwicthToSelectWeapon(int32 IdKey);
	/*UFUNCTION()
		void EventSwicthTwoWeapon();
	UFUNCTION()
		void EventSwicthThreeWeapon();
	UFUNCTION()
		void EventSwicthFourWeapon();
	UFUNCTION()
		void EventSwicthFiveWeapon();*/

	//������ ������ � �������
	UFUNCTION()
		void EventPickUpItem();

	// ������
	UFUNCTION()
		void EventChangeSpringArmUp();
	UFUNCTION()
		void EventChangeSpringArmDown();
	//UFUNCTION()
	//	void EventChangeSpringArm();

	/////////////////////////////////////////////// ���� �� �����
	//float AxisX = 0.0f;
	//float AxisY = 0.0f;

	//�������� Input ���������
	UFUNCTION(BlueprintCallable, Category = InputSettings)
	FCharacterMouseInputSettings GetPlayerInputUserSettings();

	UFUNCTION(BlueprintCallable, Category = InputSettings)
	void SetNewMouseInputSettings();

};


