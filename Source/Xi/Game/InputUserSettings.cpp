// Fill out your copyright notice in the Description page of Project Settings.


#include "../Game/InputUserSettings.h"
#include "GameFramework/GameUserSettings.h"
//#include "GameFramework/PlayerInput.h"

UInputUserSettings* UInputUserSettings::DirtyMainInputUserSettings = NULL;

UInputUserSettings::UInputUserSettings(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	SetDefaultAllInput();
}

UInputUserSettings* UInputUserSettings::GetInputUserSettings()
{
	if (UInputUserSettings::DirtyMainInputUserSettings == NULL)
	{

		UGameUserSettings::LoadConfigIni();

		UInputUserSettings::DirtyMainInputUserSettings = NewObject<UInputUserSettings>(GetTransientPackage(), UInputUserSettings::StaticClass());

		UInputUserSettings::DirtyMainInputUserSettings->SetDefaultAllInput();
		UInputUserSettings::DirtyMainInputUserSettings->LoadConfig(UInputUserSettings::DirtyMainInputUserSettings->GetClass(), *GGameUserSettingsIni);

	}
	return UInputUserSettings::DirtyMainInputUserSettings;
}

void UInputUserSettings::SetDefaultAllInput()
{
	MouseCursorSensitivity = 10.f;
	bIsOffsetOpticalAimCursor = true;
	bSystemMouseSensitivity = false;
}

void UInputUserSettings::SaveSettings()
{
	SaveConfig(CPF_Config, *GGameUserSettingsIni);
}

void UInputUserSettings::LoadSettings(bool bForceReload /*= false*/)
{
	if (bForceReload)
	{
		UGameUserSettings::LoadConfigIni(bForceReload);
	}
	LoadConfig(GetClass(), *GGameUserSettingsIni);
}

void UInputUserSettings::ApplySettings()
{
	//something else...

	SaveSettings();
}

FCharacterMouseInputSettings UInputUserSettings::GetNewInputSetttings()
{
	FCharacterMouseInputSettings NewInputSetttings;
	NewInputSetttings.MouseCursorSensitivity = MouseCursorSensitivity;
	NewInputSetttings.bIsOffsetOpticalAimCursor = bIsOffsetOpticalAimCursor;
	NewInputSetttings.bSystemMouseSensitivity = bSystemMouseSensitivity;
	return NewInputSetttings;
}

float UInputUserSettings::GetMouseSensitivity()
{
	return MouseCursorSensitivity;
}

bool UInputUserSettings::GetIsOffsetOpticalAimCursor()
{
	return bIsOffsetOpticalAimCursor;
}

bool UInputUserSettings::GetIsSystemMouseSensitivity()
{
	return bSystemMouseSensitivity;
}

void UInputUserSettings::SetMouseSensitivity(float NewMouseSensitivity)
{
	MouseCursorSensitivity = NewMouseSensitivity;
}

void UInputUserSettings::SetIsOffsetOpticalAimCursor(bool bIsOffsetOpticalAim)
{
	bIsOffsetOpticalAimCursor = bIsOffsetOpticalAim;
}

void UInputUserSettings::SetIsSystemMouseSensitivity(bool bIsSystemMouse)
{
	bSystemMouseSensitivity = bIsSystemMouse;
}

void UInputUserSettings::BeginDestroy()
{
	UInputUserSettings::DirtyMainInputUserSettings = NULL;
	Super::BeginDestroy();
}
