// Fill out your copyright notice in the Description page of Project Settings.


#include "../Structures/Xi_EnvironmentStructure.h"
#include "Materials/MaterialInterface.h"
#include "PhysicalMaterials/PhysicalMaterial.h"

// Sets default values
AXi_EnvironmentStructure::AXi_EnvironmentStructure()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AXi_EnvironmentStructure::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AXi_EnvironmentStructure::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

EPhysicalSurface AXi_EnvironmentStructure::GetSurfuceType()
{
	EPhysicalSurface ResultSurface = EPhysicalSurface::SurfaceType_Default;
	UStaticMeshComponent* myMesh = Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
	if (myMesh)
	{
		UMaterialInterface* myMaterial = myMesh->GetMaterial(0);
		if (myMaterial)
		{
			ResultSurface = myMaterial->GetPhysicalMaterial()->SurfaceType;
		}
	}
	return ResultSurface;
}

USceneComponent* AXi_EnvironmentStructure::GetObjectMesh()
{
	return Cast<UStaticMeshComponent>(GetComponentByClass(UStaticMeshComponent::StaticClass()));
}

float AXi_EnvironmentStructure::GetEffectScale()
{
	return ScaleFX;
}

TArray<UXi_StateEffect*> AXi_EnvironmentStructure::GetAllCurrentEffects()
{
	return Effects;
}

UXi_StateEffect* AXi_EnvironmentStructure::GetActiveEffect(TSubclassOf<UXi_StateEffect> AddEffectClass)
{
	bool bIsSingleEffect = true; //���� ������ ������������?
	int8 j = 0;
	if (Effects.Num() > 0)
	{
		while (j < Effects.Num() && bIsSingleEffect)
		{
			if (Effects[j]->GetClass() == AddEffectClass)
			{
				bIsSingleEffect = false;
				return Effects[j];
			}
			j++;
		}
	}
	return nullptr;
}

void AXi_EnvironmentStructure::RemoveEffect(UXi_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AXi_EnvironmentStructure::AddEffect(UXi_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

