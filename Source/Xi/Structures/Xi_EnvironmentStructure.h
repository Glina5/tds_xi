// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"

#include "../Interface/Xi_IGameActor.h"
#include "../StateEffects/Xi_StateEffect.h"

#include "Xi_EnvironmentStructure.generated.h"

UCLASS()
class XI_API AXi_EnvironmentStructure : public AActor, public IXi_IGameActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AXi_EnvironmentStructure();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	float ScaleFX = 1.f;

	EPhysicalSurface GetSurfuceType() override;
	USceneComponent* GetObjectMesh() override;
	float GetEffectScale() override;
	TArray<UXi_StateEffect*> GetAllCurrentEffects() override;
	UXi_StateEffect* GetActiveEffect(TSubclassOf<UXi_StateEffect> AddEffectClass) override;
	void RemoveEffect(UXi_StateEffect* RemoveEffect)override;
	void AddEffect(UXi_StateEffect* newEffect)override;

	//Effect
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<UXi_StateEffect*> Effects;

};
