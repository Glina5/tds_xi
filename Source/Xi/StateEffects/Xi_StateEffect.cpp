// Fill out your copyright notice in the Description page of Project Settings.


#include "../StateEffects/Xi_StateEffect.h"

#include "../Character/XiHealthComponent.h"
#include "../Interface/Xi_IGameActor.h"
#include "Kismet/GameplayStatics.h"



bool UXi_StateEffect::InitObject(AActor* Actor)
{
	myActor = Actor;

	IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->AddEffect(this);
	}
	//ToDo
	return true;
}

void UXi_StateEffect::DestroyObject()
{
	IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->RemoveEffect(this);
	}

	myActor = nullptr;
	if (this && this->IsValidLowLevel())
	{
		//this->ConditionalBeginDestroy();
		this->MarkPendingKill();
		//GetWorld()->ForceGarbageCollection(true);// ��������
		GEngine->ForceGarbageCollection(true);
	}
}

bool UXi_StateEffect_ExecuteOnce::InitObject(AActor* Actor)
{
	Super::InitObject(Actor);
	ExecuteOnce();
	return true;
}

void UXi_StateEffect_ExecuteOnce::DestroyObject()
{
	Super::DestroyObject();
}

void UXi_StateEffect_ExecuteOnce::ExecuteOnce()
{
	if (myActor)
	{
		UXiHealthComponent* myHealthComp = Cast<UXiHealthComponent>(myActor->GetComponentByClass(UXiHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
	DestroyObject();
}

bool UXi_StateEffect_ExecuteTimer::InitObject(AActor* Actor)
{
	if (GetWorld()->GetTimerManager().IsTimerActive(TimerHandle_EffectTimer))
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UXi_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UXi_StateEffect_ExecuteTimer::Execute, RateTime, true);
		return false;
	}
	Super::InitObject(Actor);

	GetWorld()->GetTimerManager().SetTimer(TimerHandle_EffectTimer, this, &UXi_StateEffect_ExecuteTimer::DestroyObject, Timer, false);
	GetWorld()->GetTimerManager().SetTimer(TimerHandle_ExecuteTimer, this, &UXi_StateEffect_ExecuteTimer::Execute, RateTime, true);

	return true;
}

void UXi_StateEffect_ExecuteTimer::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_EffectTimer);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);
	}
	Super::DestroyObject();
}

void UXi_StateEffect_ExecuteTimer::Execute()
{
	//if (myActor)
	//{
	//	//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
	//	UXiHealthComponent* myHealthComp = Cast<UXiHealthComponent>(myActor->GetComponentByClass(UXiHealthComponent::StaticClass()));
	//	if (myHealthComp)
	//	{
	//		myHealthComp->ChangeHealthValue(Power);
	//	}
	//}
}

bool UXi_StateEffect_Stun::InitObject(AActor* Actor)
{
	if (Super::InitObject(Actor))
	{
		if (myActor)
		{
			IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(myActor);
			if (myInterface)
			{
				myInterface->SwitchStunEffect(true);
			}
		}
	}

	return true;
}

void UXi_StateEffect_Stun::DestroyObject()
{
	IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(myActor);
	if (myInterface)
	{
		myInterface->SwitchStunEffect(false);
	}
	Super::DestroyObject();
}

bool UXi_StateEffect_DamageEffect::InitObject(AActor* Actor)
{
	if (Super::InitObject(Actor))
	{
		if (ParticleEffect)
		{
			FName NameBoneToAttached;
			FVector Loc = FVector(0);
			float ParticalScale = 1.f;
			USceneComponent* ComponentForAttach;
			ComponentForAttach = myActor->GetRootComponent();

			IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(myActor);
			if (myInterface)
			{
				NameBoneToAttached = myInterface->GetBoneName();
				ComponentForAttach = myInterface->GetObjectMesh();
				ParticalScale = myInterface->GetEffectScale();
			}

			if (ComponentForAttach == nullptr)
			{
				ComponentForAttach = myActor->GetRootComponent();
			}
			ParticalScale *= EffectParticalScale;

			ParticleEmitter = UGameplayStatics::SpawnEmitterAttached(ParticleEffect, ComponentForAttach, NameBoneToAttached, Loc, FRotator::ZeroRotator, FVector(ParticalScale), EAttachLocation::SnapToTarget, false);
		}
	}
	

	return true;
}

void UXi_StateEffect_DamageEffect::DestroyObject()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_EffectTimer);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ExecuteTimer);
	}
	ParticleEmitter->DestroyComponent();
	ParticleEmitter = nullptr;
	Super::DestroyObject();
}

void UXi_StateEffect_DamageEffect::Execute()
{
	if (myActor)
	{
		//UGameplayStatics::ApplyDamage(myActor,Power,nullptr,nullptr,nullptr);	
		UXiHealthComponent* myHealthComp = Cast<UXiHealthComponent>(myActor->GetComponentByClass(UXiHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeHealthValue(Power);
		}
	}
}

bool UXi_StateEffect_Immortal::InitObject(AActor* Actor)
{
	if (Super::InitObject(Actor))
	{
		if (myActor)
		{
			UXiHealthComponent* myHealthComp = Cast<UXiHealthComponent>(myActor->GetComponentByClass(UXiHealthComponent::StaticClass()));
			if (myHealthComp)
			{
				myHealthComp->ChangeImmortal(true);
			}
		}
	}

	return true;
}

void UXi_StateEffect_Immortal::DestroyObject()
{
	if (myActor)
	{
		UXiHealthComponent* myHealthComp = Cast<UXiHealthComponent>(myActor->GetComponentByClass(UXiHealthComponent::StaticClass()));
		if (myHealthComp)
		{
			myHealthComp->ChangeImmortal(false);
		}
	}
	Super::DestroyObject();
}

