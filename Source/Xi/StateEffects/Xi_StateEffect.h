// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"

#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"

#include "Xi_StateEffect.generated.h"

/**
 * 
 */
UCLASS(Blueprintable, BlueprintType)
class XI_API UXi_StateEffect : public UObject
{
	GENERATED_BODY()
	
public:

	virtual bool InitObject(AActor* Actor);
	virtual void DestroyObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		TArray<TEnumAsByte<EPhysicalSurface>> PossibleInteractSurface;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		bool bIsStakable = false;
	//ToDo ��������� ����� ������ ���������, (������ � �������) � �� ��������

	AActor* myActor = nullptr;
};

UCLASS()
class XI_API UXi_StateEffect_ExecuteOnce : public UXi_StateEffect
{
	GENERATED_BODY()

public:

	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void ExecuteOnce();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting Execute Once")
		float Power = 20.0f;
};

UCLASS()
class XI_API UXi_StateEffect_ExecuteTimer : public UXi_StateEffect
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	virtual void Execute();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Power = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float Timer = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting ExecuteTimer")
		float RateTime = 1.0f;

	FTimerHandle TimerHandle_ExecuteTimer;
	FTimerHandle TimerHandle_EffectTimer;
};

UCLASS()
class XI_API UXi_StateEffect_Stun : public UXi_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;
};

UCLASS()
class XI_API UXi_StateEffect_DamageEffect : public UXi_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;

	void Execute() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting DamageEffect")
		float EffectParticalScale = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting DamageEffect")
		UParticleSystem* ParticleEffect = nullptr;
	UPROPERTY()
	UParticleSystemComponent* ParticleEmitter = nullptr;
};

UCLASS()
class XI_API UXi_StateEffect_Immortal : public UXi_StateEffect_ExecuteTimer
{
	GENERATED_BODY()

public:
	bool InitObject(AActor* Actor) override;
	void DestroyObject() override;
};