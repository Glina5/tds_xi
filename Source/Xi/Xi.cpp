// Copyright Epic Games, Inc. All Rights Reserved.

#include "Xi.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, Xi, "Xi" );

DEFINE_LOG_CATEGORY(LogXi)
 