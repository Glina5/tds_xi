// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Weapon/WeaponDefault.h"
#include "WeaponDefault_Medic.generated.h"

/**
 * 
 */
UCLASS()
class XI_API AWeaponDefault_Medic : public AWeaponDefault
{
	GENERATED_BODY()

public:
//	AWeaponDefault_Medic();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Fire|FX")
		UParticleSystem* EffectAlternativeFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Fire|Sound")
		USoundBase* SoundAlternativeFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Fire|Sound")
		USoundBase* SoundAlternativeReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Hit|Decales")
		UDecalComponent* DecalOnAlternativeHit = nullptr;
	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Hit|Decales")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> AlternativeHitDecals;
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Hit|Sound")
		USoundBase* AlternativeHitSound = nullptr; // ���� ����� ������ ������ �����, �� ������� ���� TMap
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Hit|Effect")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> AlternativeHitFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Alternative|Hit|Effect")
		TSubclassOf<UXi_StateEffect> AlternativeEffect = nullptr;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Timers flags
	float AlternativeFireTimer = 0.0f;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AlternativeFireTick(float DeltaTime);

	void AlternativeFire();

	bool IsHealerModuleActivate();

	UFUNCTION(BlueprintCallable)
		void SetWeaponStateAlternativeFire(bool bIsFire);

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "AlternativeFireLogic")
		bool WeaponAlternativeFiring = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AlternativeFireLogic")
		bool AlwaysMedicGun = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AlternativeFireLogic")
	float AlternativeRateOfFire = 0.f;
	
};
