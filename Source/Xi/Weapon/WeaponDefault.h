// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/ArrowComponent.h"

#include "../FuncLibrary/Types.h"
#include "../Weapon/ProjectileDefault.h"
#include "WeaponDefault.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponFireStart,UAnimMontage*,AnimFireChar);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnWeaponReloadStart,UAnimMontage*,ReloadAnim); //�������� �� ����� �����������
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FonWeaponReloadEnd, bool, bIsSuccess, int32, AmmoSafe);

UCLASS()
class XI_API AWeaponDefault : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponDefault();

	FOnWeaponFireStart OnWeaponFireStart;
	FOnWeaponReloadStart OnWeaponReloadStart;
	FonWeaponReloadEnd OnWeaponReloadEnd;
	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USceneComponent* SceneComponent = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class USkeletalMeshComponent* SkeletalMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UStaticMeshComponent* StaticMeshWeapon = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* ShootLocation = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
		class UArrowComponent* BulletShellImpulse = nullptr;
// 	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"), Category = Components)
// 		class UParticleSystem* LaserBeam = nullptr;

	UPROPERTY()
		FWeaponInfo WeaponSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponInfo")
		FAdditionalWeaponInfo AdditionalWeaponInfo;
	UPROPERTY()
		FWeaponModulesInfo WeaponModule;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UParticleSystemComponent* WeaponLaserBeam = nullptr;

	EMovementState CurrentCharacterMovementState = EMovementState::Run_State; // todo ?? �������� ��� ���?

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void FireTick(float DeltaTime);
	void ReloadTick(float DeltaTime);
	//Dispersion
	void DispersionTick(float DeltaTime);
	//DropMeshTimers
	void MagazineDropTick(float DeltaTime);
	void ShellDropTick(float DeltaTime);

	void WeaponInit();

	AActor* AutoAimingTarget(bool bIsEnemy, FVector ActorTargetLocation); // todo2

	FHitResult WeaponLineTrace(FVector2D NewEndLocation);

	bool SetTargetActor(AActor* NewTarget); // todo 2

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FireLogic")
	bool WeaponFiring = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	bool WeaponReloading = false;
	bool WeaponAiming = false;

	UFUNCTION(BlueprintCallable)
	void SetWeaponStateFire(bool bIsFire);

	bool CheckWeaponCanFire();

	FProjectileInfo GetProjectile();

	//������� ������ �� �������� ������ UFUNCTION()
	UFUNCTION()
	void Fire();

	//to do debug
	UFUNCTION(BlueprintCallable)
	float GetCurrentDispersionDebug();

	void UpdateStateWeapon(EMovementState NewMovementState);
	//Dispersion
	void ChangeDispersionByShot();
	float GetCurrentDispersion() const;
	FVector ApplyDispersionToShoot(FVector DirectionShoot)const;

	//Dispersion
	FVector GetFireEndLocationWithDispersion()const;
	FVector GetFireEndLocation()const;

	//���������� ������������ ���������� ����. ����� ��� ���������
	int8 GetNumberProjectileByShot() const;

	//��������� ���� ������
	float WeaponDamage();

	//���������� ������ � �������� ������ � ��������
	int32 NewWeaponRound();

	//���������� ������ � ������
	UFUNCTION(BlueprintCallable)
	bool SetModuleToWeaponInCell(EModulesType NewModuleType, int32 NewModuleTier, int32 ModuleCellNumber);

	bool SetWeaponLaserBeam(UParticleSystemComponent* NewLaserBeam);

	UParticleSystemComponent* GetWeaponLaserBeam();

	//����� �������� ������

	//Timers'flags
	float FireTimer = 0.0f;
	float FirstShotTimer = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "ReloadLogic")
	float ReloadTimer = 0.0f;


	//������ � ��������� ������� ��� ������� ������ � ���������
	TArray<int32> CurrentWeaponModulesIndex;

	// todo ������ � Types ��� � ������
	//float WeaponScaleAim = 1000.f;

	//flags
	bool BlockFire = false;
	//Dispersion
	bool ShouldReduceDispersion = false;
	bool bIsCharacterMoving = false;
	float CurrentDispersion = 0.0f;
	float CurrentDispersionMax = 1.0f;
	float CurrentDispersionMin = 0.1f;
	float CurrentDispersionStayMax = 1.0f;
	float CurrentDispersionStayMin = 0.1f;
	float CurrentDispersionRecoil = 0.1f;
	float CurrentDispersionReduction = 0.1f;
	float CurrentDispersionStayReduction = 0.1f;
	
	//Timer Drop Magazine on reload
	bool DropMagazineFlag = false;
	float DropMagazineTimer = -1.0;

	//shell flag
	bool DropShellFlag = false;
	float DropShellTimer = -1.0f;

	FVector ShootEndLocation = FVector::ZeroVector;

	//flag Aiming
	bool WeaponHasAiming = true; // todo2

	bool IsGoodTarget = false; // todo 2

	FVector NewTargetActorLocation = FVector(0); // todo2
	
	AActor* NewTargetActor = nullptr;

	//��������� �������� ��� ���������� �����
	FTransform BulletShellOldOffset;
	FVector DropShellBulletOldImpulse = FVector::ZeroVector;

	//��������� �������� ��� ��������� ��������
	FTransform MagazineDropOldOffset;

	UFUNCTION(BlueprintCallable)
	int32 GetWeaponRound();

	UFUNCTION(BlueprintCallable)
	float GetCurrentReloadTime();

	UFUNCTION(BlueprintCallable)
	UAnimMontage* GetCurrentWeaponReloadAnimation();

	UFUNCTION(BlueprintCallable)
	void DropMagazineForCurrentWeapon(FTransform OffsetDropMagazine);

//���... ������ �� ���� ������� UFUNCTION()
	UFUNCTION()
	void InitReload();


	void FinishReload();
	void CancelReload();

	bool CheckCanWeaponReload();
	int32 GetAviableAmmoForReload();

	//UFUNCTION()
	//	void InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass);
	UFUNCTION()
		void InitDropMesh(FDropMashInfo WeaponDropMesh); // ���������� ���������� ��� UFUNCTION()


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Debug")
		bool ShowDebug = false;
};
