// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/WeaponDefault_Medic.h"
#include "Kismet/GameplayStatics.h"
#include "DrawDebugHelpers.h"

void AWeaponDefault_Medic::BeginPlay()
{
	Super::BeginPlay();
}

void AWeaponDefault_Medic::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AlternativeFireTick(DeltaTime);
}
 
void AWeaponDefault_Medic::AlternativeFireTick(float DeltaTime)
{
	if (WeaponAlternativeFiring)
	{
		if (AlternativeFireTimer < 0.f)
		{
			AlternativeFire();
		}
		else
			AlternativeFireTimer -= DeltaTime;
	}
}

void AWeaponDefault_Medic::AlternativeFire()
{
	UAnimMontage* AnimToFire = nullptr;
	if (WeaponAiming)
		AnimToFire = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	else
		AnimToFire = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}

	AlternativeFireTimer = AlternativeRateOfFire;

	//���� �� �������, ��� ������ ��������� �� ����. ����� �������� ���� ������)
	//AdditionalWeaponInfo.Round--;
	//ChangeDispersionByShot();


	//?????? �������� ��� ��������?
	//UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::Fire() AnimToFire %s"), (AnimToFire ? TEXT("True"): TEXT("False") ));
	//������ �������� ����� ����� ���������� ����� �������� ���������� ���� � �������� � ���������
	OnWeaponFireStart.Broadcast(AnimToFire);


	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());



	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();


		FVector EndLocation;
		if (IsGoodTarget) // todo 2 �������� ����� �� ����� ��� �����
		{
			EndLocation = FVector(NewTargetActor->GetActorLocation().X, NewTargetActor->GetActorLocation().Y, ShootLocation->GetComponentLocation().Z) - SpawnLocation;
		}
		else
		{
			EndLocation = GetFireEndLocation();
		}

			SpawnRotation = EndLocation.Rotation();


				//ToDo Projectile null Init trace fire			
				FHitResult Hit;
				TArray<AActor*> Actors;

				EDrawDebugTrace::Type DebugTrace;

				if (ShowDebug)
				{
					//	DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + EndLocation * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else
					DebugTrace = EDrawDebugTrace::None;

				//UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistacneTrace,
//					ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
				FVector ShotTraceEndLocationByWorld = EndLocation * WeaponSetting.DistacneTrace + SpawnLocation;
				ETraceTypeQuery MyProjectileTraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, ShotTraceEndLocationByWorld,
					MyProjectileTraceType, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (AlternativeHitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = AlternativeHitDecals[mySurfacetype];

						if (myMaterial && Hit.GetComponent())
						{
							UGameplayStatics::SpawnDecalAttached(myMaterial, FVector(20.0f), Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, 10.0f);
						}
					}
					if (AlternativeHitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = AlternativeHitFXs[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (AlternativeHitSound)
					{
						UGameplayStatics::PlaySoundAtLocation(GetWorld(), AlternativeHitSound, Hit.ImpactPoint);
					}


					UTypes::AddEffectBySurfaceType(Hit.GetActor(), AlternativeEffect, mySurfacetype, Hit.BoneName);

					//UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponDamage(), GetInstigatorController(), this, NULL);
				}


		}
		//if (GetWeaponRound() <= 0 && !WeaponReloading)
		//{
		//	//Init Reload
		//	if (CheckCanWeaponReload())
		//		InitReload();
		//}
}

bool AWeaponDefault_Medic::IsHealerModuleActivate()
{
	bool result = false;
	int8 j = 0;
	for (j = 0; j < WeaponSetting.CurrentWeaponModulesNumber; j++)
	{
		if (AdditionalWeaponInfo.PlacingWeaponModules.IsValidIndex(j))
		{
			if (AdditionalWeaponInfo.PlacingWeaponModules[j].CurrentModuleType == EModulesType::HealerModule)
			{
				result = true;
			}
		}
	}

	return result;
}

void AWeaponDefault_Medic::SetWeaponStateAlternativeFire(bool bIsFire)
{
//	if (CheckWeaponCanFire())
	WeaponAlternativeFiring = bIsFire;
	//WeaponAlternativeFiring = false;
	//FireTimer = 0.01f;//!!!!!
}
