// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/ProjectileDefault_Granade.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"

int32 DebugExplodeShow = 0;
FAutoConsoleVariableRef CVARExplodeShow(
	TEXT("Xi.DebugExplode"),
	DebugExplodeShow,
	TEXT("Draw Debug for Example"),
	ECVF_Cheat);

void AProjectileDefault_Granade::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectileDefault_Granade::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	TimerExplose(DeltaTime);


}

void AProjectileDefault_Granade::TimerExplose(float DeltaTime)
{
	if (TimerEnabled)
	{
		if (TimerToExplose > TimeToExplose)
		{
			//Explose
//			UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::TimerToExplose() Explose"));
			Explose();

		}
		else
		{
//			UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::TimerToExplose() ++"));
			TimerToExplose += DeltaTime;
		}
	}
}

void AProjectileDefault_Granade::BulletCollisionSphereHit(class UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::BulletCollisionSphereHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);
}

void AProjectileDefault_Granade::ImpactProjectile()
{
	//Init Grenade
//	UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::ImpactProjectile()"));
	TimerEnabled = true;
}

void AProjectileDefault_Granade::Explose()
{
	if (DebugExplodeShow)
	{
		float r = 0.0f;
		float Ri = ProjectileSetting.ProjectileMinRadiusDamage;
		float Ro = ProjectileSetting.ProjectileMaxRadiusDamage;
		float A = ProjectileSetting.ExploseMinDamage;
		float B = ProjectileSetting.ExploseMaxDamage;
		float F = ProjectileSetting.ExplodeFalloffCoef;

		/*��� ����������� ���� �� UGameplayStatics::ApplyRadialDamageWithFalloff
		* ����������� ���������� "����������������� �������" Ds:
		* Ds = FRadialDamageParams::GetDamageScale(r) = (1 - (r-Ri)/(Ro-Ri))^F 
		* ����� ����������� �������� ������������ � ������������ Ds:
		* ActualDamage = FMath::Lerp(A, B, Ds) = A + Ds*(B-A)
		* 
		* �������� ��� 50% �� ������������� ����� ������� ���:
		* ActualDamage = B/2
		* ������ � �������� ������� ��� �������:
		* r = ( 1 - ( (B - 2 * A)/(2 *(B + A)) )^(1 / F) ) * (Ro - Ri) + Ri;
		*/

		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMinRadiusDamage, 12, FColor::Green, false, 12.0f);
		if (Ro>Ri && B>A && A<(B/2))
		{
		r = (1 - pow((B - 2 * A) / (2 * (B + A)), 1 / F)) * (Ro - Ri) + Ri;
		UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::Explose() r = %f"), r);
		DrawDebugSphere(GetWorld(), GetActorLocation(), r, 12, FColor::Yellow, false, 12.0f);
		}
		else
		UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::Explose() Unable to calculate radius for 50procent of damage. Check Inner and Outer Radius, or Min and Max Damage"));
		DrawDebugSphere(GetWorld(), GetActorLocation(), ProjectileSetting.ProjectileMaxRadiusDamage, 12, FColor::Red, false, 12.0f);
	}

	//��� ������� �����:
	//������� ������ "�� ��� �������" � ����� ������� �� ���� ������� � ��� ����������� ������ ��� ������ �������

	TimerEnabled = false;
	if (ProjectileSetting.ExploseFX)
	{
		UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), ProjectileSetting.ExploseFX, GetActorLocation(), GetActorRotation(), FVector(1.0f));
	}
	if (ProjectileSetting.ExploseSound)
	{
		UGameplayStatics::PlaySoundAtLocation(GetWorld(), ProjectileSetting.ExploseSound, GetActorLocation());
	}

	TArray<AActor*> IgnoredActor;
	UGameplayStatics::ApplyRadialDamageWithFalloff(GetWorld(),
		ProjectileSetting.ExploseMaxDamage,
		ProjectileSetting.ExploseMinDamage,
		GetActorLocation(),
		ProjectileSetting.ProjectileMinRadiusDamage,
		ProjectileSetting.ProjectileMaxRadiusDamage,
		ProjectileSetting.ExplodeFalloffCoef,
		NULL, IgnoredActor, this, nullptr);
		//NULL, IgnoredActor, nullptr, nullptr); �������� ������ �� DamageCauser � this
//	UE_LOG(LogTemp, Warning, TEXT("AProjectileDefault_Granade::TimerToExplose() Destroy"));
	this->Destroy();
}
