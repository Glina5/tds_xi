// Fill out your copyright notice in the Description page of Project Settings.


#include "../Weapon/WeaponDefault.h"
#include "DrawDebugHelpers.h"
#include "Components/DecalComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/StaticMeshActor.h"
#include "../Character/XiInventoryComponent.h"

// Sets default values
AWeaponDefault::AWeaponDefault()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Scene"));
	RootComponent = SceneComponent;

	SkeletalMeshWeapon = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Skeletal Mesh"));
	SkeletalMeshWeapon->SetGenerateOverlapEvents(false);
	SkeletalMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	SkeletalMeshWeapon->SetupAttachment(RootComponent);

	StaticMeshWeapon = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh "));
	StaticMeshWeapon->SetGenerateOverlapEvents(false);
	StaticMeshWeapon->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshWeapon->SetupAttachment(RootComponent);

	ShootLocation = CreateDefaultSubobject<UArrowComponent>(TEXT("ShootLocation"));
	ShootLocation->SetupAttachment(RootComponent);

	BulletShellImpulse = CreateDefaultSubobject<UArrowComponent>(TEXT("BulletShellImpulse"));
	BulletShellImpulse->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AWeaponDefault::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		UXiInventoryComponent* MyInv = Cast<UXiInventoryComponent>(GetOwner()->GetComponentByClass(UXiInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->AllWeaponModules.IsValidIndex(0))
			{
				int8 j = 0;
				for (j = 0; j < MyInv->AllWeaponModules.Num(); j++)
				{
					if (MyInv->AllWeaponModules[j].WeaponCompatibleName == WeaponSetting.WeaponName)
					{
						if (AdditionalWeaponInfo.CurrentWeaponModulesIndex[static_cast<int32>(MyInv->AllWeaponModules[j].CurrentModuleType)])
						{
							AdditionalWeaponInfo.CurrentWeaponModulesIndex[static_cast<int32>(MyInv->AllWeaponModules[j].CurrentModuleType)] = static_cast<int32>(MyInv->AllWeaponModules[j].CurrentModuleType);
						}
					}
				}
			}
		}
	}
						
	//�������������� ������, ����� ����� ���� ������� ������ ��� �������� ��������� (������)
	WeaponInit();
	
}

// Called every frame
void AWeaponDefault::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	FireTick(DeltaTime);
	ReloadTick(DeltaTime);

	MagazineDropTick(DeltaTime);
	ShellDropTick(DeltaTime);

	//Dispersion
	if (WeaponSetting.TurnOnDispersion)
	{
		DispersionTick(DeltaTime);
	}
}

void AWeaponDefault::FireTick(float DeltaTime)
{
	//����������� ����������� �������������. ��� Top Down Shooter ��� �����
	if (WeaponFiring && GetWeaponRound() > 0 && !WeaponReloading)
	{

		if (FireTimer < 0.f)
		{
			Fire();
		}
		else
			FireTimer -= DeltaTime;
	}

	// ������ ����� ��� ����������� ��������� ��� ��� ������ �� �������
	/*if (WeaponFiring)
		if (FireTimer < 0.f)
		{
			if (GetWeaponRound() > 0)
			{
				if (!WeaponReloading)
				{
					Fire();
				}
			}
			else
			{
				if (!WeaponReloading)
				{
					InitReload();
				}
			}
		}
		else
			FireTimer -= DeltaTime;*/
}

void AWeaponDefault::ReloadTick(float DeltaTime)
{
	if (WeaponReloading)
	{
		if (ReloadTimer < 0.0f)
		{
			FinishReload();
		}
		else
		{
			ReloadTimer -= DeltaTime;
		}
	}
}

void AWeaponDefault::DispersionTick(float DeltaTime)
{
	if (!WeaponReloading)
	{
		if (!WeaponSetting.bIsFullyAutomatic)
		{
			bIsCharacterMoving ? CurrentDispersion = CurrentDispersionMin : CurrentDispersion = CurrentDispersionStayMin;
		}
		else
		{
			if (!WeaponFiring)
			{
				CurrentDispersion = CurrentDispersion + CurrentDispersionRecoil;
			}
			if (bIsCharacterMoving)
			{
				if (CurrentDispersion > CurrentDispersionMax)
				{
					CurrentDispersion = CurrentDispersionMax;
				}
				else
				{
					if (CurrentDispersion < CurrentDispersionMin)
					{
						CurrentDispersion = CurrentDispersionMin;
					}
				}
			}
			else
			{
				if (CurrentDispersion > CurrentDispersionStayMax)
				{
					CurrentDispersion = CurrentDispersionStayMax;
				}
				else
				{
					if (CurrentDispersion < CurrentDispersionStayMin)
					{
						CurrentDispersion = CurrentDispersionStayMin;
					}
				}								
			}
		}

		if (FirstShotTimer <= 0.f)
		{
			CurrentDispersion = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMin;
			//FirstShotTimer = WeaponSetting.DispersionWeapon.FirstShotDispersionRecoil;
		}
		else
		{
			FirstShotTimer -= DeltaTime;
		}
	}
	//if (ShowDebug)
	//	UE_LOG(LogTemp, Warning, TEXT("Dispersion: MAX = %f. MIN = %f. Current = %f"), CurrentDispersionMax, CurrentDispersionMin, CurrentDispersion);
}

void AWeaponDefault::MagazineDropTick(float DeltaTime)
{
	if (DropMagazineFlag)
	{
		if (DropMagazineTimer < 0.0f)
		{
			DropMagazineFlag = false;
			if (SkeletalMeshWeapon)
			{
			DropMagazineForCurrentWeapon(SkeletalMeshWeapon->GetSocketTransform(FName("Clip_Bone")));
			}
		}
		else
			DropMagazineTimer -= DeltaTime;
	}
}

void AWeaponDefault::ShellDropTick(float DeltaTime)
{
	if (DropShellFlag)
	{
		if (DropShellTimer < 0.0f)
		{
			DropShellFlag = false;
			InitDropMesh(WeaponSetting.ShellBullets);
		}
		else
			DropShellTimer -= DeltaTime;
	}
}

void AWeaponDefault::WeaponInit()
{
	if (SkeletalMeshWeapon && !SkeletalMeshWeapon->SkeletalMesh)
	{
		SkeletalMeshWeapon->DestroyComponent(true);
	}

	if (StaticMeshWeapon && !StaticMeshWeapon->GetStaticMesh())
	{
		StaticMeshWeapon->DestroyComponent();
	}
	UpdateStateWeapon(EMovementState::Run_State);
}

AActor* AWeaponDefault::AutoAimingTarget(bool bIsEnemy, FVector ActorTargetLocation) // todo2
{
	AActor* NewTarget = NULL;
	if (!bIsEnemy)
	{
		FHitResult Hit;
		Hit = WeaponLineTrace(FVector2D(ActorTargetLocation.X,ActorTargetLocation.Y));

		if (Hit.GetActor())
		{
			NewTarget = Hit.GetActor();
		}

	}
	return NewTarget;
}

FHitResult AWeaponDefault::WeaponLineTrace(FVector2D NewEndLocation)
{
	FHitResult Hit;
	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		
		FVector EndLocation;
		
		EndLocation = FVector(NewEndLocation.X, NewEndLocation.Y, ShootLocation->GetComponentLocation().Z) - SpawnLocation;

		
		TArray<AActor*> Actors;

		EDrawDebugTrace::Type DebugTrace;

		//if (ShowDebug)
		//{
		//	//	DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
		//	// 			
		//	//DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + EndLocation * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
		//	DebugTrace = EDrawDebugTrace::ForDuration;
		//}
		//else
			DebugTrace = EDrawDebugTrace::None;

		FVector ShotTraceEndLocationByWorld = EndLocation * WeaponSetting.DistacneTrace + SpawnLocation;
		ETraceTypeQuery MyProjectileTraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);
		UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, ShotTraceEndLocationByWorld,
			MyProjectileTraceType, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
	}

	return Hit;
}

bool AWeaponDefault::SetTargetActor(AActor* NewTarget)
{
	bool bIsSuccess = false;
	// todo 2 �������� �������� �� ���� ����
	if (NewTarget->IsValidLowLevel())
	{
		NewTargetActor = NewTarget;
		bIsSuccess = true;
	}
	return bIsSuccess;
}

void AWeaponDefault::SetWeaponStateFire(bool bIsFire)
{
	if (CheckWeaponCanFire())
		WeaponFiring = bIsFire;
	else
		WeaponFiring = false;
		FireTimer = 0.01f;//!!!!!
}

bool AWeaponDefault::CheckWeaponCanFire()
{
	//���� ����� Reload (�����������) ���� ��� ������ �������� �� ��� 
	return !BlockFire;
}

FProjectileInfo AWeaponDefault::GetProjectile()
{
	return WeaponSetting.ProjectileSetting;
}

void AWeaponDefault::Fire()
{
	UAnimMontage* AnimToFire = nullptr;
	if (WeaponAiming)
		AnimToFire = WeaponSetting.AnimWeaponInfo.AnimCharFireAim;
	else
		AnimToFire = WeaponSetting.AnimWeaponInfo.AnimCharFire;

	if (WeaponSetting.AnimWeaponInfo.AnimWeaponFire
		&& SkeletalMeshWeapon
		&& SkeletalMeshWeapon->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
	{
		SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(WeaponSetting.AnimWeaponInfo.AnimWeaponFire);
	}

	FireTimer = WeaponSetting.RateOfFire;

	//���� �� �������, ��� ������ ��������� �� ����. ����� �������� ���� ������)
	AdditionalWeaponInfo.Round--;
	if (WeaponSetting.bIsFullyAutomatic)
	{
		ChangeDispersionByShot();
	}

	//UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::Fire() AnimToFire %s"), (AnimToFire ? TEXT("True"): TEXT("False") ));
	//������ �������� ����� ����� ���������� ����� �������� ���������� ���� � �������� � ���������
	OnWeaponFireStart.Broadcast(AnimToFire);


	UGameplayStatics::SpawnSoundAtLocation(GetWorld(), WeaponSetting.SoundFireWeapon, ShootLocation->GetComponentLocation());
	UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.EffectFireWeapon, ShootLocation->GetComponentTransform());

	//��������
	int8 NumberProjectile = GetNumberProjectileByShot();

	/*
	* ���� �� ������� � ׸� ���������� �����. �� � ������� � ����������� ������������ �������. � ��� ��������, ����� ������ ����������!
	*/


	if (ShootLocation)
	{
		FVector SpawnLocation = ShootLocation->GetComponentLocation();
		FRotator SpawnRotation = ShootLocation->GetComponentRotation();

		FProjectileInfo ProjectileInfo;
		ProjectileInfo = GetProjectile();

		FVector EndLocation;

		for (int8 i = 0; i < NumberProjectile; i++)//Shotgun
		{

			// UE_LOG(LogTemp, Warning, TEXT("WeaponSetting.TurnOnDispersion: %s"), (WeaponSetting.TurnOnDispersion ? TEXT("True"): TEXT("False") ));
			if (WeaponSetting.TurnOnDispersion)
			{
				EndLocation = GetFireEndLocationWithDispersion();
			}
			else
			{
				EndLocation = GetFireEndLocation();
			}
			if (FirstShotTimer <= 0.f)
			{
				//UpdateStateWeapon(CurrentCharacterMovementState);
				bIsCharacterMoving ? CurrentDispersion = CurrentDispersionMax : CurrentDispersion = CurrentDispersionStayMax;
			}
			FirstShotTimer = WeaponSetting.DispersionWeapon.FirstShotDispersionRecoil; //������ ������� ������, ��������� �� ���������� ��������� ���� �� ���������� ������ FirstShotTimer!


			//FVector Dir = EndLocation - SpawnLocation; ���������� �� ����� ������� �� � EndLocation

			//FVector Dir = ShootEndLocation - SpawnLocation; ������

			//Dir.Normalize(); - End location ��� �������� ���������������


			//��� �� ����� ������� �� ����� ������� !!!!! ��� ������ ��� � �����
			//FMatrix myMatrix(Dir, FVector(0, 0, 0), FVector(0, 0, 0), FVector::ZeroVector);
			//SpawnRotation = myMatrix.Rotator();

			//SpawnRotation = Dir.Rotation(); ��������� �� Dir, ������� ������ EndLocation

			SpawnRotation = EndLocation.Rotation();

			ProjectileInfo.ProjectileDamage = WeaponDamage();


			if (ProjectileInfo.Projectile)
			{
				//Projectile Init ballistic fire

				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = GetOwner();
				SpawnParams.Instigator = GetInstigator();

				AProjectileDefault* myProjectile = Cast<AProjectileDefault>(GetWorld()->SpawnActor(ProjectileInfo.Projectile, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myProjectile)
				{
					//ToDo Init Projectile settings by id in table row(or keep in weapon table)
					//Projectile->BulletProjectileMovement->InitialSpeed = 2500.0f;
					myProjectile->InitProjectile(ProjectileInfo);
					//myProjectile->InitProjectile(WeaponSetting.ProjectileSetting);

				//UE_LOG(LogTemp, Log, TEXT("Actor location: %s"), *ShootLocation.ToString());

				//myProjectile->InitialLifeSpan = 20.0f;

				}
			}
			else
			{
				//ToDo Projectile null Init trace fire			
				FHitResult Hit;
				TArray<AActor*> Actors;

				EDrawDebugTrace::Type DebugTrace;

				if (ShowDebug)
				{
				//	DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + ShootLocation->GetForwardVector() * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DrawDebugLine(GetWorld(), SpawnLocation, SpawnLocation + EndLocation * WeaponSetting.DistacneTrace, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);
					DebugTrace = EDrawDebugTrace::ForDuration;
				}
				else
					DebugTrace = EDrawDebugTrace::None;

				//UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, EndLocation * WeaponSetting.DistacneTrace,
//					ETraceTypeQuery::TraceTypeQuery4, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);
				FVector ShotTraceEndLocationByWorld = EndLocation * WeaponSetting.DistacneTrace + SpawnLocation;
				ETraceTypeQuery MyProjectileTraceType = UEngineTypes::ConvertToTraceType(ECollisionChannel::ECC_GameTraceChannel2);
				UKismetSystemLibrary::LineTraceSingle(GetWorld(), SpawnLocation, ShotTraceEndLocationByWorld,
					MyProjectileTraceType, false, Actors, DebugTrace, Hit, true, FLinearColor::Red, FLinearColor::Green, 5.0f);

				UParticleSystemComponent* TraceFireBeam = UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), WeaponSetting.ProjectileSetting.ProjectileFireTraceFx, SpawnLocation);
				if (TraceFireBeam)
				{
					TraceFireBeam->SetBeamSourcePoint(0, SpawnLocation, 0);
					if (Hit.bBlockingHit)
					{
						TraceFireBeam->SetBeamEndPoint(0, Hit.ImpactPoint);
					}
					else
					{
						TraceFireBeam->SetBeamEndPoint(0, ShotTraceEndLocationByWorld);
					}
					//TraceFireBeam->Life
				}

				if (Hit.GetActor() && Hit.PhysMaterial.IsValid())
				{
					EPhysicalSurface mySurfacetype = UGameplayStatics::GetSurfaceType(Hit);

					if (WeaponSetting.ProjectileSetting.HitDecals.Contains(mySurfacetype))
					{
						UMaterialInterface* myMaterial = WeaponSetting.ProjectileSetting.HitDecals[mySurfacetype];

						if (myMaterial && Hit.GetComponent())
						{
							UDecalComponent* myDecal = UGameplayStatics::SpawnDecalAttached(myMaterial, WeaponSetting.ProjectileSetting.DecalsSize, Hit.GetComponent(), NAME_None, Hit.ImpactPoint, Hit.ImpactNormal.Rotation(), EAttachLocation::KeepWorldPosition, WeaponSetting.ProjectileSetting.DecalsLifeSpan);
							if (myDecal)
							{
								myDecal->SetFadeScreenSize(WeaponSetting.ProjectileSetting.DecalsFadeScreenSize);
							}
						}
					}
					if (WeaponSetting.ProjectileSetting.HitFXs.Contains(mySurfacetype))
					{
						UParticleSystem* myParticle = WeaponSetting.ProjectileSetting.HitFXs[mySurfacetype];
						if (myParticle)
						{
							UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myParticle, FTransform(Hit.ImpactNormal.Rotation(), Hit.ImpactPoint, FVector(1.0f)));
						}
					}

					if (WeaponSetting.ProjectileSetting.HitSound.Contains(mySurfacetype))
					{
						USoundBase* mySound = WeaponSetting.ProjectileSetting.HitSound[mySurfacetype];
						if (mySound)
						{
							UGameplayStatics::PlaySoundAtLocation(GetWorld(), mySound, Hit.ImpactPoint);
						}
					}

					//UXi_StateEffect* NewEffect = NewObject<UXi_StateEffect>(Hit.GetActor(), FName("Effect"));

					// ��������� � Types
					//IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(Hit.GetActor());
					//if (myInterface)
					//{
					//	//EPhysicalSurface mySurface;
					//	//mySurface = myInterface->GetSurfuceType();
					//	UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype)

					//	if (mySurface != EPhysicalSurface::SurfaceType_Default) 
					//	{
					//		if (ProjectileInfo.Effect)
					//		{
					//			UXi_StateEffect* myEffect = Cast<UXi_StateEffect>(ProjectileInfo.Effect->GetDefaultObject());
					//			if (myEffect)
					//			{
					//				bool bIsCanAdd = false;
					//				int8 i = 0;
					//				while (i < myEffect->PossibleInteractSurface.Num() && !bIsCanAdd)
					//				{
					//					if (myEffect->PossibleInteractSurface[i] == mySurface)
					//					{
					//						bIsCanAdd = true;
					//						UXi_StateEffect* NewEffect = NewObject<UXi_StateEffect>(Hit.GetActor(), FName("Effect"));
					//						if (NewEffect)
					//						{
					//							NewEffect->InitObject();
					//						}
					//					}
					//					i++;
					//				}
					//			}
					//		}
					//	}
					//}

					UTypes::AddEffectBySurfaceType(Hit.GetActor(), ProjectileInfo.Effect, mySurfacetype, Hit.BoneName);	
					//UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::Fire() Hit.Component: %s"), *Hit.Component->GetName());

					//if (Hit.GetActor()->GetClass()->ImplementsInterface(UXi_IGameActor::StaticClass()))
					//{
					//	//IXi_IGameActor::Execute_AviableForEffects(Hit.GetActor());
					//	//IXi_IGameActor::Execute_AviableForEffectsBP(Hit.GetActor());
					//}

					UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponDamage(), GetInstigatorController(), this, NULL);
					//UGameplayStatics::ApplyDamage(Hit.GetActor(), WeaponSetting.ProjectileSetting.ProjectileDamage, GetInstigatorController(), this, NULL);
				}

			}

		}
		if (GetWeaponRound() <= 0 && !WeaponReloading)
		{
			//Init Reload
			if (CheckCanWeaponReload())
				InitReload();
		}
	}

	if (WeaponSetting.ShellBullets.DropMesh && SkeletalMeshWeapon)
	{
			WeaponSetting.ShellBullets.DropMeshOffset = BulletShellOldOffset + SkeletalMeshWeapon->GetSocketTransform(FName("AmmoEject"));
//			WeaponSetting.ShellBullets.DropMeshOffset = BulletShellOldOffset + BulletShellImpulse->GetComponentTransform(); // �������� � ����� ������
			WeaponSetting.ShellBullets.DropMeshImpulseDir = DropShellBulletOldImpulse + BulletShellImpulse->GetForwardVector();

		if (WeaponSetting.ShellBullets.DropMeshTime < 0.0f)
		{
			InitDropMesh(WeaponSetting.ShellBullets);
		}
		else
		{
			DropShellFlag = true;
			DropShellTimer = WeaponSetting.ShellBullets.DropMeshTime;
		}
	}


	//if (BulletCasingsImpulse)
	//{
	//		//��������� �����
	//		FVector SpawnShallLocation = BulletCasingsImpulse->GetComponentLocation();
	//		FRotator SpawnShallRotation = BulletCasingsImpulse->GetComponentRotation();
	//		//��������� �����
	//		if (WeaponSetting.ShellBullets)
	//		{
	//			FActorSpawnParameters SpawnParams;
	//			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	//			SpawnParams.Owner = GetOwner();
	//			SpawnParams.Instigator = GetInstigator();
	//			AStaticMeshActor* BulletShell = GetWorld()->SpawnActor<AStaticMeshActor>(&SpawnShallLocation, &SpawnShallRotation, SpawnParams);
	//			BulletShell->SetStaticMesh((WeaponSetting.ShellBullets);
	//			BulletShell->SetMobility(EComponentMobility::Movable);
	//			BulletShell->SetLifeSpan(3.0f);
	//		}
	//}
}

float AWeaponDefault::GetCurrentDispersionDebug()
{
	return CurrentDispersion;
}

void AWeaponDefault::UpdateStateWeapon(EMovementState NewMovementState)
{

	CurrentCharacterMovementState = NewMovementState;

	//ToDo Dispersion
	BlockFire = false;

	if (WeaponSetting.bIsFullyAutomatic)
	{
		bIsCharacterMoving ? CurrentDispersion -= CurrentDispersionMax : CurrentDispersion -= CurrentDispersionStayMax; 
	}

	switch (NewMovementState)
	{
	case EMovementState::Aim_State:
		WeaponAiming = true; // todo
		CurrentDispersionStayMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMax;
		CurrentDispersionStayMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMax + WeaponSetting.DispersionWeapon.Run_AddDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMin + WeaponSetting.DispersionWeapon.Run_AddDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimRecoil + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimRecoil;
		CurrentDispersionStayReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Aim_AddDispersionReduction;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Aim_AddDispersionReduction + WeaponSetting.DispersionWeapon.Run_AddDispersionReduction;
		//ShouldReduceDispersion = true; // todo
		break;
	case EMovementState::AimWalk_State:
		WeaponAiming = true;
		CurrentDispersionStayMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMax;
		CurrentDispersionStayMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMax + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimMin + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimRecoil + WeaponSetting.DispersionWeapon.Aim_AddDispersionAimRecoil + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimRecoil;
		CurrentDispersionStayReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Aim_AddDispersionReduction;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Aim_AddDispersionReduction + WeaponSetting.DispersionWeapon.Walk_AddDispersionReduction;
		//ShouldReduceDispersion = true;
		break;
	case EMovementState::Walk_State:
		WeaponAiming = false;
		CurrentDispersionStayMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax;
		CurrentDispersionStayMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimRecoil + WeaponSetting.DispersionWeapon.Walk_AddDispersionAimRecoil;
		CurrentDispersionStayReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Walk_AddDispersionReduction;
		//ShouldReduceDispersion = false;
		break;
	case EMovementState::Run_State:
		WeaponAiming = false;
		CurrentDispersionStayMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax;
		CurrentDispersionStayMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin;
		CurrentDispersionMax = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMax + WeaponSetting.DispersionWeapon.Run_AddDispersionAimMax;
		CurrentDispersionMin = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimMin + WeaponSetting.DispersionWeapon.Run_AddDispersionAimMin;
		CurrentDispersionRecoil = WeaponSetting.DispersionWeapon.Stay_StateDispersionAimRecoil + WeaponSetting.DispersionWeapon.Run_AddDispersionAimRecoil;
		CurrentDispersionStayReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction;
		CurrentDispersionReduction = WeaponSetting.DispersionWeapon.Stay_StateDispersionReduction + WeaponSetting.DispersionWeapon.Run_AddDispersionReduction;
		//ShouldReduceDispersion = false;
		break;
	case EMovementState::Sprint_State:
		WeaponAiming = false; // todo ����� � �������
		BlockFire = true;
		SetWeaponStateFire(false);//set fire trigger to false
		//Block Fire
		break;
	default:
		break;
	}
	if (WeaponSetting.bIsFullyAutomatic)
	{
		bIsCharacterMoving ? CurrentDispersion += CurrentDispersionMax : CurrentDispersion += CurrentDispersionStayMax;
	}
}

void AWeaponDefault::ChangeDispersionByShot()
{
	bIsCharacterMoving ? CurrentDispersion -= CurrentDispersionReduction : CurrentDispersion -= CurrentDispersionStayReduction;
}

float AWeaponDefault::GetCurrentDispersion() const
{
	float Result = CurrentDispersion;
	return Result;
}

FVector AWeaponDefault::ApplyDispersionToShoot(FVector DirectionShoot) const
{
	float HorizontalDispersionHalfAngleRad = GetCurrentDispersion() * PI / 180.f;
	float VerticalDispersionHalfAngleRad = HorizontalDispersionHalfAngleRad / WeaponSetting.DispersionWeapon.RatioHorizontalToVerticalDispersion;
	return FMath::VRandCone(DirectionShoot, HorizontalDispersionHalfAngleRad, VerticalDispersionHalfAngleRad);
}

FVector AWeaponDefault::GetFireEndLocationWithDispersion() const
{
	//bool bShootDirection = false;
	FVector EndLocation = FVector(0.f);

	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());

	if (tmpV.Size() > WeaponSetting.SizeVectorToChangeShootDirectionLogic)
	{
		// EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot((ShootLocation->GetComponentLocation() - ShootEndLocation).GetSafeNormal()) * -20000.0f; ��������
		/*
		*�� ������� ����� ������������ ����� ��� �� ������ �� ��������� 
		*� ����� ������ ����� ������� ��-�� ���� ����������� �������� �� -1, 
		*� ����� �������� �� 20000 ����� �������� ������ ������������ � �������������???
		*� ����� ����� � ����� SpawnLocation ( ShootLocation->GetComponentLocation()) ���� �� �������� ��� ��������?
		*/

		EndLocation = ApplyDispersionToShoot(ShootEndLocation - ShootLocation->GetComponentLocation());
		if (ShowDebug)
		{
			float HorizontalDispersionHalfAngleRad = GetCurrentDispersion() * PI / 180.f;
			float VerticalDispersionHalfAngleRad = HorizontalDispersionHalfAngleRad / WeaponSetting.DispersionWeapon.RatioHorizontalToVerticalDispersion;
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), -(ShootLocation->GetComponentLocation() - ShootEndLocation), WeaponSetting.DistacneTrace, HorizontalDispersionHalfAngleRad, VerticalDispersionHalfAngleRad, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}
	else
	{
		//EndLocation = ShootLocation->GetComponentLocation() + ApplyDispersionToShoot(ShootLocation->GetForwardVector()) * 20000.0f;
//		EndLocation = ApplyDispersionToShoot(ShootLocation->GetForwardVector()); // ������ ���� ���� ����������� �����
		EndLocation = ApplyDispersionToShoot(FVector(ShootLocation->GetForwardVector().X, ShootLocation->GetForwardVector().Y, 0.0f));
		if (ShowDebug)
		{
			float HorizontalDispersionHalfAngleRad = GetCurrentDispersion() * PI / 180.f;
			float VerticalDispersionHalfAngleRad = HorizontalDispersionHalfAngleRad / WeaponSetting.DispersionWeapon.RatioHorizontalToVerticalDispersion;
			DrawDebugCone(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetForwardVector(), WeaponSetting.DistacneTrace, HorizontalDispersionHalfAngleRad, VerticalDispersionHalfAngleRad, 32, FColor::Emerald, false, .1f, (uint8)'\000', 1.0f);
		}
	}


	if (ShowDebug)
	{
		//direction weapon look
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector() * 500.0f, FColor::Cyan, false, 5.f, (uint8)'\000', 0.5f);
		//direction projectile must fly
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootEndLocation, FColor::Red, false, 5.f, (uint8)'\000', 0.5f);
		//Direction Projectile Current fly
		//DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), EndLocation, FColor::Black, false, 5.f, (uint8)'\000', 0.5f); // Old Version
		DrawDebugLine(GetWorld(), ShootLocation->GetComponentLocation(), ShootLocation->GetComponentLocation()+EndLocation*20000.0f, FColor::Black, false, 5.f, (uint8)'\000', 0.5f);

	//	DrawDebugSphere(GetWorld(), ShootLocation->GetComponentLocation() + ShootLocation->GetForwardVector()*WeaponSetting.SizeVectorToChangeShootDirectionLogic, 10.f, 8, FColor::Red, false, 4.0f);
	}
	return EndLocation;
}

FVector AWeaponDefault::GetFireEndLocation() const
{
	FVector EndLocation = FVector(0.f);
	FVector tmpV = (ShootLocation->GetComponentLocation() - ShootEndLocation);
	//UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Size = %f"), tmpV.X, tmpV.Y, tmpV.Size());
	if (tmpV.Size() > WeaponSetting.SizeVectorToChangeShootDirectionLogic)
	{
		EndLocation = ShootEndLocation - ShootLocation->GetComponentLocation();
		EndLocation = EndLocation.GetSafeNormal();
	}
	else
	{
//		EndLocation = ShootLocation->GetForwardVector();  // ������ ���� ���� ����������� �����
		 EndLocation = FVector(ShootLocation->GetForwardVector().X, ShootLocation->GetForwardVector().Y, 0.f);
	//	EndLocation = FVector(ShootLocation->GetForwardVector().X, ShootLocation->GetForwardVector().Y, ShootLocation->GetComponentLocation().Z);
	}

	return EndLocation;
}

int8 AWeaponDefault::GetNumberProjectileByShot() const
{
	return WeaponSetting.NumberProjectileByShot;
}


float AWeaponDefault::WeaponDamage()
{
	float NewDamage = WeaponSetting.ProjectileSetting.ProjectileDamage;
	int8 j = 0;
	for (j = 0; j < WeaponSetting.CurrentWeaponModulesNumber; j++)
	{
		if (AdditionalWeaponInfo.PlacingWeaponModules.IsValidIndex(j))
		{
			if (AdditionalWeaponInfo.PlacingWeaponModules[j].CurrentModuleType == EModulesType::DamageModule)
			{
				switch (AdditionalWeaponInfo.PlacingWeaponModules[j].PlayersHaveTier)
				{
				case 0:
					break;
				case 1:
					NewDamage = NewDamage * AdditionalWeaponInfo.PlacingWeaponModules[j].Tier1;
					break;
				case 2:
					NewDamage = NewDamage * AdditionalWeaponInfo.PlacingWeaponModules[j].Tier2;
					break;
				case 3:
					NewDamage = NewDamage * AdditionalWeaponInfo.PlacingWeaponModules[j].Tier3;
					break;
				default:
					break;
				}
			}
		}
	}
	
	return NewDamage;
}

int32 AWeaponDefault::NewWeaponRound()
{
	int32 NewRound = WeaponSetting.MaxRound;
	int8 j = 0;
	for (j = 0; j < WeaponSetting.CurrentWeaponModulesNumber; j++)
	{
		if (AdditionalWeaponInfo.PlacingWeaponModules.IsValidIndex(j))
		{
			if (AdditionalWeaponInfo.PlacingWeaponModules[j].CurrentModuleType == EModulesType::RoundModule)
			{
				switch (AdditionalWeaponInfo.PlacingWeaponModules[j].PlayersHaveTier)
				{
				case 0:
					break;
				case 1:
					NewRound = NewRound + static_cast<int32>(AdditionalWeaponInfo.PlacingWeaponModules[j].Tier1);
					break;
				case 2:
					NewRound = NewRound + static_cast<int32>(AdditionalWeaponInfo.PlacingWeaponModules[j].Tier2);
					break;
				case 3:
					NewRound = NewRound + static_cast<int32>(AdditionalWeaponInfo.PlacingWeaponModules[j].Tier3);
					break;
				default:
					break;
				}
			}
		}
	}

	return NewRound;
}

bool AWeaponDefault::SetModuleToWeaponInCell(EModulesType NewModuleType, int32 NewModuleTier, int32 ModuleCellNumber)
{
	bool result = true;
	if (GetOwner())
	{
		UXiInventoryComponent* MyInv = Cast<UXiInventoryComponent>(GetOwner()->GetComponentByClass(UXiInventoryComponent::StaticClass()));
		if (MyInv)
		{
			
			if (AdditionalWeaponInfo.CurrentWeaponModulesIndex.IsValidIndex(static_cast<int32>(NewModuleType)))
			{
				if (MyInv->AllWeaponModules.IsValidIndex(AdditionalWeaponInfo.CurrentWeaponModulesIndex[static_cast<int32>(NewModuleType)]))
				{
					if (AdditionalWeaponInfo.PlacingWeaponModules.IsValidIndex(ModuleCellNumber))
					{
						AdditionalWeaponInfo.PlacingWeaponModules[ModuleCellNumber] = MyInv->AllWeaponModules[AdditionalWeaponInfo.CurrentWeaponModulesIndex[static_cast<int32>(NewModuleType)]];
						//TO DO!!!!!!! �������� ���������� ���� ������! ����� ���������� � ��������� � ������ ���������� ������ ��������
						AdditionalWeaponInfo.PlacingWeaponModules[ModuleCellNumber].PlayersHaveTier = NewModuleTier;
					}
					else result = false;
				}
				else result = false;
			}
			else result = false;
		}
		else result = false;
	}
	else result = false;

	if (result == false)
	{
		UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::SetModuleToWeaponInCell - Something Wrong :D"));
	}

	return result;
}

bool AWeaponDefault::SetWeaponLaserBeam(UParticleSystemComponent* NewLaserBeam) // return false if NewLaserBeam = nullptr;
{
	if (NewLaserBeam)
	{
		WeaponLaserBeam = NewLaserBeam;
		return true;
	}
	WeaponLaserBeam = nullptr;
	return false;
}

UParticleSystemComponent* AWeaponDefault::GetWeaponLaserBeam()
{
	return WeaponLaserBeam;
}

int32 AWeaponDefault::GetWeaponRound()
{
	return AdditionalWeaponInfo.Round;
}


float AWeaponDefault::GetCurrentReloadTime()
{
	return WeaponSetting.ReloadTime;
}


UAnimMontage* AWeaponDefault::GetCurrentWeaponReloadAnimation()
{
	//����� ������ ������ ���� ���������� � �������� ������ ����������� �� �����
	/*if (WeaponAiming)
	{
		return WeaponSetting.AnimWeaponInfo.AnimWeaponReloadAim;
	}
	else
	{
		return WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
	}*/
	return WeaponSetting.AnimWeaponInfo.AnimWeaponReload;
}

void AWeaponDefault::DropMagazineForCurrentWeapon(FTransform OffsetDropMagazine)
{
	if (WeaponSetting.MagazineDrop.DropMesh)
	{
		WeaponSetting.MagazineDrop.DropMeshOffset = MagazineDropOldOffset + OffsetDropMagazine;
		InitDropMesh(WeaponSetting.MagazineDrop);
	}
}

void AWeaponDefault::InitReload()
{
	WeaponReloading = true;

	ReloadTimer = WeaponSetting.ReloadTime;


	if (WeaponSetting.AnimWeaponInfo.AnimCharReload)
	{
		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReload);

		UAnimMontage* AnimWeaponToReload = nullptr;

		AnimWeaponToReload = WeaponSetting.AnimWeaponInfo.AnimWeaponReload;

		if (WeaponSetting.AnimWeaponInfo.AnimWeaponReload
			&& SkeletalMeshWeapon
			&& SkeletalMeshWeapon->GetAnimInstance())//Bad Code? maybe best way init local variable or in func
		{
			float PlayRateAnim = AnimWeaponToReload->GetPlayLength()/WeaponSetting.ReloadTime; // ��������� �������� �������� ��� �������� ��������� ������� �� ������� �����������
			SkeletalMeshWeapon->GetAnimInstance()->Montage_Play(AnimWeaponToReload,PlayRateAnim);
		}

		if (SkeletalMeshWeapon && WeaponSetting.MagazineDrop.DropMesh)
		{
			float MagazinDropNewTimer = WeaponSetting.MagazineDrop.DropMeshTime;
			if (WeaponSetting.MagazineDrop.DropMeshTime >= 0.0f)
			{
				MagazinDropNewTimer = WeaponSetting.MagazineDrop.DropMeshTime + (WeaponSetting.ReloadTime / 2.0f);
			}
				DropMagazineFlag = true;
				DropMagazineTimer = MagazinDropNewTimer;
			//DropMagazineForCurrentWeapon(SkeletalMeshWeapon->GetComponentTransform());
		}
	}

	//����� ������ ������ ���� ���������� � �������� ������ ����������� �� �����
	//if (WeaponAiming)
	//{
	//	if (WeaponSetting.AnimWeaponInfo.AnimCharReloadAim)
	//	{
	//		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReloadAim); //Broadcast() - ����� ���� ��� ���� �������
	//	}
	//}
	//else
	//{
	//	if (WeaponSetting.AnimWeaponInfo.AnimCharReload)
	//	{
	//		OnWeaponReloadStart.Broadcast(WeaponSetting.AnimWeaponInfo.AnimCharReload);
	//	}
	//}


	//ToDo Anim Reload
}

void AWeaponDefault::FinishReload()
{
	WeaponReloading = false;

	//�����������, ��� int8

	int32 AviableAmmoFromInventory = GetAviableAmmoForReload();
	int32 AmmoNeedTakeFromInv;
	int32 NeedToReload = WeaponSetting.MaxRound - AdditionalWeaponInfo.Round;

	//AdditionalWeaponInfo.Round = WeaponSetting.MaxRound;

	if (NeedToReload > AviableAmmoFromInventory)
	{
		AdditionalWeaponInfo.Round += AviableAmmoFromInventory; // += !!! ���������� �������! � �� ������������!
		AmmoNeedTakeFromInv = AviableAmmoFromInventory;
	}
	else
	{
		AdditionalWeaponInfo.Round += NeedToReload;
		AmmoNeedTakeFromInv = NeedToReload;
	}
	OnWeaponReloadEnd.Broadcast(true, -AmmoNeedTakeFromInv);
}

void AWeaponDefault::CancelReload()
{
	WeaponReloading = false;
	if (SkeletalMeshWeapon && SkeletalMeshWeapon->GetAnimInstance())
		SkeletalMeshWeapon->GetAnimInstance()->StopAllMontages(0.15f);

	//����� ���� �������� ����, �� �� ������� �������.
	//OnWeaponReloadEnd.Broadcast(false, 0);
	OnWeaponReloadEnd.Broadcast(false, AdditionalWeaponInfo.Round);
	DropMagazineFlag = false;
}

bool AWeaponDefault::CheckCanWeaponReload()
{
	bool result = true;
	if (GetOwner())
	{
		UXiInventoryComponent* MyInv = Cast<UXiInventoryComponent>(GetOwner()->GetComponentByClass(UXiInventoryComponent::StaticClass()));
		if (MyInv)
		{
			int32 AviableAmmoForWeapon;
			if (!MyInv->GetAmmoForWeapon(WeaponSetting.AmmoType, AviableAmmoForWeapon))
			{
				MyInv->OnWeaponIsEmpty.Broadcast(WeaponSetting.WeaponType,true);
				result = false;
			}
		}
	}

	return result;
}

int32 AWeaponDefault::GetAviableAmmoForReload()
{
	int32 AviableAmmoForWeapon = WeaponSetting.MaxRound;
	if (GetOwner())
	{
		UXiInventoryComponent* MyInv = Cast<UXiInventoryComponent>(GetOwner()->GetComponentByClass(UXiInventoryComponent::StaticClass()));
		if (MyInv)
		{
			if (MyInv->GetAmmoForWeapon(WeaponSetting.AmmoType, AviableAmmoForWeapon))
			{
				//UE_LOG(LogTemp, Warning, TEXT("AWeaponDefault::GetAviableAmmoForReload %i"), AviableAmmoForWeapon);
				//AviableAmmoForWeapon = AviableAmmoForWeapon; //�������, MyInv->CheckAmmoForWeapon ��� ����������� AviableAmmoForWeapon
				//UE_LOG(LogTemp, Warning, TEXT("after AWeaponDefault::GetAviableAmmoForReload %i"), AviableAmmoForWeapon);
			}
		}
	}
	return AviableAmmoForWeapon;
}

//void AWeaponDefault::InitDropMesh(UStaticMesh* DropMesh, FTransform Offset, FVector DropImpulseDirection, float LifeTimeMesh, float ImpulseRandomDispersion, float PowerImpulse, float CustomMass)


void AWeaponDefault::InitDropMesh(FDropMashInfo WeaponDropMesh)
{
	//InitDropMesh(WeaponDropMesh.DropMesh, WeaponDropMesh.DropMeshOffset, WeaponDropMesh.DropMeshImpulseDir, WeaponDropMesh.DropMeshLifeTime, WeaponDropMesh.ImpulseRandomDispersion, WeaponDropMesh.PowerImpulse, WeaponDropMesh.CustomMass);

	//CreateDefaultSubobject() Not use

	//Not actor for abstract object
	//if (WeaponSetting.MagazineDrop)
	//{
	//	UStaticMeshComponent* newStaticMesh = NewObject<UStaticMeshComponent>(this, FName("DropClipStaticMesh"));
	//	if (newStaticMesh)
	//	{
	//		newStaticMesh->SetStaticMesh(WeaponSetting.MagazineDrop);		
	//		//...
	//	}
	//}

	if (WeaponDropMesh.DropMesh)
	{
		FTransform Transform;

		Transform = WeaponDropMesh.DropMeshOffset;

//		FVector LocalDir = WeaponDropMesh.DropMeshOffset.GetLocation(); // ??

		/*FVector LocalDir = this->GetActorForwardVector() * WeaponDropMesh.DropMeshOffset.GetLocation().X + this->GetActorRightVector() * WeaponDropMesh.DropMeshOffset.GetLocation().Y + this->GetActorUpVector() * WeaponDropMesh.DropMeshOffset.GetLocation().Z;

		Transform.SetLocation(GetActorLocation() + LocalDir);
		Transform.SetScale3D(WeaponDropMesh.DropMeshOffset.GetScale3D());

		Transform.SetRotation((GetActorRotation() + WeaponDropMesh.DropMeshOffset.Rotator()).Quaternion());*/

		AStaticMeshActor* NewActor = nullptr;


		FActorSpawnParameters Param;
		Param.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;
		Param.Owner = this;
		NewActor = GetWorld()->SpawnActor<AStaticMeshActor>(AStaticMeshActor::StaticClass(), Transform, Param);

		if (NewActor && NewActor->GetStaticMeshComponent())
		{
//			NewActor->InitialLifeSpan = WeaponDropMesh.DropMeshLifeTime;
			NewActor->GetStaticMeshComponent()->SetCollisionProfileName(TEXT("IgnoreOnlyPawn"));
			NewActor->GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);

			//set parameter for new actor
			NewActor->SetActorTickEnabled(false);
			NewActor->SetLifeSpan(WeaponDropMesh.DropMeshLifeTime);
//			NewActor->InitialLifeSpan = WeaponDropMesh.DropMeshLifeTime; //DON'T WORK!!!!!!


			NewActor->GetStaticMeshComponent()->Mobility = EComponentMobility::Movable;
			NewActor->GetStaticMeshComponent()->SetSimulatePhysics(true);
			NewActor->GetStaticMeshComponent()->SetStaticMesh(WeaponDropMesh.DropMesh);
			NewActor->GetStaticMeshComponent()->SetCastShadow(false);
			//NewActor->GetStaticMeshComponent()->SetCollisionObjectType()



			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel1, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_GameTraceChannel2, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECollisionResponse::ECR_Ignore);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldStatic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_WorldDynamic, ECollisionResponse::ECR_Block);
			NewActor->GetStaticMeshComponent()->SetCollisionResponseToChannel(ECC_PhysicsBody, ECollisionResponse::ECR_Block);



			if (WeaponDropMesh.CustomMass > 0.0f)
			{
				NewActor->GetStaticMeshComponent()->SetMassOverrideInKg(NAME_None, WeaponDropMesh.CustomMass, true);
			}

			if (!WeaponDropMesh.DropMeshImpulseDir.IsNearlyZero())
			{
				FVector FinalDir;
//				LocalDir = LocalDir + (WeaponDropMesh.DropMeshImpulseDir * 1000.0f); // ��-����� ��� ������
//				LocalDir = WeaponDropMesh.DropMeshImpulseDir * 1000.0f;
//				FinalDir = LocalDir + (WeaponDropMesh.DropMeshImpulseDir * 1000.0f);

				if (!FMath::IsNearlyZero(WeaponDropMesh.ImpulseRandomDispersion))
				{
//					FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(FinalDir, WeaponDropMesh.ImpulseRandomDispersion);
					//FinalDir += UKismetMathLibrary::RandomUnitVectorInConeInDegrees(LocalDir, ImpulseRandomDispersion); // ��-����� ��� ���� ������
					FinalDir = UKismetMathLibrary::RandomUnitVectorInConeInDegrees((WeaponDropMesh.DropMeshImpulseDir * 1000.0f), WeaponDropMesh.ImpulseRandomDispersion); 
				}
				FinalDir.GetSafeNormal(0.0001f);

				NewActor->GetStaticMeshComponent()->AddImpulse(FinalDir * WeaponDropMesh.PowerImpulse);
			}
		}


	}

}