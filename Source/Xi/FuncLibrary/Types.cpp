// Fill out your copyright notice in the Description page of Project Settings.


#include "../FuncLibrary/Types.h"
#include "../Xi.h"

#include "../Interface/Xi_IGameActor.h"

void UTypes::AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UXi_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName NameHitBone)
{
	if (SurfaceType != EPhysicalSurface::SurfaceType_Default && TakeEffectActor && AddEffectClass)
	{
		UXi_StateEffect* myEffect = Cast<UXi_StateEffect>(AddEffectClass->GetDefaultObject());
		if (myEffect)
		{
			bool bIsHavePossibleSurface = false;
			int32 i = 0;
			while (i < myEffect->PossibleInteractSurface.Num() && !bIsHavePossibleSurface)
			{
				if (myEffect->PossibleInteractSurface[i] == SurfaceType)
				{
					bIsHavePossibleSurface = true;
					int32 IndexActiveEffect = 0;
					IXi_IGameActor* myInterface = Cast<IXi_IGameActor>(TakeEffectActor);
					if (!myEffect->bIsStakable)
					{
						UXi_StateEffect* ActiveEffect = nullptr;
						if (myInterface)
						{
							ActiveEffect = myInterface->GetActiveEffect(AddEffectClass);
							if (ActiveEffect)
							{
								ActiveEffect->InitObject(TakeEffectActor);
							}
							else
							{
								if (myInterface)
								{
									myInterface->SetBoneName(NameHitBone);
								}
								UXi_StateEffect* NewEffect = NewObject<UXi_StateEffect>(TakeEffectActor, AddEffectClass); //������ FName("Effect"), ����� ��� ������� �������������� ��� ���� ������
								if (NewEffect)
								{
									NewEffect->InitObject(TakeEffectActor);
								}
							}
						}
					}
					else
					{
						if (myInterface)
						{
							myInterface->SetBoneName(NameHitBone);
						}
						UXi_StateEffect* NewEffect = NewObject<UXi_StateEffect>(TakeEffectActor, AddEffectClass); //������ FName("Effect"), ����� ��� ������� �������������� ��� ���� ������
						if (NewEffect)
						{
							NewEffect->InitObject(TakeEffectActor);
						}
					}
				}				
				i++;
			}
		}

	}
}