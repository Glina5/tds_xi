// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Engine/DataTable.h"
#include "../StateEffects/Xi_StateEffect.h"

#include "Types.generated.h"

// ������ �������� ������������
UENUM(BlueprintType)
enum class EMovementState : uint8
{
	// DisplayName - �������� ������������ � ���������
	Aim_State UMETA(DisplayName = "Aim_State"),
	Walk_State UMETA(DisplayName = "Walk_State"),
	AimWalk_State UMETA(DisplayName = "AimWalk_State"),
	Run_State UMETA(DisplayName = "Run_State"),
	Sprint_State UMETA(DisplayName = "Sprint_State"),
};

//������ ������
UENUM(BlueprintType)
enum class EWeaponType : uint8
{
	PistolType UMETA(DisplayName = "Pistol"),
	RifleType UMETA(DisplayName = "Rifle"),
	ShotGunType UMETA(DisplayName = "Shotgun"),
	SniperRifle UMETA(DisplayName = "SniperRifle"),
	GrenadeLauncher UMETA(DisplayName = "GrenadeLauncher"),
	RocketLauncher UMETA(DisplayName = "RocketLauncher")
};

//������ �����������
UENUM(BlueprintType)
enum class EAmmoType : uint8
{
	PistolAmmo UMETA(DisplayName = "PistolAmmo"),
	RifleAmmo UMETA(DisplayName = "RifleAmmo"),
	ShotgunAmmo UMETA(DisplayName = "ShotgunAmmo"),
	SniperAmmo UMETA(DisplayName = "SniperRifleAmmo"),
	GrenadeLauncherAmmo UMETA(DisplayName = "GrenadeLauncherAmmo"),
	RocketLauncherAmmo UMETA(DisplayName = "RocketLauncherAmmo"),
	//�������
	MaxAmmoType
};

//������ ����������� ���������
UENUM(BlueprintType)
enum class EPortableType : uint8
{
	ShieldCharge UMETA(DisplayName = "ShieldCharge"),
	FirstAid UMETA(DisplayName = "FirstAid"),
	Bomb UMETA(DisplayName = "Bomb"),
	AmmoBox UMETA(DisplayName = "AmmoBox"),
	Keys UMETA(DisplayName = "Keys"),
	Scrap UMETA(DisplayName = "Scrap"),
	//�������
	MaxPortableType UMETA(Hidden)
};

//������ ����� �������
UENUM(BlueprintType)
enum class EModulesType : uint8
{
	EmptyModule UMETA(DisplayName = "Empty Module"),
	DamageModule UMETA(DisplayName = "Damage Module"),
	RoundModule UMETA(DisplayName = "Round Module"),
	RateOfFireModule UMETA(DisplayName = "RateOfFire Module"),
	ErgonomicsModule UMETA(DisplayName = "Ergonomics Module"),
	ReloadSpeedModule UMETA(DisplayName = "ReloadSpeed Module"),
	NumberProjectileModule UMETA(DisplayName = "NumberProjectile Module"),
	PrecisionModule UMETA(DisplayName = "Precision Module"),
	EffectModule UMETA(DisplayName = "Effect Module"),
	AimerModule UMETA(DisplayName = "Aimer Module"),
	HealerModule UMETA(DisplayName = "Healer Module"),
	//�������
	MaxModulesType UMETA(Hidden)
};


//���������� � ������� ��� ������
USTRUCT(BlueprintType)
struct FWeaponModulesInfo : public FTableRowBase
{
	GENERATED_BODY()

		//��� ������ (RowName �� DataTable ������ WeaponInfo)
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		FName WeaponCompatibleName = FName(TEXT("Common"));

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
	FText WeaponModuleDescription = FText::FromString("Empty weapon module description!");

	//������� � ��������� � ������ ����
//	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WeaponModules")
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		int32 PlayersHaveTier = 0;

	//����� ���� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		EModulesType CurrentModuleType = EModulesType::EmptyModule;

	//��������� ����� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		float Tier0 = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		float Tier1 = 1.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		float Tier2 = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponModules")
		float Tier3 = 3.0f;

};

//���������� � �������� ��� �� ������
USTRUCT(BlueprintType)
struct FAdditionalWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponStats")
		int32 Round = 10;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModuleStats")
		TArray<FWeaponModulesInfo> PlacingWeaponModules;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModuleStats")
		int32 WeaponSlotModulesNumber = 0;
	//������ � ��������� ������� ��� ������� ������ � ���������
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "ModuleStats")
		TArray<int32> CurrentWeaponModulesIndex;
};

// ��������� ����� ������
USTRUCT(BlueprintType)
struct FWeaponSlot
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FName NameItem;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WeaponSlot")
		FAdditionalWeaponInfo AdditionalInfo; // ���� ���������� � �������� ������ � ���������
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "WeaponSlot")
		bool bIsEmpty = false; //���� ���� � ��������� ����������� ��������, �� �� � ������
};

// ��������� ����� ������
USTRUCT(BlueprintType)
struct FAmmoSlot
{
	GENERATED_BODY()

		///Index Slot by Index Array
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		EAmmoType AmmoType = EAmmoType::RifleAmmo;
		//EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 Cout = 100; // ������� � ��������� ��������� ���� ������� �������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AmmoSlot")
		int32 MaxCout = 100; // ����� �������� ������� � ���������
};

// �������� �������� ������������
USTRUCT(BlueprintType)
struct FCharacterSpeed
{
	GENERATED_BODY()
	// EditAnywhere - ����� � ���������, BlueprintReadWrite - ����� ������������� � ���������, Category - �������� ��������� � ����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimSpeed = 300.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float WalkSpeed = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float AimWalkSpeed = 100.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float RunSpeed = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SprintSpeed = 800.0f;
};

//���������� � ����� � �������
USTRUCT(BlueprintType)
struct FProjectileInfo 
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	TSubclassOf<class AProjectileDefault> Projectile = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		UStaticMesh* ProjectileStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		FTransform ProjectileStaticMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		UParticleSystem* ProjectileTrailFx = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		UParticleSystem* ProjectileFireTraceFx = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		FTransform ProjectileTrailFxOffset = FTransform();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
	float ProjectileDamage = 20.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		float ProjectileLifeTime = 20.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Setting")
		float ProjectileInitSpeed = 2000.0f;

	//material to decal on hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		TMap<TEnumAsByte<EPhysicalSurface>, UMaterialInterface*> HitDecals;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		FVector DecalsSize = FVector(5.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		float DecalsLifeSpan = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		float DecalsFadeScreenSize = 0.001f; // 0.01f Default
	//Sound when hit
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		TMap < TEnumAsByte<EPhysicalSurface>, USoundBase*> HitSound; // ���� ����� ������ ������ �����, �� ������� ���� TMap
	//fx when hit check by surface
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hit")
		TMap<TEnumAsByte<EPhysicalSurface>, UParticleSystem*> HitFXs;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Effect")
		TSubclassOf<UXi_StateEffect> Effect = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		UParticleSystem* ExploseFX = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		USoundBase* ExploseSound = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMaxRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ProjectileMinRadiusDamage = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExploseMaxDamage = 40.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExploseMinDamage = 8.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Explode")
		float ExplodeFalloffCoef = 1.0f;
	//Timer add
};

USTRUCT(BlueprintType)
struct FWeaponDispersion
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Stay_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Stay_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Stay_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Stay_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_AddDispersionAimMax = -2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_AddDispersionAimMin = -0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_AddDispersionAimRecoil = -1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_AddDispersionReduction = -0.3f;

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_AddDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_AddDispersionAimMin = 0.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_AddDispersionAimRecoil = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_AddDispersionReduction = -0.1f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_AddDispersionAimMax = 8.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_AddDispersionAimMin = 3.7f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_AddDispersionAimRecoil = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_AddDispersionReduction = -0.2f;

	/*
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimMin = 0.3f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Aim_StateDispersionReduction = 0.3f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimMin = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float AimWalk_StateDispersionReduction = 0.4f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimMax = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimMin = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Walk_StateDispersionReduction = 0.2f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimMax = 10.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimMin = 4.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionAimRecoil = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float Run_StateDispersionReduction = 0.1f;
	*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float FirstShotDispersionRecoil = 3.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion ")
		float RatioHorizontalToVerticalDispersion = 3.f;
};

//��������
USTRUCT(BlueprintType)
struct FAnimationWeaponInfo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
		UAnimMontage* AnimCharFire = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
		UAnimMontage* AnimCharFireAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
		UAnimMontage* AnimCharReload = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Char") //����� �������� ����������� � ������������
	//	UAnimMontage* AnimCharReloadAim = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		UAnimMontage* AnimWeaponReload = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim Weapon")
	//	UAnimMontage* AnimWeaponReloadAim = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon")
		UAnimMontage* AnimWeaponFire = nullptr;
};

// ���� �� ����� ���
USTRUCT(BlueprintType)
struct FDropItem : public FTableRowBase
{
	GENERATED_BODY()

		///Index Slot by Index Array
		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		UStaticMesh* WeaponStaticMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		USkeletalMesh* WeaponSkeletMesh = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropWeapon")
		FWeaponSlot WeaponInfo;
};

//������������ ����
USTRUCT(BlueprintType)
struct FDropMashInfo
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		UStaticMesh* DropMesh = nullptr;
	//0.0f immediately drop
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		float DropMeshTime = -1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		float DropMeshLifeTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		FTransform DropMeshOffset = FTransform();
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		FVector DropMeshImpulseDir = FVector(0.0f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		float PowerImpulse = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		float ImpulseRandomDispersion = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh ")
		float CustomMass = 0.0f;

};

//���������� � ������
USTRUCT(BlueprintType)
struct FWeaponInfo : public FTableRowBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Class")
		TSubclassOf<class AWeaponDefault> WeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Name")
		FName WeaponName = FName(TEXT("Empty"));
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool bIsFullyAutomatic = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float RateOfFire = 0.5f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		float ReloadTime = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 MaxRound = 10;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		int32 NumberProjectileByShot = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundReloadWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundRaiseUpWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundLowerDownWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundScopeInWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Sound")
		USoundBase* SoundScopeOutWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "FX")
		UParticleSystem* EffectFireWeapon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "LaserBeam")
		UParticleSystem* EffectLaserBeam = nullptr;
	//if null use trace logic (TSubclassOf<class AProjectileDefault> Projectile = nullptr)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projectile")
		FProjectileInfo ProjectileSetting;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float WeaponDamage = 20.0f; // ���� � � ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trace")
		float DistacneTrace = 2000.0f; // ��������� �� ������ �������� �����
	//one decal on all?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitEffect ")
		UDecalComponent* DecalOnHit = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Animation")
		FAnimationWeaponInfo AnimWeaponInfo;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	//	UAnimMontage* AnimCharFire = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	//	UAnimMontage* AnimCharReload = nullptr;
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Anim ")
	//	UAnimMontage* AnimWeaponReload = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FDropMashInfo MagazineDrop;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DropMesh")
		FDropMashInfo ShellBullets;

	//����� ������ ����� �� �������� ��� ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DebugZone")
		float SizeVectorToChangeShootDirectionLogic = 130.0f;
	//�������� ������� ������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		bool TurnOnDispersion = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Dispersion")
		FWeaponDispersion DispersionWeapon;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Aim")
		bool bIsOpticalSight = false;

	//inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		float SwitchTimeToWeapon = 1.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		UTexture2D* WeaponIcon = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EWeaponType WeaponType = EWeaponType::RifleType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		EAmmoType AmmoType = EAmmoType::RifleAmmo;

		//������� �������� ���������� ������� ��� ����� ������. �������� 10?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Modules")
		int32 CurrentWeaponModulesNumber = 0;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Modules")
		int32 NumberTypesWeaponModules = static_cast<int32>(EModulesType::MaxModulesType);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Modules")
		float WeaponScopeScaleAim = 1000.f;
};

//��������� ����������� ���������
USTRUCT(BlueprintType)
struct FPortableSlot
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PortableSlot")
		EPortableType PortableType = EPortableType::FirstAid;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PortableSlot")
		int32 Cout = 1; // ������� � ��������� ��������� ��������� ������� ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PortableSlot")
		int32 MaxCout = 3; // ����� �������� ������� � ���������

};


UCLASS()
class XI_API UTypes : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:

	UFUNCTION(BlueprintCallable)
	static void AddEffectBySurfaceType(AActor* TakeEffectActor, TSubclassOf<UXi_StateEffect> AddEffectClass, EPhysicalSurface SurfaceType, FName NameHitBone);
};
