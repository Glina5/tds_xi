// Fill out your copyright notice in the Description page of Project Settings.


#include "../Interface/Xi_IGameActor.h"

// Add default functionality here for any IXi_IGameActor functions that are not pure virtual.

EPhysicalSurface IXi_IGameActor::GetSurfuceType()
{
	return EPhysicalSurface::SurfaceType_Default;
}

FName IXi_IGameActor::GetBoneName()
{
	return NAME_None;
}

void IXi_IGameActor::SetBoneName(FName NameHitBone)
{

}

float IXi_IGameActor::GetEffectScale()
{
	return 1.f;
}

USceneComponent* IXi_IGameActor::GetObjectMesh()
{
	return nullptr;
}

TArray<UXi_StateEffect*> IXi_IGameActor::GetAllCurrentEffects()
{
	TArray<UXi_StateEffect*> Effect;
	return Effect;
}

UXi_StateEffect* IXi_IGameActor::GetActiveEffect(TSubclassOf<UXi_StateEffect> AddEffectClass)
{
	return nullptr;
}

void IXi_IGameActor::RemoveEffect(UXi_StateEffect* RemoveEffect)
{

}

void IXi_IGameActor::AddEffect(UXi_StateEffect* newEffect)
{

}

void IXi_IGameActor::SwitchStunEffect(bool OnStun)
{

}
