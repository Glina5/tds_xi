// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class Xi : ModuleRules
{
	public Xi(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

        PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay", "NavigationSystem", "AIModule", "PhysicsCore", "Slate", "SlateCore", "ApplicationCore", "UMG" });
    }
}
