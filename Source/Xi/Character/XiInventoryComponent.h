// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../FuncLibrary/Types.h"
#include "XiInventoryComponent.generated.h"

/*USTRUCT(BlueprintType)
struct FWeaponSlotsWithNoAmmo
{
	GENERATED_BODY()

		UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		TArray<FWeaponSlot> Slots;
};*/

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnSwitchWeapon, FName, WeaponIdName, FAdditionalWeaponInfo, WeaponAdditionalInfo, int32, NewCurrentIndexWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoChange, EAmmoType, TypeAmmo, int32, Cout);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAdditionalInfoChange, FName, CurrentWeaponName, FAdditionalWeaponInfo, AdditionalInfo);
//DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAmmoEmpty, EAmmoType, TypeAmmo, FWeaponSlotsWithNoAmmo, EmptyWeapon);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnAmmoIsEmpty, EAmmoType, TypeAmmo, bool, bIsAmmoEmpty);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponIsEmpty, EWeaponType, TypeWeapon, bool, bIsWeaponEmpty);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnWeaponAmmoAviable, FName, CurrentWeaponName,int32,CurrentIndex);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnUpdateWeaponSlots, int32, IndexSlotChange, FWeaponSlot, NewWeaponSlotInfo, FWeaponInfo, NewInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnShowUIWeaponInfo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnSelectEmptyWeapon, EWeaponType, SelectedWeapon);

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class XI_API UXiInventoryComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UXiInventoryComponent();

	FOnSwitchWeapon OnSwitchWeapon; //1
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoChange OnAmmoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAdditionalInfoChange OnWeaponAdditionalInfoChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnAmmoIsEmpty OnAmmoIsEmpty;
//		FOnWeaponAmmoEmpty OnWeaponAmmoEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponIsEmpty OnWeaponIsEmpty;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnWeaponAmmoAviable OnWeaponAndAmmoAvailable;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnUpdateWeaponSlots OnUpdateWeaponSlots;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnShowUIWeaponInfo OnShowUIWeaponInfo;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Inventory")
		FOnSelectEmptyWeapon OnSelectEmptyWeapon;

private:

	// ������������ ���������� ������� ������� ����� ����� �������� � ����
//	const int32 TotalNumberOfWeaponModules = 300;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	const int32 MaxInventorySlotsWeaponAndAmmo = 9;

	TArray<FWeaponModulesInfo> AllWeaponModules;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory|Weapons")
		TArray<FWeaponSlot> WeaponSlots; //1
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory|Ammo")
		TArray<FAmmoSlot> AmmoSlots; //��������� ������������� ��� �������� WeaponSlots 
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory|Ammo")
		TArray<FAmmoSlot> StartAmmoForCurrentWeapon; // ��������� ���������� ��������� �������� �����������
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory|Weapons")
		TArray<FWeaponSlot> StartWeaponSlots; //1
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Inventory|Portable")
		TArray<FPortableSlot> StartPortableSlots; // ��������� ���������� ��������� �������� ��� ����������� ���������
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Inventory|Portable")
		TArray<FPortableSlot> PortableSlots; 

//	int32 MaxSlotsWeapon = 0; //1


	//TODO OMG Refactoring need!!!
	bool IsSwitchWeaponToIndex(int32 ChangeToIndex, int32& CorrectChangeeToIndex, int32 OldIndex, bool bIsForward, bool bIsSelectedWeapon = false); // 1
	void SwitchWeaponToIndex(int32 CorrectChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo);

	FAdditionalWeaponInfo GetAdditionalInfoWeapon(int32 IndexWeapon); //1
	int32 GetWeaponIndexSlotByName(FName IdWeaponName); //1
	FName GetWeaponNameBySlotIndex(int32 indexSlot);
	void SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo); //1

	UFUNCTION(BlueprintCallable)
	void ChangeWeaponSlotsIsEmpty(EAmmoType TypeAmmo, bool NewStateEmpty);

	//TODO ���� ������
	UFUNCTION(BlueprintCallable)
	void CheckWeaponIsEmpty(EWeaponType TypeWeapon);

	//���������� ������ ���� ������ ���������� � ������
	UFUNCTION(BlueprintCallable)
	TArray<FName> GetAvaliableWeaponName();

	//���������� ������ � ���������� �������� � ��������� ������
	UFUNCTION(BlueprintCallable)
	TArray<FWeaponModulesInfo> GetChosenWeaponModules(FName WeaponName);

	//���������� ������ � ���������� �������� ��� ���������� ������
	UFUNCTION(BlueprintCallable)
	TArray<FWeaponModulesInfo> GetAvaliableWeaponModules(FName WeaponName);

	//���������� ������ � ������
	UFUNCTION(BlueprintCallable)
	bool SetModuleToWeaponInCell(FName WeaponName, EModulesType NewModuleType, int32 ModuleCellNumber);

	//�������� ��������� �� ����� �� ����� ��������
	UFUNCTION(BlueprintCallable)
	TArray<FWeaponModulesInfo> GetAllWeaponModules();




	UFUNCTION(BlueprintCallable)
		bool AmmoSlotChangeValue(EAmmoType TypeAmmo, int32 CoutChangeAmmo);
		//void AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo);

//	void InitAmmoWithWeapon(EWeaponType TypeWeapon, FAdditionalWeaponInfo CurrentAdditionalWeaponInfo); // ��������� �� �����

	bool GetAmmoForWeapon(EAmmoType TypeAmmo, int32& AviableAmmForWeapon);

//	bool GetAllWeaponWithCurrentAmmo(EAmmoType TypeAmmo, FWeaponSlotsWithNoAmmo& WeaponWithCurrentAmmo); 

	//Interface PickUp Actors
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeAmmo(EAmmoType TypeAmmo); // TO DO
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeapon(EWeaponType TypeWeapon);
		//bool CheckCanTakeWeapon(int32& FreeSlot);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool SwitchWeaponToInventory(FWeaponSlot NewWeapon, EWeaponType TypeWeapon, FDropItem& DropItemInfo);
	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool TryGetWeaponToInventory(FWeaponSlot NewWeapon);

	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool CheckCanTakeWeaponUpdate(EWeaponType TypeWeapon, FWeaponSlot NewWeaponSlotInfo);


	UFUNCTION(BlueprintCallable, Category = "Interface")
		bool GetDropItemInfoFromInventory(EWeaponType TypeWeapon, FDropItem& DropItemInfo);

	UFUNCTION(BlueprintCallable, Category = "Interface")
		FPortableSlot GetPortableSlotByEnum(EPortableType TypePortable);

	bool ChangePortableCoutByEnum(EPortableType TypePortable); // ����� ���� cout


		
};
