// Fill out your copyright notice in the Description page of Project Settings.


#include "../Character/XiCharacterHealthComponent.h"

void UXiCharacterHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (ChangeValue > 0.0f)
	{
		Super::ChangeHealthValue(ChangeValue);
	}
	else if (!Immortal)
	{
		if (ShieldExist)
		{
			LaunchShield();
		}
		if (!ShieldDestroyed && ShieldExist)
		{
			float CurrentDamage = ChangeValue * ShieldCoefDamage;
			ChangeShieldValue(CurrentDamage);
			if (Shield < 0.0f)
			{
				//FX
				//UE_LOG(LogTemp, Warning, TEXT("UXiCharacterHealthComponent::ChangeHealthValue - Sheild < 0"));
			}
		}
		else
		{
			if (ShieldExist)
			{
				SetCurrentShield(0.0f);
				OnShieldChange.Broadcast(Shield, -1.0f); // for visual
			}
			if (Armor > 0.0f)
			{
				ChangeArmorValue(ChangeValue);
			}
			else
			{
				Super::ChangeHealthValue(ChangeValue);
			}
		}
	}
}

float UXiCharacterHealthComponent::GetCurrentShield()
{
	return Shield;
}

void UXiCharacterHealthComponent::SetCurrentShield(float NewShield)
{
	if (NewShield > 100.0f)
	{
		Shield = 100.0f;
	}
	else if (NewShield < 0.0f)
	{
		Shield = 0.0f;
	}
	else
	{
		Shield = NewShield;
	}
}

void UXiCharacterHealthComponent::ChangeShieldValue(float ChangeValue)
{
	Shield += ChangeValue;

	if (Shield > 100.0f)// ���� ������ �� ����
	{
		Shield = 100.0f;
	}
	else
	{
		if (Shield < 0.0f)
		{
			Shield = 0.0f;
		ShieldDestroyed = true;
		OnShieldDestroyed.Broadcast(ShieldDestroyed);
		}
	}

	OnShieldChange.Broadcast(Shield, ChangeValue);
}

void UXiCharacterHealthComponent::CoolDownShieldEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_ShieldRecoveryRateTimer, this, &UXiCharacterHealthComponent::RecoveryShield, ShieldRecoverRate, true);
	}
}

void UXiCharacterHealthComponent::RecoveryShield()
{
	float tmp = Shield;
	tmp = tmp + ShieldRecoverValue;
	if (tmp > 100.0f)
	{
		Shield = 100.0f;
		ShieldDestroyed = false;
		OnShieldDestroyed.Broadcast(ShieldDestroyed);
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		}
	}
	else
	{
		Shield = tmp;
	}

	OnShieldChange.Broadcast(Shield, ShieldRecoverValue);
}

bool UXiCharacterHealthComponent::GetShieldDestroyed()
{
	return ShieldDestroyed;
}

void UXiCharacterHealthComponent::LaunchShield()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownShieldTimer, this, &UXiCharacterHealthComponent::CoolDownShieldEnd, CoolDownShieldRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
	}
	//add Broadcast to Widget Hidden Shieldd Bar
}

bool UXiCharacterHealthComponent::GetShieldExist()
{
	return ShieldExist;
}

void UXiCharacterHealthComponent::SetShieldExist(bool NewShieldExist)
{
	ShieldExist = NewShieldExist;
	OnShieldExist.Broadcast(ShieldExist);
	if (ShieldExist)
	{
		ShieldDestroyed = true;
		SetCurrentShield(0.0f);
		OnShieldChange.Broadcast(Shield, -1.0f);
	}
}

void UXiCharacterHealthComponent::ClearShieldTimer()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_ShieldRecoveryRateTimer);
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_CollDownShieldTimer);
	}
}

float UXiCharacterHealthComponent::GetCurrentArmor()
{
	return Armor;
}

void UXiCharacterHealthComponent::SetCurrentArmor(float NewArmor)
{
	if (NewArmor > 100.0f)
	{
		Armor = 100.0f;
	}
	else
	{
		Armor = NewArmor;
	}
}

void UXiCharacterHealthComponent::ChangeArmorValue(float ChangeValue)
{
	float CorrectArmorDamage = ChangeValue;
	if (ChangeValue > 0.0f)
	{
		Armor += ChangeValue;
	}
	else
	{
		float ArmorDamage = ChangeValue * ArmorCoefDivision;
		CorrectArmorDamage = ArmorDamage * CoefDamage;

		if (Armor < CorrectArmorDamage)
		{
			ArmorDamage = Armor / CoefDamage;
			Armor = 0.0f;
		}
		else
		{
			Armor += CorrectArmorDamage;
		}

		Super::ChangeHealthValue(ChangeValue - ArmorDamage);
	}

	if (Armor > 100.0f)
	{
		Armor = 100.0f;
	}
	else
	{
		if (Armor < 0.0f)
		{
			Armor = 0.0f;
		}
	}

		OnArmorChange.Broadcast(Armor, CorrectArmorDamage);
}

float UXiCharacterHealthComponent::GetCurrentStamina()
{
	return Stamina;
}

void UXiCharacterHealthComponent::SetCurrentStamina(float NewStamina)
{
	if (NewStamina > 100.0f)
	{
		Stamina = 100.0f;
	}
	else
	{
		Stamina = NewStamina;
	}
}

void UXiCharacterHealthComponent::StartChangeStamina()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_StartChangeStaminaTimer, this, &UXiCharacterHealthComponent::ChangeStaminaValue, StaminaDeclineRateValue, true);
	}
}

void UXiCharacterHealthComponent::StopChangeStamina()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StartChangeStaminaTimer);
	}
}

void UXiCharacterHealthComponent::ChangeStaminaValue()
{
	Stamina -= StaminaDeclineValue;

	if (Stamina > 100.0f) // ������, �� ��
	{
		Stamina = 100.0f;
	}
	else if (Stamina < 0.0f)
	{
		Stamina = 0.0f;
		Exhausted = true;
		OnExhaustedChange.Broadcast(Exhausted);
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StartChangeStaminaTimer);
		}
	}

	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_CollDownStaminaTimer, this, &UXiCharacterHealthComponent::CoolDownStaminaEnd, CoolDownStaminaRecoverTime, false);

		GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
	}

	OnStaminaChange.Broadcast(Stamina, StaminaDeclineValue);
}

void UXiCharacterHealthComponent::CoolDownStaminaEnd()
{
	if (GetWorld())
	{
		GetWorld()->GetTimerManager().SetTimer(TimerHandle_StaminaRecoveryRateTimer, this, &UXiCharacterHealthComponent::RecoveryStamina, StaminaRecoverRate, true);
	}
}

void UXiCharacterHealthComponent::RecoveryStamina()
{
	float tmp = Stamina;
	tmp = tmp + StaminaRecoverValue;
	if (tmp > 100.0f)
	{
		Stamina = 100.0f;
		if (GetWorld())
		{
			GetWorld()->GetTimerManager().ClearTimer(TimerHandle_StaminaRecoveryRateTimer);
		}
		if (Exhausted)
		{
		Exhausted = false;
		}
	}
	else
	{
		Stamina = tmp;
		if (Exhausted && (Stamina > StaminaWalkingValue))
		{
		Exhausted = false;
		OnExhaustedChange.Broadcast(Exhausted);
		}
	}

	OnStaminaChange.Broadcast(Stamina, StaminaRecoverValue);
}
