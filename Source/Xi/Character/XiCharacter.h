// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "../Game/XiPlayerController.h"
#include "../FuncLibrary/Types.h"
#include "../Weapon/WeaponDefault.h"
#include "../Weapon/WeaponDefault_Medic.h"//TO DO!!
#include "../Character/XiInventoryComponent.h"
#include "../Character/XiCharacterHealthComponent.h"
#include "../Interface/Xi_IGameActor.h"
#include "../StateEffects/Xi_StateEffect.h"
//#include "Components/WidgetComponent.h"

#include "XiCharacter.generated.h"

//USTRUCT(BlueprintType)
//struct FCharacterSpeedInfo
//{
//	GENERATED_BODY()
//
//
//};

UCLASS(Blueprintable)
class AXiCharacter : public ACharacter, public IXi_IGameActor
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
		void CharacterTurnTick(float DeltaTime, AXiPlayerController* CurrentPlayerController, FHitResult CursorHit);

	bool bIsOpticalAiming = false;

		// todo �������� ������� ��� �������� ������	

public:
	AXiCharacter();

	FTimerHandle TimerHandle_RagDollTimer;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	// ������������ ��������� (��������)
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
//	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; } //������� ���������� �� Forceinline

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Inventory", meta = (AllowPrivateAccess = "true"))
		class UXiInventoryComponent* InventoryComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Health", meta = (AllowPrivateAccess = "true"))
		class UXiCharacterHealthComponent* HealthComponent;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Shield", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* StaticMeshShield = nullptr;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Target", meta = (AllowPrivateAccess = "true")) // todo 2
		class UWidgetComponent* TargetWidgetComponent;

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	/*UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;*/

	bool bIsStun = false;

	AActor* AimingActor; // todo2
	float StartAimingTimer = 1.f; // todo2
	float AutoAimingTimer = 2.f; // todo2

	float CurrentTargetArmLenght = 2000.f;
	FVector CameraAimMoveDistance = FVector::ZeroVector;
	bool bIsAimMoveCamera = false;
	bool bIsAimBackMoveCamera = false;
	float CameraToScopeSpeed = 10.f;

	bool bIsLaserBeamStart = false;

	bool bModuleAimingTarget = false;

	bool bIsCurrentCharacterMoving = false;


	int32 CameraViewportHeight = 0;
	int32 CameraViewportWidth = 0;

	float CurrentCursorPosX = 0.f;
	float CurrentCursorPosY = 0.f;


public:
	//Cursor
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		UMaterialInterface* CursorMaterial = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		FVector CursorSize = FVector(20.0f, 40.0f, 40.0f);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		bool bSystemCursor = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		bool bReturnOpticalAimCursor = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cursor")
		float CurrentMouseSensotivity = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Camera")
		bool IsAim = false;

	UDecalComponent* CurrentCursor = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::Run_State;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FCharacterSpeed MovementInfo;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Movement")
		float LookAtCursorRotationYaw = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool MovementWalk = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool MovementAim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool MovementSprint = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		bool bIsAlive = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "State")
		TArray<UAnimMontage*> DeadsAnim;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ability")
		TSubclassOf<UXi_StateEffect> AbilityEffect = nullptr;

	//FlagAiming
	bool bAimingStartTarget = false; // todo2
	bool bSearchAimingTarget = false; // todo2
	bool bStopSearchAimingTarget = false; // todo2

	//Weapon	
	AWeaponDefault* CurrentWeapon = nullptr;

	////for demo 
	//UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Demo")
	//	TSubclassOf<AWeaponDefault> InitWeaponClass = nullptr;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "DemoDataTable")
	FName InitWeaponName;

	//Effects
	UPROPERTY()
	TArray<UXi_StateEffect*> Effects;
	FName MyNameHitBone;


	////Inventory
	//const int8 MaxInventorySlotsWeaponAndAmmo = 9;

	//��� �������
	UFUNCTION()
		void MovementTick(float DeltaTime);

	void AimingTargetTick(float DeltaTime);

	void OpticalAimingTick(float DeltaTime);

	void AimingLaserBeamTick(float DeltaTime);

	void NewMouseCursorTick(float DeltaTime);

	//������
	UFUNCTION(BlueprintCallable)
		void OffsetMouseCursorX(float DeltaCursorX, int32 CurrentViewportSize);
	UFUNCTION(BlueprintCallable)
		void OffsetMouseCursorY(float DeltaCursorY, int32 CurrentViewportSize);

	UFUNCTION(BlueprintCallable)
		void SetCharacterNewMouseInputSettings(FCharacterMouseInputSettings PlayerNewInputSetttings);

	//�������� � ������
	UFUNCTION(BlueprintCallable)
		void AttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		void AlternativeAttackCharEvent(bool bIsFiring);
	UFUNCTION(BlueprintCallable)
		AWeaponDefault* GetCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon);
	UFUNCTION(BlueprintCallable)//VisualOnly
		void RemoveCurrentWeapon();
	UFUNCTION(BlueprintCallable)
		void ReloadWeaponEvent();
	UFUNCTION()
		void WeaponReloadStart(UAnimMontage* AnimReload);
	UFUNCTION()
		void WeaponReloadEnd(bool bIsSuccess, int32 AmmoSafe); // ����� �������� ���������� bool Failed ���� ����� ��������� ��������
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadStart_BP(UAnimMontage* AnimReload);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponReloadEnd_BP(bool bIsSuccess);

	UFUNCTION()
		void WeaponFireStart(UAnimMontage* AnimFire);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponFireStart_BP(UAnimMontage* AnimFire);

	UFUNCTION()
		void WeaponAlternativeFireStart(UAnimMontage* AnimFire);
	UFUNCTION(BlueprintNativeEvent)
		void WeaponAlternativeFireStart_BP(UAnimMontage* AnimFire);

	void WeaponAutoAimingTarget(bool bIsEnemy); // todo2

	bool CheckAimingTarget(AActor* NewAimingTarget);

	void StopAutoAimingTarget();

	void StopWeaponAiming();

	void StartOpticalAiming();

	bool CheckIsOpticalAiming();

	void StartLaserBeam();

	void MouseSensitivityByDirection(float CummonMouseSensitivity, float NormalCameraOffset, float MouseDeltaDirection, float& MouseSensitivityDirection);

/*	UFUNCTION()
		void TryChangeSpringArm();
	UFUNCTION(BlueprintNativeEvent)
		void TryChangeSpringArm_BP();*/

	UFUNCTION()
		void TryChangeSpringArmUp();
	UFUNCTION(BlueprintNativeEvent)
		void TryChangeSpringArmUp_BP();
	UFUNCTION()
		void TryChangeSpringArmDown();
	UFUNCTION(BlueprintNativeEvent)
		void TryChangeSpringArmDown_BP();


	//������� �������� ����
	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementWalk();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementWalkToRun();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementAim();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementAimToRun();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementSprint();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementSprintToRun();
	UFUNCTION(BlueprintCallable)
		void ChangeMovementState(EMovementState NemMovementState);

	UFUNCTION(BlueprintCallable)
		bool GetIsCharacterMoving();
	UFUNCTION(BlueprintCallable)
		void SetIsCharacterMoving(bool NewMotion);

	//ability func
	void TryAbilityEnabled();

	//Effect Stun
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun")
	UAnimMontage* AnimCharStun = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun")
	UParticleSystem* CharStunParticleEffect = nullptr;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stun")
	float StunEffectParticalScale = 1.0f;
	UParticleSystemComponent* CharStunParticleEmitter = nullptr;

	//������� ��������� ������� �������� �� FORCEINLINE
	UFUNCTION(BlueprintCallable)
		UDecalComponent* GetCursorToWorld();

	//Inventory Func

	void TrySwicthNextWeapon();
	void TrySwitchPreviosWeapon();
	void TrySwitchToSelectWeapon(int32 SelectedWeapon);
	void TryPickUpItem();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly)
		int32 CurrentIndexWeapon = 0;

	//�����???
	////������� ������� ����� � ���������� ����� �������� � ����� ������������
	//UFUNCTION(BlueprintImplementableEvent, Category = "Camera")
	//	static void AimEvents(const bool& IsAim);


	//Interface
	//bool AviableForEffects_Implementation() override; //������ ��� ������� ��������� ���������������� � ���������

	EPhysicalSurface GetSurfuceType() override;
	FName GetBoneName() override;
	void SetBoneName(FName NameHitBone) override;
	USceneComponent* GetObjectMesh() override;
	TArray<UXi_StateEffect*> GetAllCurrentEffects() override;
	UXi_StateEffect* GetActiveEffect(TSubclassOf<UXi_StateEffect> AddEffectClass) override;
	void RemoveEffect(UXi_StateEffect* RemoveEffect)override;
	void AddEffect(UXi_StateEffect* newEffect)override;
	void SwitchStunEffect(bool OnStun)override;

	//End Interface

	UFUNCTION()
	void CharDead();

	UFUNCTION()
	void ExhaustedChangeMovement(bool bIsExhausted);

	void EnableRagdoll();
	virtual float TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser) override;

	UFUNCTION(BlueprintCallable)
	bool PickUpShield(int32 NumPickUpShieldCharge);

	UFUNCTION()
	void RestartShield(bool NewShieldDestroyed);

};

