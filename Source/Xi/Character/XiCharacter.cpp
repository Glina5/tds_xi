// Copyright Epic Games, Inc. All Rights Reserved.

#include "../Character/XiCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"

#include "Components/WidgetComponent.h"

#include "Components/StaticMeshComponent.h"
#include "../Weapon/ProjectileDefault.h"

#include "../Game/XiGameInstance.h"
#include "../Game/XiPlayerController.h"

#include "../Game/InputUserSettings.h"

#include "Materials/Material.h"
// ������ � ����������� 
#include "Kismet/GameplayStatics.h"
// ������������� ���������� ���������� ���������� Bluprint'��
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

AXiCharacter::AXiCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction // ��� ��� ��� �� �����?????
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	InventoryComponent = CreateDefaultSubobject<UXiInventoryComponent>(TEXT("InventoryComponent"));
	HealthComponent = CreateDefaultSubobject<UXiCharacterHealthComponent>(TEXT("HealthComponent"));

	StaticMeshShield = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Static Mesh Shield"));
	StaticMeshShield->SetGenerateOverlapEvents(false);
	StaticMeshShield->SetCollisionProfileName(TEXT("NoCollision"));
	StaticMeshShield->SetupAttachment(RootComponent);

	TargetWidgetComponent = CreateDefaultSubobject<UWidgetComponent>(TEXT("Target Widget")); // todo 2
	TargetWidgetComponent->SetCastShadow(false);
	TargetWidgetComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	TargetWidgetComponent->SetVisibility(false);

	if (HealthComponent)
	{
		HealthComponent->OnDead.AddDynamic(this, &AXiCharacter::CharDead);
		HealthComponent->OnExhaustedChange.AddDynamic(this, &AXiCharacter::ExhaustedChangeMovement);
		HealthComponent->OnShieldDestroyed.AddDynamic(this, &AXiCharacter::RestartShield);
	}
	if (InventoryComponent)
	{
		InventoryComponent->OnSwitchWeapon.AddDynamic(this, &AXiCharacter::InitWeapon);
	}
//	UE_LOG(LogTemp, Warning, TEXT("Character Constract"));

	// Create a decal in the world to show the cursor's location
	//
	//CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	//CursorToWorld->SetupAttachment(RootComponent);
	//static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal'"));
	//if (DecalMaterialAsset.Succeeded())
	//{
	//	CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	//}
	//CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	//CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;
}

void AXiCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	//if (CursorToWorld != nullptr)
	//{
	//	// �������� ��������� �� ������ (���� ��������� ����� ������ �������, �� ������ ����� ���������)
	//	MovementTick(DeltaSeconds);
	//	if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
	//	{
	//		if (UWorld* World = GetWorld())
	//		{
	//			FHitResult HitResult;
	//			FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
	//			FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
	//			FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
	//			Params.AddIgnoredActor(this);
	//			World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
	//			FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
	//			CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
	//		}
	//	}
	//	else if (APlayerController* PC = Cast<APlayerController>(GetController()))
	//	{
	//		FHitResult TraceHitResult;
	//		PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
	//		FVector CursorFV = TraceHitResult.ImpactNormal;
	//		FRotator CursorR = CursorFV.Rotation();
	//		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
	//		CursorToWorld->SetWorldRotation(CursorR);
	//	}
	//}
	if (!bSystemCursor)
	{
		NewMouseCursorTick(DeltaSeconds);
	}
	else
	{
		if (CurrentCursor)
		{

			APlayerController* myPC = Cast<APlayerController>(GetController());
			if (myPC)
			{
				FHitResult TraceHitResult;
				myPC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
				FVector CursorFV = TraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();

				CurrentCursor->SetWorldLocation(TraceHitResult.Location);
				CurrentCursor->SetWorldRotation(CursorR);
			}
		}
		// �������� ��������� �� ������ (���� ��������� ����� ������ �������, �� ������ ����� ���������)
		MovementTick(DeltaSeconds);
	}

	AimingTargetTick(DeltaSeconds);
	AimingLaserBeamTick(DeltaSeconds);
//	OpticalAimingTick(DeltaSeconds);

}

void AXiCharacter::BeginPlay()
{
	Super::BeginPlay();

	//InitWeapon(InitWeaponName);
//	UE_LOG(LogTemp, Warning, TEXT("Character Begin play"));
	if (CursorMaterial)
	{
		CurrentCursor = UGameplayStatics::SpawnDecalAtLocation(GetWorld(), CursorMaterial, CursorSize, FVector(0));
		AXiPlayerController* myController = Cast<AXiPlayerController>(GetController());
		if (myController)
		{
			myController->GetMousePosition(CurrentCursorPosX, CurrentCursorPosY);
			FCharacterMouseInputSettings PlayerInputSetttings;
			PlayerInputSetttings = myController->GetPlayerInputUserSettings();
			SetCharacterNewMouseInputSettings(PlayerInputSetttings);
		}
	}
	PickUpShield(-1); // ���� ������� � ��������� ������ ����, �� ��������� ���

	//�������� ������� �������� � �������� ������, ����� �������� ���������, ���� �������� ��������.
	// todo ���� ����� �����
}

void AXiCharacter::CharacterTurnTick(float DeltaTime, AXiPlayerController* CurrentPlayerController, FHitResult CursorHit)
{
	if (CurrentPlayerController)
	{
		//FVector CursorLocationWithCorrection = CursorHit.Location + (FVector(0.f, 0.f, this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z)/(CursorHit.TraceStart - CursorHit.Location).GetSafeNormal().Z); // HumanLenght/Cos(���� ������ �� ������). Cos = ��������� ������������ �������� �� ����� ��������. ��� ���������� � ������ �� ��� Z: 
		//FVector CursorLocationWithCorrection = CursorHit.Location + (FVector(0.f, 0.f, (this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z) - CursorHit.Location.Z) * UKismetMathLibrary::Sqrt(1.f/((((CursorHit.TraceStart - CursorHit.Location).GetSafeNormal()).Z)* (((CursorHit.TraceStart - CursorHit.Location).GetSafeNormal()).Z))-1.f)); // tg()
		//FVector CursorLocationWithCorrection = CursorHit.Location - (CursorHit.TraceStart - CursorHit.Location).GetSafeNormal().ToOrientationRotator().RotateVector((FVector(0.f, 0.f, (this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z) - CursorHit.Location.Z)) ); // Rotator
		FVector CursorLocationWithCorrection = CursorHit.Location + ( (CursorHit.TraceStart - CursorHit.Location).GetSafeNormal() * (this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z - CursorHit.Location.Z) ); // ��������
		//UE_LOG(LogTemp, Warning, TEXT("CursorLocationWithCorrection: X = %f. Y = %f. Z = %f"), CursorLocationWithCorrection.X, CursorLocationWithCorrection.Y, CursorLocationWithCorrection.Z);
		//UE_LOG(LogTemp, Warning, TEXT("CursorHit.Location: X = %f. Y = %f. Z = %f"), CursorHit.Location.X, CursorHit.Location.Y, this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z);

		// ����� ����������� ����� MakeRotFromX(const FVector& Start, const FVector& Target);
		//float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
		LookAtCursorRotationYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), CursorLocationWithCorrection).Yaw;
		//if (MovementState != EMovementState::Sprint_State) // todo ??
		//{
		SetActorRotation(FQuat(FRotator(0.0f, LookAtCursorRotationYaw, 0.0f)));
		//}

		FVector CharacterMovementVector = FVector::ZeroVector;
		CharacterMovementVector = GetLastMovementInputVector();
		CharacterMovementVector.IsNearlyZero(0.01F) ? bIsCurrentCharacterMoving = false : bIsCurrentCharacterMoving = true;
		AWeaponDefault* myWeapon = nullptr;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			CurrentWeapon->ShootEndLocation = FVector(CursorLocationWithCorrection.X, CursorLocationWithCorrection.Y, this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z); //  todo + Vector ������ ���, ����� ����� ������������� �� �����?? �����?? � ��� ��������� � ��������� � ������������.
			CurrentWeapon->bIsCharacterMoving = bIsCurrentCharacterMoving;
			/*
			if (bIsAimMoveCamera && myWeapon->WeaponSetting.bIsOpticalSight)
			{
				bIsOpticalAiming = true;
				//UE_LOG(LogTemp, Warning, TEXT("3. CurrentCursorLocation: X = %f. Y = %f"), CurrentCursorLocation.X, CurrentCursorLocation.Y);
				//UE_LOG(LogTemp, Warning, TEXT("3. CursorLocation: X = %f. Y = %f"), CurrentCursor->GetComponentLocation().X, CurrentCursor->GetComponentLocation().Y);

				FVector CurrentCursorLocation = FVector::ZeroVector;
				CurrentCursorLocation = CurrentCursor->GetComponentLocation();
				FVector CameraDistanceTarget = FVector::ZeroVector;
				FRotator CameraBoomRotation = FRotator(CameraBoom->GetTargetRotation().Pitch + 90.f, 0.f, 0.f); // todo
				CameraDistanceTarget = CameraBoom->GetComponentLocation() + CameraAimMoveDistance + (CameraBoomRotation.RotateVector(FVector(0.f, 0.f, CameraBoom->TargetArmLength)));
				
				FVector DeltaDistanceCamera = FVector::ZeroVector;
				DeltaDistanceCamera = UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetComponentLocation(), CameraDistanceTarget, DeltaTime, CameraToScopeSpeed);
				FVector DeltaDistanceCursor = FVector::ZeroVector;
				DeltaDistanceCursor = TopDownCameraComponent->GetComponentLocation() - DeltaDistanceCamera;


				TopDownCameraComponent->SetWorldLocation(DeltaDistanceCamera);
				if (!TopDownCameraComponent->GetComponentLocation().Equals(CameraDistanceTarget, 1.f))
				{
					if (bReturnOpticalAimCursor)
					{
						//UE_LOG(LogTemp, Warning, TEXT("0. CameraViewport: X = %d. Y = %d"), CameraViewportWidth, CameraViewportHeight);
						FVector2D NewCursorScreenLocation = FVector2D::ZeroVector;
						CurrentPlayerController->ProjectWorldLocationToScreen(CurrentCursorLocation + DeltaDistanceCursor, NewCursorScreenLocation, false);
						//UE_LOG(LogTemp, Warning, TEXT("1. NewCursorScreenLocation: X = %f. Y = %f"), NewCursorScreenLocation.X, NewCursorScreenLocation.Y);
						//UE_LOG(LogTemp, Warning, TEXT("2. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
						if (NewCursorScreenLocation.X < 0)
						{
							NewCursorScreenLocation.X = 0;
						}
						else if (NewCursorScreenLocation.X > static_cast<float>(CameraViewportWidth))
						{
							NewCursorScreenLocation.X = static_cast<float>(CameraViewportWidth);
						}
						CurrentCursorPosX = NewCursorScreenLocation.X;


						if (NewCursorScreenLocation.Y < 0)
						{
							NewCursorScreenLocation.Y = 0;
						}
						else if (NewCursorScreenLocation.Y > static_cast<float>(CameraViewportHeight))
						{
							NewCursorScreenLocation.Y = static_cast<float>(CameraViewportHeight);
						}
						CurrentCursorPosY = NewCursorScreenLocation.Y;
						//UE_LOG(LogTemp, Warning, TEXT("3. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
					}
				}
				else
				{
					UE_LOG(LogTemp, Warning, TEXT("STOP ------------------------------------------------------"));
					bIsAimMoveCamera = false;
				}
			}
			else
			{
				if (bIsAimBackMoveCamera)
				{
					FVector BackNewStartDistanceCamera = FVector::ZeroVector;
					FVector BackDeltaDistanceCamera = FVector::ZeroVector;
					FVector CurrentCursorLocation = CurrentCursor->GetComponentLocation();
					BackNewStartDistanceCamera = TopDownCameraComponent->GetComponentLocation();
					TopDownCameraComponent->SetRelativeLocation(UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetRelativeLocation(), FVector::ZeroVector, DeltaTime, CameraToScopeSpeed));

					if (bReturnOpticalAimCursor)
					{
						BackDeltaDistanceCamera = BackNewStartDistanceCamera - TopDownCameraComponent->GetComponentLocation();
						FVector2D NewCursorScreenLocation = FVector2D::ZeroVector;
						CurrentPlayerController->ProjectWorldLocationToScreen(CurrentCursorLocation + BackDeltaDistanceCamera, NewCursorScreenLocation, false);
						if (NewCursorScreenLocation.X < 0)
						{
							NewCursorScreenLocation.X = 0;
						}
						else if (NewCursorScreenLocation.X > static_cast<float>(CameraViewportWidth))
						{
							NewCursorScreenLocation.X = static_cast<float>(CameraViewportWidth);
						}
						CurrentCursorPosX = NewCursorScreenLocation.X;


						if (NewCursorScreenLocation.Y < 0)
						{
							NewCursorScreenLocation.Y = 0;
						}
						else if (NewCursorScreenLocation.Y > static_cast<float>(CameraViewportHeight))
						{
							NewCursorScreenLocation.Y = static_cast<float>(CameraViewportHeight);
						}
						CurrentCursorPosY = NewCursorScreenLocation.Y;

						//UE_LOG(LogTemp, Warning, TEXT("1. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
					}
					if (TopDownCameraComponent->GetRelativeLocation().Equals(FVector::ZeroVector, 0.0001f))
					{
						UE_LOG(LogTemp, Warning, TEXT("STOP-- 2 ------------------------------------------------------"));
						bIsAimBackMoveCamera = false;
						bIsOpticalAiming = false;
					}
				}
				
			}
			*/
		}
	}
}

// 	InputComponent->BindAxis("MoveForward", this, &AXiPlayerController::InputAxisX);
// 	InputComponent->BindAction("ResetVR", IE_Pressed, this, &AXiPlayerController::OnResetVR);
void AXiCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("MovementModeWalk", IE_Pressed, this, &AXiCharacter::ChangeMovementWalk);
	PlayerInputComponent->BindAction("MovementModeWalk", IE_Released, this, &AXiCharacter::ChangeMovementWalkToRun);
	PlayerInputComponent->BindAction("MovementModeAim", IE_Pressed, this, &AXiCharacter::ChangeMovementAim);
	PlayerInputComponent->BindAction("MovementModeAim", IE_Released, this, &AXiCharacter::ChangeMovementAimToRun);
	PlayerInputComponent->BindAction("MovementModeSprint", IE_Pressed, this, &AXiCharacter::ChangeMovementSprint);
	PlayerInputComponent->BindAction("MovementModeSprint", IE_Released, this, &AXiCharacter::ChangeMovementSprintToRun);

	PlayerInputComponent->BindAction("AbilityAction", IE_Pressed, this, &AXiCharacter::TryAbilityEnabled);
}

// �������� ��������� �� ������
void AXiCharacter::MovementTick(float DeltaTime) 
{
	if (bIsAlive && !bIsStun)
	{
		APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
		if (myController)
		{
			FHitResult ResultHit;
			//myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, ResultHit); //Bug was DefaultEngine.ini
			myController->GetHitResultUnderCursor(ECC_GameTraceChannel1, true, ResultHit);
			// ����� ����������� ����� MakeRotFromX(const FVector& Start, const FVector& Target);
			//float FindRotaterResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			LookAtCursorRotationYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), ResultHit.Location).Yaw;
			//if (MovementState != EMovementState::Sprint_State)
			//{
				SetActorRotation(FQuat(FRotator(0.0f, LookAtCursorRotationYaw, 0.0f)));
			//}
				FVector CharacterMovementVector = FVector::ZeroVector;
				CharacterMovementVector = GetLastMovementInputVector();
				//UE_LOG(LogTemp, Warning, TEXT("CharacterMovementVector: X = %f. Y = %f. Z = f%"), CharacterMovementVector.X, CharacterMovementVector.Y, CharacterMovementVector.Z); // todo
				CharacterMovementVector.IsNearlyZero(0.01F) ? bIsCurrentCharacterMoving = false : bIsCurrentCharacterMoving = true;
				//CharacterVelocity = myController->GetVelocity();
				AWeaponDefault* myWeapon = nullptr;
				myWeapon = GetCurrentWeapon();
				if (myWeapon)
				{ 
	//			UE_LOG(LogTemp, Warning, TEXT("Vector: X = %f. Y = %f. Z = %f"), ResultHit.Location.X, ResultHit.Location.Y, this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z);
				CurrentWeapon->ShootEndLocation = FVector(ResultHit.Location.X, ResultHit.Location.Y, this->GetMesh()->GetSocketLocation("WeaponSocketRightHand").Z);			
				CurrentWeapon->bIsCharacterMoving = bIsCurrentCharacterMoving;
				//UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::MovementTick - Check this work"));
				}
				if (bIsAimMoveCamera && myWeapon->WeaponSetting.bIsOpticalSight)
				{
					bIsOpticalAiming = true;
					FVector CurrentCursorLocation = FVector::ZeroVector;
					//	UE_LOG(LogTemp, Warning, TEXT("1. CurrentCursorLocation: X = %f. Y = %f"), CurrentCursorLocation.X, CurrentCursorLocation.Y);
						//UE_LOG(LogTemp, Warning, TEXT("1. CursorLocation: X = %f. Y = %f"), CurrentCursor->GetComponentLocation().X, CurrentCursor->GetComponentLocation().Y);
					CurrentCursorLocation = CurrentCursor->GetComponentLocation();
					//	myController->ProjectWorldLocationToScreen(CurrentCursorLocation, NewMouseScreenLocation, false);
						//UE_LOG(LogTemp, Warning, TEXT("2. CurrentCursorLocation: X = %f. Y = %f"), CurrentCursorLocation.X, CurrentCursorLocation.Y);
						//UE_LOG(LogTemp, Warning, TEXT("2. CursorLocation: X = %f. Y = %f"), CurrentCursor->GetComponentLocation().X, CurrentCursor->GetComponentLocation().Y);
					FVector CameraDistanceTarget = FVector::ZeroVector;
					FRotator CameraBoomRotation = FRotator(CameraBoom->GetTargetRotation().Pitch + 90.f, 0.f, 0.f); // todo
					CameraDistanceTarget = CameraBoom->GetComponentLocation() + CameraAimMoveDistance + (CameraBoomRotation.RotateVector(FVector(0.f, 0.f, CameraBoom->TargetArmLength)));
					FVector DeltaDistanceCamera = FVector::ZeroVector;
					FVector DeltaDistanceCursor = FVector::ZeroVector;
					DeltaDistanceCamera = UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetComponentLocation(), CameraDistanceTarget, DeltaTime, CameraToScopeSpeed);
					DeltaDistanceCursor = TopDownCameraComponent->GetComponentLocation() - DeltaDistanceCamera;
					FVector NormalCameraOffsetVector = FVector(DeltaDistanceCursor.X, DeltaDistanceCursor.Y, 0.f).GetSafeNormal(0.001f);
					TopDownCameraComponent->SetWorldLocation(DeltaDistanceCamera);
					//UE_LOG(LogTemp, Warning, TEXT("3. CurrentCursorLocation: X = %f. Y = %f"), CurrentCursorLocation.X, CurrentCursorLocation.Y);
					//UE_LOG(LogTemp, Warning, TEXT("3. CursorLocation: X = %f. Y = %f"), CurrentCursor->GetComponentLocation().X, CurrentCursor->GetComponentLocation().Y);
					if (!TopDownCameraComponent->GetComponentLocation().Equals(CameraDistanceTarget, 1.f))
					{
						//UE_LOG(LogTemp, Warning, TEXT("CameraDistanceTarget: X = %f. Y = %f. Z = %f"), CameraDistanceTarget.X, CameraDistanceTarget.Y, CameraDistanceTarget.Z);
						int32 nX = 0;
						int32 nY = 0;
						int32 maxX = 0;
						int32 maxY = 0;
						myController->GetViewportSize(maxX, maxY);
						float MouseDeltaX = 0.f;
						float MouseDeltaY = 0.f;
						myController->GetInputMouseDelta(MouseDeltaX, MouseDeltaY);
						//UE_LOG(LogTemp, Warning, TEXT("1. InputMouseDelta: X = %f. Y = %f"), MouseDeltaX, MouseDeltaY);
						float MouseSensitivity = 10.f; // todo �������� ���������������� ���� �����������
						float MouseSensitivityX = MouseSensitivity;
						float MouseSensitivityY = MouseSensitivity;
						//float MouseSensitivityY = 10.f; // todo �������� ���������������� ���� �����������
						//UPlayerInput* myPlayerInput = myController->PlayerInput;
						//myPlayerInput->GetMouseSensitivityX(); // #include "GameFramework/PlayerInput.h"
						//MouseSensitivityX = MouseSensitivityX + myController->PlayerInput->GetMouseSensitivityX();
						FVector2D NewMouseScreenLocation = FVector2D::ZeroVector;
						//UE_LOG(LogTemp, Warning, TEXT("1. NewMouseScreenLocation: X = %f. Y = %f"), NewMouseScreenLocation.X, NewMouseScreenLocation.Y);
						myController->ProjectWorldLocationToScreen(CurrentCursorLocation + DeltaDistanceCursor, NewMouseScreenLocation, false);
						/*	if (NewMouseScreenLocation.X < 0)
							{
								NewMouseScreenLocation.X = 0;
							}
							if (NewMouseScreenLocation.Y < 0)
							{
								NewMouseScreenLocation.Y = 0;
							}*/
						//UE_LOG(LogTemp, Warning, TEXT("1. Forward NewMouseScreenLocation: X = %f. Y = %f"), NewMouseScreenLocation.X, NewMouseScreenLocation.Y);
						//UE_LOG(LogTemp, Warning, TEXT("2. NewMouseScreenLocation: X = %f. Y = %f"), NewMouseScreenLocation.X, NewMouseScreenLocation.Y);
						if (MouseDeltaX != 0)
						{
							MouseSensitivityByDirection(MouseSensitivity, -NormalCameraOffsetVector.X, MouseDeltaX, MouseSensitivityX);
						}
						if (MouseDeltaY != 0)
						{
							MouseSensitivityByDirection(MouseSensitivity, NormalCameraOffsetVector.Y, MouseDeltaY, MouseSensitivityY);
						}
						nX = UKismetMathLibrary::Round(NewMouseScreenLocation.X + (MouseDeltaX * MouseSensitivityX));
						nY = UKismetMathLibrary::Round(NewMouseScreenLocation.Y - (MouseDeltaY * MouseSensitivityY));
						//if (MouseDeltaX != 0 && MouseDeltaY != 0)
						//{
						//	UE_LOG(LogTemp, Warning, TEXT("1. Forward MouseDelta: X = %f. Y = %f"), MouseDeltaX, MouseDeltaY);
						//	UE_LOG(LogTemp, Warning, TEXT("2. Forward MouseSensitivity: X = %f. Y = %f"), MouseSensitivityX, MouseSensitivityY);
						//}
						if (nX < maxX && nY < maxY && nX > 0 && nY > 0)
						{
							myController->SetMouseLocation(nX, nY);
						}
					}
					else
					{
						bIsAimMoveCamera = false;
					}
				}
				else
				{
					if (bIsAimBackMoveCamera)
					{
						FVector BackNewStartDistanceCamera = FVector::ZeroVector;
						FVector BackDeltaDistanceCamera = FVector::ZeroVector;
						FVector CurrentCursorLocation = CurrentCursor->GetComponentLocation();
						BackNewStartDistanceCamera = TopDownCameraComponent->GetComponentLocation();
						TopDownCameraComponent->SetRelativeLocation(UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetRelativeLocation(), FVector::ZeroVector, DeltaTime, CameraToScopeSpeed));
						BackDeltaDistanceCamera = BackNewStartDistanceCamera - TopDownCameraComponent->GetComponentLocation();
						FVector NormalCameraOffsetVector = FVector(BackDeltaDistanceCamera.X, BackDeltaDistanceCamera.Y, 0.f).GetSafeNormal(0.001f);

						int32 nX = 0;
						int32 nY = 0;
						int32 maxX = 0;
						int32 maxY = 0;
						myController->GetViewportSize(maxX, maxY);
						float MouseDeltaX = 0.f;
						float MouseDeltaY = 0.f;
						myController->GetInputMouseDelta(MouseDeltaX, MouseDeltaY);
						float MouseSensitivity = 10.f; // todo �������� ���������������� ���� �����������
						float MouseSensitivityX = MouseSensitivity;
						float MouseSensitivityY = MouseSensitivity;

						FVector2D NewMouseScreenLocation = FVector2D::ZeroVector;
						myController->ProjectWorldLocationToScreen(CurrentCursorLocation + BackDeltaDistanceCamera, NewMouseScreenLocation, false);
						/*if (NewMouseScreenLocation.X < 0)
						{
							NewMouseScreenLocation.X = 0;
						}
						if (NewMouseScreenLocation.Y < 0)
						{
							NewMouseScreenLocation.Y = 0;
						}*/

						//UE_LOG(LogTemp, Warning, TEXT("2. Back NewMouseScreenLocation: X = %f. Y = %f"), NewMouseScreenLocation.X, NewMouseScreenLocation.Y);
						if (MouseDeltaX != 0)
						{
							MouseSensitivityByDirection(MouseSensitivity, -NormalCameraOffsetVector.X, MouseDeltaX, MouseSensitivityX);
						}
						if (MouseDeltaY != 0)
						{
							MouseSensitivityByDirection(MouseSensitivity, NormalCameraOffsetVector.Y, MouseDeltaY, MouseSensitivityY);
						}
						nX = UKismetMathLibrary::Round(NewMouseScreenLocation.X + (MouseDeltaX * MouseSensitivityX));
						nY = UKismetMathLibrary::Round(NewMouseScreenLocation.Y - (MouseDeltaY * MouseSensitivityY));
						if (nX < maxX && nY < maxY && nX > 0 && nY > 0)
						{
							myController->SetMouseLocation(nX, nY);
						}
						//UE_LOG(LogTemp, Warning, TEXT("2. Back MouseScreenLocation: nX = %d. nY = %d"), nX, nY);
						if (TopDownCameraComponent->GetRelativeLocation().Equals(FVector::ZeroVector, 0.0001f))
						{
							bIsAimBackMoveCamera = false;
							bIsOpticalAiming = false;
						}
					}
				}
		}
	}

	//if (CurrentWeapon) ������� � AWeaponDefault::UpdateStateWeapon
	//{
	//	if (FMath::IsNearlyZero(GetVelocity().Size(), 0.5f))
	//	{
	//		CurrentWeapon->ShouldReduceDispersion = true;
	//	}
	//	else
	//	{
	//		CurrentWeapon->ShouldReduceDispersion = false;
	//	}
	//}
}

void AXiCharacter::AimingTargetTick(float DeltaTime) // todo2
{
	if (bSearchAimingTarget)
	{
		if (StartAimingTimer < 0.f)
		{
			WeaponAutoAimingTarget(true);
		}
		else
			StartAimingTimer -= DeltaTime;

		if (bAimingStartTarget)
		{
			if (AimingActor->IsValidLowLevel())
			{
				TargetWidgetComponent->SetWorldLocation(AimingActor->GetActorLocation());
				TargetWidgetComponent->SetVisibility(true);
				if (AutoAimingTimer < 0.f)
				{
					//WeaponAutoAimingTarget(true);
					if (CheckAimingTarget(AimingActor))
					{
						bAimingStartTarget = false;
						bStopSearchAimingTarget = false;
						TargetWidgetComponent->SetVisibility(false);
					}
				}
				else
					AutoAimingTimer -= DeltaTime;
			}
			else
			{
				bAimingStartTarget = false;
				bStopSearchAimingTarget = false;
				TargetWidgetComponent->SetVisibility(false);
			}
		}

	}
}

void AXiCharacter::OpticalAimingTick(float DeltaTime)
{
	AXiPlayerController* myController = Cast<AXiPlayerController>(GetController());
	if (myController)
	{
		AWeaponDefault* myWeapon = nullptr;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			if (bIsAimMoveCamera && myWeapon->WeaponSetting.bIsOpticalSight)
			{
				bIsOpticalAiming = true;
				//UE_LOG(LogTemp, Warning, TEXT("3. CurrentCursorLocation: X = %f. Y = %f"), CurrentCursorLocation.X, CurrentCursorLocation.Y);
				//UE_LOG(LogTemp, Warning, TEXT("3. CursorLocation: X = %f. Y = %f"), CurrentCursor->GetComponentLocation().X, CurrentCursor->GetComponentLocation().Y);

				FVector CurrentCursorLocation = FVector::ZeroVector;
				CurrentCursorLocation = CurrentCursor->GetComponentLocation();
				FVector CameraDistanceTarget = FVector::ZeroVector;
				FRotator CameraBoomRotation = FRotator(CameraBoom->GetTargetRotation().Pitch + 90.f, 0.f, 0.f); // todo
				CameraDistanceTarget = CameraBoom->GetComponentLocation() + CameraAimMoveDistance + (CameraBoomRotation.RotateVector(FVector(0.f, 0.f, CameraBoom->TargetArmLength)));

				FVector DeltaDistanceCamera = FVector::ZeroVector;
				DeltaDistanceCamera = UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetComponentLocation(), CameraDistanceTarget, DeltaTime, CameraToScopeSpeed);
				FVector DeltaDistanceCursor = FVector::ZeroVector;
				DeltaDistanceCursor = TopDownCameraComponent->GetComponentLocation() - DeltaDistanceCamera;


				TopDownCameraComponent->SetWorldLocation(DeltaDistanceCamera);
			
				if (!TopDownCameraComponent->GetComponentLocation().Equals(CameraDistanceTarget, 1.f))
				{
					if (bReturnOpticalAimCursor)
					{
						//UE_LOG(LogTemp, Warning, TEXT("0. CameraViewport: X = %d. Y = %d"), CameraViewportWidth, CameraViewportHeight);
						FVector2D NewCursorScreenLocation = FVector2D::ZeroVector;
						myController->ProjectWorldLocationToScreen(CurrentCursorLocation + DeltaDistanceCursor, NewCursorScreenLocation, false);
						//UE_LOG(LogTemp, Warning, TEXT("1. NewCursorScreenLocation: X = %f. Y = %f"), NewCursorScreenLocation.X, NewCursorScreenLocation.Y);
						//UE_LOG(LogTemp, Warning, TEXT("2. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
						if (NewCursorScreenLocation.X < 0)
						{
							NewCursorScreenLocation.X = 0;
						}
						else if (NewCursorScreenLocation.X > static_cast<float>(CameraViewportWidth))
						{
							NewCursorScreenLocation.X = static_cast<float>(CameraViewportWidth);
						}
						CurrentCursorPosX = static_cast<float>(UKismetMathLibrary::Round(NewCursorScreenLocation.X));


						if (NewCursorScreenLocation.Y < 0)
						{
							NewCursorScreenLocation.Y = 0;
						}
						else if (NewCursorScreenLocation.Y > static_cast<float>(CameraViewportHeight))
						{
							NewCursorScreenLocation.Y = static_cast<float>(CameraViewportHeight);
						}
						CurrentCursorPosY = static_cast<float>(UKismetMathLibrary::Round(NewCursorScreenLocation.Y));
						//UE_LOG(LogTemp, Warning, TEXT("3. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
					}
				}
				else
				{
					TopDownCameraComponent->SetWorldLocation(CameraDistanceTarget);
					UE_LOG(LogTemp, Warning, TEXT("STOP ------------------------------------------------------"));
					bIsAimMoveCamera = false;
				}
			}
			else
			{
				if (bIsAimBackMoveCamera)
				{
					FVector BackNewStartDistanceCamera = FVector::ZeroVector;
					FVector BackDeltaDistanceCamera = FVector::ZeroVector;
					FVector CurrentCursorLocation = CurrentCursor->GetComponentLocation();
					BackNewStartDistanceCamera = TopDownCameraComponent->GetComponentLocation();
					TopDownCameraComponent->SetRelativeLocation(UKismetMathLibrary::VInterpTo(TopDownCameraComponent->GetRelativeLocation(), FVector::ZeroVector, DeltaTime, CameraToScopeSpeed));

					
					if (bReturnOpticalAimCursor)
					{
						BackDeltaDistanceCamera = BackNewStartDistanceCamera - TopDownCameraComponent->GetComponentLocation();
						FVector2D NewCursorScreenLocation = FVector2D::ZeroVector;
						myController->ProjectWorldLocationToScreen(CurrentCursorLocation + BackDeltaDistanceCamera, NewCursorScreenLocation, false);
						if (NewCursorScreenLocation.X < 0.f)
						{
							NewCursorScreenLocation.X = 0.f;
						}
						else if (NewCursorScreenLocation.X > static_cast<float>(CameraViewportWidth))
						{
							NewCursorScreenLocation.X = static_cast<float>(CameraViewportWidth);
						}
						CurrentCursorPosX = static_cast<float>(UKismetMathLibrary::Round(NewCursorScreenLocation.X));


						if (NewCursorScreenLocation.Y < 0.f)
						{
							NewCursorScreenLocation.Y = 0.f;
						}
						else if (NewCursorScreenLocation.Y > static_cast<float>(CameraViewportHeight))
						{
							NewCursorScreenLocation.Y = static_cast<float>(CameraViewportHeight);
						}
						CurrentCursorPosY = static_cast<float>(UKismetMathLibrary::Round(NewCursorScreenLocation.Y));

						//UE_LOG(LogTemp, Warning, TEXT("1. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
					}
					if (TopDownCameraComponent->GetRelativeLocation().Equals(FVector::ZeroVector, 1.f))
					{
						TopDownCameraComponent->SetRelativeLocation(FVector::ZeroVector);
						UE_LOG(LogTemp, Warning, TEXT("STOP-- 2 ------------------------------------------------------"));
						bIsAimBackMoveCamera = false;
						bIsOpticalAiming = false;
					}
				}
			}
		}
	}
}

void AXiCharacter::AimingLaserBeamTick(float DeltaTime)
{
	if (bIsLaserBeamStart)
	{
		AWeaponDefault* myWeapon = nullptr;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			UParticleSystemComponent* LaserBeam = myWeapon->GetWeaponLaserBeam();
			if (LaserBeam)
			{
				LaserBeam->SetBeamSourcePoint(0, myWeapon->ShootLocation->GetComponentLocation(), 0);
				FHitResult Hit;
				// to do ������ ������� ������� ����� ����� ������
			//	FVector NewEndLocation = FVector(myWeapon->ShootLocation->GetComponentLocation() + ((myWeapon->ShootEndLocation - myWeapon->ShootLocation->GetComponentLocation()) * 5000.f));
				FVector NewEndLocation = FVector(myWeapon->ShootLocation->GetComponentLocation() + (myWeapon->GetFireEndLocation() * 5000.f));
				Hit = myWeapon->WeaponLineTrace(FVector2D(NewEndLocation.X, NewEndLocation.Y)); // todo ������ ���������� ����� 5000 - ��������� ��������� ���� 
				if (Hit.bBlockingHit)
				{
					LaserBeam->SetBeamEndPoint(0, FVector(Hit.ImpactPoint.X, Hit.ImpactPoint.Y, myWeapon->ShootLocation->GetComponentLocation().Z));
					//UE_LOG(LogTemp, Warning, TEXT("1. LaserBeamEndLocation: X = %f. Y = %f"), Hit.ImpactPoint.X, Hit.ImpactPoint.Y);
				}
				//UE_LOG(LogTemp, Warning, TEXT("2. CursorLocation (myWeapon->ShootEndLocation): X = %f. Y = %f"), myWeapon->ShootEndLocation.X, myWeapon->ShootEndLocation.Y);
				//LaserBeam->SetBeamEndPoint(0, NewEndLocation); // todo ������ ���������� ����� 5000 - ��������� ��������� ����
			}
		}
	}
}

void AXiCharacter::NewMouseCursorTick(float DeltaTime)
{
	//AXiPlayerController* myController = Cast<AXiPlayerController>(UGameplayStatics::GetPlayerController(GetWorld(), 0)); // todo ����� ����� GetWorld()?
	AXiPlayerController* myController = Cast<AXiPlayerController>(GetController());
	if (myController)
	{
		//if (myController->GetMovableCursor())
		//{
		//	myController->GetViewportSize(CameraViewportHeight, CameraViewportWidth);
		//	//myController->SetMouseLocation(CameraViewportHeight / 2, CameraViewportWidth / 2);
		//	float MouseDeltaX = 0.f;
		//	float MouseDeltaY = 0.f;
		//	myController->GetInputMouseDelta(MouseDeltaX, MouseDeltaY);
		//	if (MouseDeltaX != 0.f && MouseDeltaY != 0.f)
		//	{
		//		MouseDeltaX *= CurrentMouseSensotivity; // todo X & Y sesotivity
		//		MouseDeltaY *= CurrentMouseSensotivity;
		//		if ((CurrentCursorPosX + MouseDeltaX) <= static_cast<float>(CameraViewportHeight) && (CurrentCursorPosX + MouseDeltaX) >= 0.f)
		//		{
		//			CurrentCursorPosX += MouseDeltaX;
		//		}
		//		if ((CurrentCursorPosY - MouseDeltaY) <= static_cast<float>(CameraViewportWidth) && (CurrentCursorPosY - MouseDeltaY) >= 0.f)
		//		{
		//			CurrentCursorPosY -= MouseDeltaY;
		//		}
		//	}

		//	// �������� �� ����� �� ������� ������.
		//	if (CurrentCursorPosX < 0.f)
		//	{
		//		CurrentCursorPosX = 0.f;
		//	}
		//	else if (CurrentCursorPosX > static_cast<float>(CameraViewportHeight))
		//	{
		//		CurrentCursorPosX = CameraViewportHeight;
		//	}
		//	if (CurrentCursorPosY < 0.f)
		//	{
		//		CurrentCursorPosY = 0.f;
		//	}
		//	else if (CurrentCursorPosY > static_cast<float>(CameraViewportWidth))
		//	{
		//		CurrentCursorPosY = CameraViewportWidth;
		//	}

		//}
		if (CurrentCursor)
		{
			FHitResult VisibleTraceHitResult;
			FVector2D MousePosition = FVector2D(CurrentCursorPosX, CurrentCursorPosY);
			//to do - ?? ���� ��������, �� ����� �������. ������ ���������� ����� - ECC_GameTraceChannel1 --- myController->GetHitResultAtScreenPosition(MousePosition, ECC_Visibility, true, TraceHitResult)
			if (myController->GetHitResultAtScreenPosition(MousePosition, ECC_Visibility, true, VisibleTraceHitResult))
			{
				//FVector CursorLocation = FVector::ZeroVector;
				FVector CursorFV = VisibleTraceHitResult.ImpactNormal;
				FRotator CursorR = CursorFV.Rotation();
				//FVector CursorFV = FVector::ZeroVector;
				//myController->DeprojectScreenPositionToWorld(CurrentCursorPosX,CurrentCursorPosY, CursorLocation, CursorFV);
				//CursorFV *= -1.f;
				//FRotator CursorR = CursorFV.Rotation();
				CurrentCursor->SetWorldLocation(VisibleTraceHitResult.Location);
				//CurrentCursor->SetWorldLocation(CursorLocation);
				CurrentCursor->SetWorldRotation(CursorR);
				//UE_LOG(LogTemp, Warning, TEXT("0. CurrentCursorPos: X = %f. Y = %f"), CurrentCursorPosX, CurrentCursorPosY);
				//UE_LOG(LogTemp, Warning, TEXT("1. CursorLocation: X = %f. Y = %f. Z = %f"), CursorLocation.X, CursorLocation.Y, CursorLocation.Z);
				//UE_LOG(LogTemp, Warning, TEXT("2. TraceHitResult.Location: X = %f. Y = %f. Z = %f"), TraceHitResult.Location.X, TraceHitResult.Location.Y, TraceHitResult.Location.Z);
				FHitResult CursorTraceHitResult;
				if (myController->GetHitResultAtScreenPosition(MousePosition, ECC_GameTraceChannel1, true, CursorTraceHitResult))
				{
					if (bIsAlive && !bIsStun)
					{
						CharacterTurnTick(DeltaTime, myController, CursorTraceHitResult);


					}
				}
			}
			if (bIsAimMoveCamera || bIsAimBackMoveCamera)
			{
				OpticalAimingTick(DeltaTime);
			}
		}

	}
}

void AXiCharacter::OffsetMouseCursorX(float DeltaCursorX, int32 CurrentViewportSize)
{
	CameraViewportWidth = CurrentViewportSize;
	//myController->SetMouseLocation(CameraViewportHeight / 2, CameraViewportWidth / 2);
	if (DeltaCursorX != 0.f)
	{
		DeltaCursorX *= CurrentMouseSensotivity; // todo X & Y sesotivity
		if ((CurrentCursorPosX + DeltaCursorX) <= static_cast<float>(CameraViewportWidth) && (CurrentCursorPosX + DeltaCursorX) >= 0.f)
		{
			CurrentCursorPosX += DeltaCursorX;
		}
	}

	// �������� �� ����� �� ������� ������.
	if (CurrentCursorPosX < 0.f)
	{
		CurrentCursorPosX = 0.f;
	}
	else if (CurrentCursorPosX > static_cast<float>(CameraViewportWidth))
	{
		CurrentCursorPosX = CameraViewportWidth;
	}
}

void AXiCharacter::OffsetMouseCursorY(float DeltaCursorY, int32 CurrentViewportSize)
{
	bool bIsInvertMouseY = false;
	if (!bIsInvertMouseY)
	{
		DeltaCursorY *= -1.f;
	}
	CameraViewportHeight = CurrentViewportSize;
	//myController->SetMouseLocation(CameraViewportHeight / 2, CameraViewportWidth / 2);
	if (DeltaCursorY != 0.f)
	{
		DeltaCursorY *= CurrentMouseSensotivity; // todo X & Y sesotivity
		if ((CurrentCursorPosY + DeltaCursorY) <= static_cast<float>(CameraViewportHeight) && (CurrentCursorPosY + DeltaCursorY) >= 0.f)
		{
			CurrentCursorPosY += DeltaCursorY;
		}
	}
	// �������� �� ����� �� ������� ������.
	if (CurrentCursorPosY < 0.f)
	{
		CurrentCursorPosY = 0.f;
	}
	else if (CurrentCursorPosY > static_cast<float>(CameraViewportHeight))
	{
		CurrentCursorPosY = CameraViewportHeight;
	}
}

void AXiCharacter::SetCharacterNewMouseInputSettings(FCharacterMouseInputSettings PlayerNewInputSetttings)
{
	bSystemCursor = PlayerNewInputSetttings.bSystemMouseSensitivity;
	bReturnOpticalAimCursor = PlayerNewInputSetttings.bIsOffsetOpticalAimCursor;
	CurrentMouseSensotivity = PlayerNewInputSetttings.MouseCursorSensitivity;
}

void AXiCharacter::AttackCharEvent(bool bIsFiring)
{
	if (!bIsStun)
	{
		AWeaponDefault* myWeapon = nullptr;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			//ToDo Check melee or range
			myWeapon->SetWeaponStateFire(bIsFiring);
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::AttackCharEvent - CurrentWeapon -NULL"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::AttackCharEvent - Character under Stun"));
	}
}

void AXiCharacter::AlternativeAttackCharEvent(bool bIsFiring)
{
	if (!bIsStun)
	{
		
		AWeaponDefault_Medic* myWeapon = nullptr;
		myWeapon = Cast<AWeaponDefault_Medic>(GetCurrentWeapon());
		if (IsValid(myWeapon) && myWeapon)
		{
			if (myWeapon->IsHealerModuleActivate())
			{
				myWeapon->SetWeaponStateAlternativeFire(bIsFiring);
			}
			//ToDo Check melee or range
		}
		else
			UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::AlternativeAttackCharEvent - CurrentWeapon -NULL or not AlternativeWeapon"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::AlternativeAttackCharEvent - Character under Stun"));
	}
}

AWeaponDefault* AXiCharacter::GetCurrentWeapon()
{
	return CurrentWeapon;
}


void AXiCharacter::InitWeapon(FName IdWeapon, FAdditionalWeaponInfo WeaponAdditionalInfo, int32 NewCurrentIndexWeapon)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->Destroy();
		CurrentWeapon = nullptr;
	}

	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetGameInstance());
	FWeaponInfo myWeaponInfo;
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(IdWeapon, myWeaponInfo))
		{
			if (myWeaponInfo.WeaponClass)
			{
				FVector SpawnLocation = FVector(0);
				FRotator SpawnRotation = FRotator(0);
	
				FActorSpawnParameters SpawnParams;
				SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
				SpawnParams.Owner = this;
				//SpawnParams.Owner = GetOwner(); � ����� ������ ����� �������������� ����� ����������!
				SpawnParams.Instigator = GetInstigator();
	
				AWeaponDefault* myWeapon = Cast<AWeaponDefault>(GetWorld()->SpawnActor(myWeaponInfo.WeaponClass, &SpawnLocation, &SpawnRotation, SpawnParams));
				if (myWeapon)
				{
					FAttachmentTransformRules Rule(EAttachmentRule::SnapToTarget, false);
					myWeapon->AttachToComponent(GetMesh(), Rule, FName("WeaponSocketRightHand"));
					CurrentWeapon = myWeapon;

					myWeapon->WeaponSetting = myWeaponInfo;


					//ToDo ������ �� ������ ���� � ������ ������ ������ ��� ����� ��������� � ������
					//myWeapon->AdditionalWeaponInfo.Round = myWeaponInfo.MaxRound; // ����� ��� ����� ����� ���� ���� ��� ���� ����������� WeaponAdditionalInfo
	
					myWeapon->UpdateStateWeapon(MovementState);


					myWeapon->AdditionalWeaponInfo = WeaponAdditionalInfo;
					//if(InventoryComponent)
					CurrentIndexWeapon = NewCurrentIndexWeapon;//fix

					//Not Forget remove delegate on change/drop weapon
					myWeapon->OnWeaponReloadStart.AddDynamic(this, &AXiCharacter::WeaponReloadStart); // AddDynamic ����� ���������� �������� ���������
					myWeapon->OnWeaponReloadEnd.AddDynamic(this, &AXiCharacter::WeaponReloadEnd);
					
					myWeapon->OnWeaponFireStart.AddDynamic(this, &AXiCharacter::WeaponFireStart);

					myWeapon->WeaponSetting.MaxRound = myWeapon->NewWeaponRound();
					if (InventoryComponent)
					{
						if (myWeapon->AdditionalWeaponInfo.Round > myWeapon->WeaponSetting.MaxRound)
						{
							InventoryComponent->AmmoSlotChangeValue(myWeapon->WeaponSetting.AmmoType, (myWeapon->AdditionalWeaponInfo.Round - myWeapon->WeaponSetting.MaxRound));
							myWeapon->AdditionalWeaponInfo.Round = myWeapon->WeaponSetting.MaxRound;
						}
						else
						{
							//myWeapon->AdditionalWeaponInfo.Round = myWeapon->WeaponSetting.MaxRound;
						}
						InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
					}

					// after switch try reload weapon if needed
					if (CurrentWeapon->GetWeaponRound() <= 0 && CurrentWeapon->CheckCanWeaponReload())
						CurrentWeapon->InitReload();
					//ReloadWeaponEvent(); ������ ������ ��������� �����������

					if (InventoryComponent)
					{
						//InventoryComponent->InitAmmoWithWeapon(myWeapon->WeaponSetting.WeaponType, myWeapon->AdditionalWeaponInfo);
						InventoryComponent->OnWeaponAndAmmoAvailable.Broadcast(IdWeapon,NewCurrentIndexWeapon);// ������� ��� �������
					}

					//������������� ��������� �������� ��� ������� ������
					myWeapon->BulletShellOldOffset = myWeaponInfo.ShellBullets.DropMeshOffset;
					myWeapon->DropShellBulletOldImpulse = myWeaponInfo.ShellBullets.DropMeshImpulseDir;
					myWeapon->MagazineDropOldOffset = myWeaponInfo.MagazineDrop.DropMeshOffset;

					//�������� ������ ������� � ������
					AWeaponDefault_Medic* myMedicWeapon = nullptr;
					myMedicWeapon = Cast<AWeaponDefault_Medic>(GetCurrentWeapon());
					if (IsValid(myMedicWeapon) && myMedicWeapon)
					{
						bModuleAimingTarget = myMedicWeapon->IsHealerModuleActivate();
					}
					else
					{
						bModuleAimingTarget = false;
					}

					if (MovementAim)
					{
						ChangeMovementAim();
					}
					
					////��� �������� ���� ��� To_Do
					//InitWeaponName = myWeapon->WeaponSetting.


				}
			}
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::InitWeapon - Weapon not found in table"));
		}
	}
}

void AXiCharacter::RemoveCurrentWeapon()
{

}

void AXiCharacter::ReloadWeaponEvent()
{
	if (CurrentWeapon)
	{
		if (CurrentWeapon->GetWeaponRound() < CurrentWeapon->WeaponSetting.MaxRound && CurrentWeapon->CheckCanWeaponReload() &&!CurrentWeapon->WeaponReloading)
		{
			//ToDO Animation here? ��� ������ ������ � ����� ��
			CurrentWeapon->InitReload();
		}
	}
}

void AXiCharacter::WeaponReloadStart(UAnimMontage* AnimReload)
{
	WeaponReloadStart_BP(AnimReload);
}

void AXiCharacter::WeaponReloadEnd(bool bIsSuccess, int32 AmmoTake)
{
	if (bIsSuccess)
	{
		if (InventoryComponent && CurrentWeapon)
		{
			if (InventoryComponent->AmmoSlotChangeValue(CurrentWeapon->WeaponSetting.AmmoType, AmmoTake))
			{
				InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
				InventoryComponent->ChangeWeaponSlotsIsEmpty(CurrentWeapon->WeaponSetting.AmmoType, true);
			}
			else
			InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
		}
	}
	WeaponReloadEnd_BP(bIsSuccess);
}

void AXiCharacter::WeaponReloadStart_BP_Implementation(UAnimMontage* AnimReload)
{
	// in BP
}

void AXiCharacter::WeaponReloadEnd_BP_Implementation(bool bIsSuccess)
{
	// in BP
}

void AXiCharacter::WeaponFireStart(UAnimMontage* AnimFire)
{
	if (InventoryComponent && CurrentWeapon)
		InventoryComponent->SetAdditionalInfoWeapon(CurrentIndexWeapon, CurrentWeapon->AdditionalWeaponInfo);
//	UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::WeaponFireStart() AnimFire %s"), (AnimFire ? TEXT("True") : TEXT("False")));
	WeaponFireStart_BP(AnimFire);
}

void AXiCharacter::WeaponFireStart_BP_Implementation(UAnimMontage* AnimFire)
{
	// in BP
}

void AXiCharacter::WeaponAlternativeFireStart(UAnimMontage* AnimFire)
{
	WeaponAlternativeFireStart_BP(AnimFire);
}

void AXiCharacter::WeaponAlternativeFireStart_BP_Implementation(UAnimMontage* AnimFire)
{
	//in BP
}

void AXiCharacter::WeaponAutoAimingTarget(bool bIsEnemy) // todo2
{
	if (!bStopSearchAimingTarget)
	{
		bStopSearchAimingTarget = true;
		AWeaponDefault* myWeapon = nullptr;
		FVector WeaponCursorLocation = FVector::ZeroVector;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			WeaponCursorLocation = myWeapon->ShootEndLocation;
			float CursorToCharacterDistance = 0.f;
			FVector CursorToCharacterVector = FVector::ZeroVector;
			TArray<AActor*> FoundCharacters;
			TArray<AActor*> GoodCharacters;
			//AActor* AimingCharacter;
			UGameplayStatics::GetAllActorsOfClass(GetWorld(), AXiCharacter::StaticClass(), FoundCharacters);
			for (AActor* a : FoundCharacters)
			{
				CursorToCharacterVector = WeaponCursorLocation - a->GetActorLocation();
				CursorToCharacterDistance = ((CursorToCharacterVector.X) * (CursorToCharacterVector.X)) + ((CursorToCharacterVector.Y) * (CursorToCharacterVector.Y));
				// TO DO ������� ���������� ����� ����������� ��������� ���������� ������ ���� ����
				if (CursorToCharacterDistance < 90000.f && a != Cast<AActor>(this))
				{
					GoodCharacters.Add(a);
				}
			}
			// TO DO ������� ���������� ����� ����������� ��������� ���������� ������ ���� ����
			float NewDistance = 90000.f;
			for (AActor* a : GoodCharacters)
			{
				CursorToCharacterVector = WeaponCursorLocation - a->GetActorLocation();
				CursorToCharacterDistance = ((CursorToCharacterVector.X) * (CursorToCharacterVector.X)) + ((CursorToCharacterVector.Y) * (CursorToCharacterVector.Y));
				if (NewDistance > CursorToCharacterDistance)
				{
					NewDistance = CursorToCharacterDistance;
					AimingActor = a;
				}
			}
			//��������� ������� �������� �� �������, ���� ������ �� ���, �� ������������ � ���� �������
			if (AimingActor->IsValidLowLevel())
			{
				myWeapon->NewTargetActorLocation = AimingActor->GetActorLocation();
				bAimingStartTarget = true;
				// todo 2 �������� �������� � ���������� �������������
			}
			else
			{
				bStopSearchAimingTarget = false;
				AimingActor = nullptr;
			}

		}
		else
		{
			bStopSearchAimingTarget = false;
			AimingActor = nullptr;
		}
	}
	// todo 2 ������ ���������� ����� �������
	StartAimingTimer = 1.f;
}

bool AXiCharacter::CheckAimingTarget(AActor* NewAimingTarget)
{
	bool IsLostTarget = false;
	AWeaponDefault* myWeapon = nullptr;
	FVector WeaponCursorLocation = FVector::ZeroVector;
	myWeapon = GetCurrentWeapon();
	if (NewAimingTarget->IsValidLowLevel() && myWeapon)
	{
			// TO DO ������� ���������� ����� ����������� ��������� ���������� ������ ���� ����
			float NewDistance = 90000.f;
			FVector CursorToCharacterVector = FVector::ZeroVector;
			float CursorToCharacterDistance = 0.f;
			WeaponCursorLocation = myWeapon->ShootEndLocation;
			CursorToCharacterVector = WeaponCursorLocation - NewAimingTarget->GetActorLocation();
			CursorToCharacterDistance = ((CursorToCharacterVector.X) * (CursorToCharacterVector.X)) + ((CursorToCharacterVector.Y) * (CursorToCharacterVector.Y));
			if (NewDistance <= CursorToCharacterDistance)
			{
				myWeapon->IsGoodTarget = false;
				IsLostTarget = true;
				AimingActor = nullptr;
				return IsLostTarget;
			}


		AActor* TakenAimingTargetActor = nullptr;
		TakenAimingTargetActor = myWeapon->AutoAimingTarget(false, NewAimingTarget->GetActorLocation());
			if (TakenAimingTargetActor == NewAimingTarget)
			{
				myWeapon->NewTargetActorLocation = NewAimingTarget->GetActorLocation();
				myWeapon->SetTargetActor(NewAimingTarget);
				myWeapon->IsGoodTarget = true;
			}
			else
			{
				myWeapon->IsGoodTarget = false;
			}
	}
	// todo 2 ������ ���������� �����
	AutoAimingTimer = 2.f;
	return IsLostTarget;
}

void AXiCharacter::StopAutoAimingTarget()
{
	bSearchAimingTarget = false;
	bStopSearchAimingTarget = false;
	bAimingStartTarget = false;
	AimingActor = nullptr;
	TargetWidgetComponent->SetVisibility(false);
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->IsGoodTarget = false;
	}

}

void AXiCharacter::StopWeaponAiming() //
{
	// ��������� �������������
	StopAutoAimingTarget();

	//������� ������ � �������� ���������
	bIsAimMoveCamera = false;
	if (bIsOpticalAiming)
	{
		bIsAimBackMoveCamera = true;
	}

	//�������� ������
	bIsLaserBeamStart = false;
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		UParticleSystemComponent* LaserBeam = myWeapon->GetWeaponLaserBeam();
		if (LaserBeam)
		{
			LaserBeam->Deactivate();
		}
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), myWeapon->WeaponSetting.SoundLowerDownWeapon, myWeapon->ShootLocation->GetComponentLocation());
		if (myWeapon->WeaponSetting.bIsOpticalSight)
		{
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), myWeapon->WeaponSetting.SoundScopeOutWeapon, myWeapon->ShootLocation->GetComponentLocation());
		}
	}

}

void AXiCharacter::StartOpticalAiming()
{
	FVector CurrentCursorWorldLocation = FVector::ZeroVector;
	FVector CapsuleComponentWorldLocation = FVector::ZeroVector;
	CurrentCursorWorldLocation = GetCursorToWorld()->GetComponentLocation();
	CapsuleComponentWorldLocation = GetCapsuleComponent()->GetComponentLocation();

	FVector CameraDirection = FVector::ZeroVector;
	CameraDirection = FVector(CurrentCursorWorldLocation.X - CapsuleComponentWorldLocation.X, CurrentCursorWorldLocation.Y - CapsuleComponentWorldLocation.Y, 0.f);
	float VectorTolerance = 0.0001f;
//	CameraDirection = CameraDirection.GetSafeNormal(VectorTolerance); //���������� ������� todo ?
	CameraDirection.Normalize(VectorTolerance); 
	float CameraAimingMovementDistance = 0.f;
	if (TopDownCameraComponent && CameraBoom)
	{
		AWeaponDefault* myWeapon = nullptr;
		myWeapon = GetCurrentWeapon();
		if (myWeapon)
		{
			CameraAimingMovementDistance = myWeapon->WeaponSetting.WeaponScopeScaleAim; //CameraBoom->TargetArmLength * myWeapon->WeaponScaleAim; ����� ��������������� �� 0.5 �� 1  NargetArmLemght
			CameraAimMoveDistance = FVector(CameraDirection.X * CameraAimingMovementDistance / TopDownCameraComponent->AspectRatio, CameraDirection.Y * CameraAimingMovementDistance, 0.f);
			bIsAimBackMoveCamera = false;
			bIsAimMoveCamera = true;
			UGameplayStatics::SpawnSoundAtLocation(GetWorld(), myWeapon->WeaponSetting.SoundScopeInWeapon, myWeapon->ShootLocation->GetComponentLocation());
		}
	}
}

bool AXiCharacter::CheckIsOpticalAiming()
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		return myWeapon->WeaponSetting.bIsOpticalSight;
	}
	return false;
}

void AXiCharacter::StartLaserBeam()
{
	AWeaponDefault* myWeapon = nullptr;
	myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->SetWeaponLaserBeam(UGameplayStatics::SpawnEmitterAtLocation(GetWorld(), myWeapon->WeaponSetting.EffectLaserBeam, myWeapon->ShootLocation->GetComponentTransform()));
		UParticleSystemComponent* LaserBeam = myWeapon->GetWeaponLaserBeam();
		if (LaserBeam)
		{
			LaserBeam->SetBeamSourcePoint(0, myWeapon->ShootLocation->GetComponentLocation(),0);
		//	LaserBeam->SetBeamEndPoint(0, myWeapon->ShootLocation->GetComponentLocation() + ((myWeapon->ShootEndLocation - myWeapon->ShootLocation->GetComponentLocation()) * 5000.f)); // todo ������ ���������� ����� 5000 - ��������� ��������� ����
			LaserBeam->SetBeamEndPoint(0, myWeapon->ShootLocation->GetComponentLocation() + (myWeapon->GetFireEndLocation() * 5000.f)); // todo ������ ���������� ����� 5000 - ��������� ��������� ����
			bIsLaserBeamStart = true;
		}
		UGameplayStatics::SpawnSoundAtLocation(GetWorld(), myWeapon->WeaponSetting.SoundRaiseUpWeapon, myWeapon->ShootLocation->GetComponentLocation());
	}

}

void AXiCharacter::MouseSensitivityByDirection(float CummonMouseSensitivity, float NormalCameraOffset, float MouseDeltaDirection,float& MouseSensitivityDirection)
{
	if ((MouseDeltaDirection * NormalCameraOffset) > 0)
	{
		MouseSensitivityDirection = 1.f + ((CummonMouseSensitivity - 1.f) * UKismetMathLibrary::Abs(NormalCameraOffset));
	}
	else if ((MouseDeltaDirection * NormalCameraOffset) < 0)
	{
		MouseSensitivityDirection = 1.f + ((1.f / CummonMouseSensitivity - 1.f) * UKismetMathLibrary::Abs(NormalCameraOffset));
	}
	else if ((MouseDeltaDirection * NormalCameraOffset) == 0)
	{
		MouseSensitivityDirection = 1.f;
	}
}

void AXiCharacter::TryChangeSpringArmUp()
{
	TryChangeSpringArmUp_BP();
}

void AXiCharacter::TryChangeSpringArmUp_BP_Implementation()
{
	//in BP
}

void AXiCharacter::TryChangeSpringArmDown()
{
	TryChangeSpringArmDown_BP();
}

void AXiCharacter::TryChangeSpringArmDown_BP_Implementation()
{
	//in BP
}


//void AXiCharacter::TryChangeSpringArm()
//{
//	TryChangeSpringArm_BP();
//}
//
//void AXiCharacter::TryChangeSpringArm_BP_Implementation()
//{
//	//in BP
//}

void AXiCharacter::CharacterUpdate()
{
	float ResSpeed = 600.0f;

	switch (MovementState)
	{
	case EMovementState::Aim_State:
		ResSpeed = MovementInfo.AimSpeed;
		break;
	case EMovementState::Walk_State:
		ResSpeed = MovementInfo.WalkSpeed;
		break;
	case EMovementState::AimWalk_State:
		ResSpeed = MovementInfo.AimWalkSpeed;
		break;
	case EMovementState::Run_State:
		ResSpeed = MovementInfo.RunSpeed;
		break;
	case EMovementState::Sprint_State:
		ResSpeed = MovementInfo.SprintSpeed;
		break;
	default:
		break;
	}
	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
}

void AXiCharacter::ChangeMovementWalk()
{
	MovementWalk = true;
	if (MovementAim)
	{
		ChangeMovementState(EMovementState::AimWalk_State);
	}
	else {
		ChangeMovementState(EMovementState::Walk_State);
	}
}

void AXiCharacter::ChangeMovementWalkToRun()
{
	if (HealthComponent)
	{
		if (!HealthComponent->Exhausted)
		{
			MovementWalk = false;
			if (MovementAim)
			{
				ChangeMovementState(EMovementState::Aim_State);
			}
			else {
				ChangeMovementState(EMovementState::Run_State);
			}
		}
	}
	else
	{
		MovementWalk = false;
		if (MovementAim)
		{
			ChangeMovementState(EMovementState::Aim_State);
		}
		else {
			ChangeMovementState(EMovementState::Run_State);
		}
	}
}

void AXiCharacter::ChangeMovementAim()
{
	MovementAim = true;

	if (MovementWalk)
	{
		ChangeMovementState(EMovementState::AimWalk_State);
	}
	else {
		ChangeMovementState(EMovementState::Aim_State);
	}
}

void AXiCharacter::ChangeMovementAimToRun()
{
	if (!MovementSprint)
	{
		StopWeaponAiming();
	}

	if (HealthComponent)
	{
		if (!HealthComponent->Exhausted)
		{
			MovementAim = false;
			if (MovementWalk)
			{
				ChangeMovementState(EMovementState::Walk_State);
			}
			else {
				ChangeMovementState(EMovementState::Run_State);
			}
		}
		else
		{
			MovementAim = false;
			if (MovementWalk)
			{
				ChangeMovementState(EMovementState::Walk_State);
			}
		}
	}
	else
	{
		MovementAim = false;
		if (MovementWalk)
		{
			ChangeMovementState(EMovementState::Walk_State);
		}
		else {
			ChangeMovementState(EMovementState::Run_State);
		}
	}
}

void AXiCharacter::ChangeMovementSprint()
{
	if (MovementAim)
	{
		StopWeaponAiming();
	}

	if (HealthComponent)
	{
		if (!HealthComponent->Exhausted) // ��������� ������ ����� �������, ����� �������� ���������
		{
			MovementSprint = true;
			ChangeMovementState(EMovementState::Sprint_State);
			HealthComponent->StartChangeStamina();
		}
	}
	else
	{
		MovementSprint = true;
		ChangeMovementState(EMovementState::Sprint_State);
	}
}

void AXiCharacter::ChangeMovementSprintToRun()
{
	if (HealthComponent)
	{
		HealthComponent->StopChangeStamina();
		MovementSprint = false;
		if (MovementWalk)
		{
			ChangeMovementWalk();
		}
		else if (MovementAim)
		{
			ChangeMovementAim();
		}
		else {
			ChangeMovementState(EMovementState::Run_State);
		}
	}
	else
	{
		MovementSprint = false;
		if (MovementWalk)
		{
			ChangeMovementWalk();
		}
		else if (MovementAim)
		{
			ChangeMovementAim();
		}
		else {
			ChangeMovementState(EMovementState::Run_State);
		}
	}
}

void AXiCharacter::ChangeMovementState(EMovementState NemMovementState)
{

	if (MovementSprint)
	{
		MovementState = EMovementState::Sprint_State;
		IsAim = false;
		CharacterUpdate();
	}
	else {
		if (MovementAim)
		{
			IsAim = MovementAim;

			if (bModuleAimingTarget)
			{
				bSearchAimingTarget = true;
			}
			// todo ������ aimingtarget

			StartLaserBeam();

			if (CheckIsOpticalAiming())
			{
				StartOpticalAiming();
			}
		}
		else
		{
			IsAim = false;
		}
		MovementState = NemMovementState;
		CharacterUpdate();
	}

	//Weapon state update
	AWeaponDefault* myWeapon = GetCurrentWeapon();
	if (myWeapon)
	{
		myWeapon->UpdateStateWeapon(MovementState);
	}

}

bool AXiCharacter::GetIsCharacterMoving()
{
	return bIsCurrentCharacterMoving;
}

void AXiCharacter::SetIsCharacterMoving(bool NewMotion)
{
	bIsCurrentCharacterMoving = NewMotion;
}

void AXiCharacter::TryAbilityEnabled()
{

	if (AbilityEffect)//TODO Cool down
	{
		MyNameHitBone = FName(TEXT("head"));
		UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::TryAbilityEnabled() MyNameHitBone: %s"), *MyNameHitBone.ToString());
		UTypes::AddEffectBySurfaceType(this, AbilityEffect, GetSurfuceType(), MyNameHitBone);

		/*UXi_StateEffect* NewEffect = NewObject<UXi_StateEffect>(this, AbilityEffect);
		if (NewEffect)
		{
			NewEffect->InitObject(this);
		}*/
	}
}

UDecalComponent* AXiCharacter::GetCursorToWorld()
{
	return CurrentCursor;
}


//ToDO in one func TrySwitchPreviosWeapon && TrySwicthNextWeapon
//need Timer to Switch with Anim, this method stupid i must know switch success for second logic inventory
//now we not have not success switch/ if 1 weapon switch to self
void AXiCharacter::TrySwicthNextWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		}

		if (InventoryComponent)
		{
			int32 NewCurrentIndex = 0;
			if (InventoryComponent->IsSwitchWeaponToIndex(CurrentIndexWeapon + 1, NewCurrentIndex, OldIndex, true))
			{
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
				if (MovementAim)
				{
					StopWeaponAiming();
				}
				InventoryComponent->SwitchWeaponToIndex(NewCurrentIndex, OldIndex, OldInfo);
				//����� �������� ������ �� ������ ���� ������ �� ����������
			}
		}
	}
	else
	{
		/*int8 CorrectIndex = (CurrentIndexWeapon + 1);
		if (CorrectIndex > InventoryComponent->WeaponSlots.Num() - 1)
			CorrectIndex = 0;
		else
			if (CorrectIndex < 0)
				CorrectIndex = InventoryComponent->WeaponSlots.Num() - 1;
		InventoryComponent->OnSelectEmptyWeapon.Broadcast((EWeaponType)CorrectIndex);*/
	}
}

void AXiCharacter::TrySwitchPreviosWeapon()
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		//We have more then one weapon go switch
		int8 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		}

		if (InventoryComponent)
		{
			int32 NewCurrentIndex = 0;
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->IsSwitchWeaponToIndex(CurrentIndexWeapon - 1, NewCurrentIndex, OldIndex, false))
			{
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();
				if (MovementAim)
				{
					StopWeaponAiming();
				}
				InventoryComponent->SwitchWeaponToIndex(NewCurrentIndex, OldIndex, OldInfo);
				//����� �������� ������ �� ������ ���� ������ �� ����������
			}
		}
	}
	else
	{
		/*int8 CorrectIndex = (CurrentIndexWeapon - 1);
		if (CorrectIndex > InventoryComponent->WeaponSlots.Num() - 1)
			CorrectIndex = 0;
		else
			if (CorrectIndex < 0)
				CorrectIndex = InventoryComponent->WeaponSlots.Num() - 1;
		InventoryComponent->OnSelectEmptyWeapon.Broadcast((EWeaponType)CorrectIndex);*/
	}
}

void AXiCharacter::TrySwitchToSelectWeapon(int32 SelectedWeapon)
{
	if (InventoryComponent->WeaponSlots.Num() > 1)
	{
		int32 OldIndex = CurrentIndexWeapon;
		FAdditionalWeaponInfo OldInfo;
		if (CurrentWeapon)
		{
			OldInfo = CurrentWeapon->AdditionalWeaponInfo;
		}

		if (InventoryComponent)
		{
			int32 NewCurrentIndex = 0;
			//InventoryComponent->SetAdditionalInfoWeapon(OldIndex, GetCurrentWeapon()->AdditionalWeaponInfo);
			if (InventoryComponent->IsSwitchWeaponToIndex(SelectedWeapon, NewCurrentIndex, OldIndex, false, true))
			{
				if (CurrentWeapon->WeaponReloading)
					CurrentWeapon->CancelReload();

				if (MovementAim)
				{
					StopWeaponAiming();
				}
				InventoryComponent->SwitchWeaponToIndex(NewCurrentIndex, OldIndex, OldInfo);
				//����� �������� ������ �� ������ ���� ������ �� ����������
			}
		}
	}
	else
	{
		//InventoryComponent->OnSelectEmptyWeapon.Broadcast((EWeaponType)SelectedWeapon);
	}
}

void AXiCharacter::TryPickUpItem()
{

}

EPhysicalSurface AXiCharacter::GetSurfuceType()
{
	EPhysicalSurface ResultSurface = EPhysicalSurface::SurfaceType_Default;
	if (HealthComponent)
	{
		if (HealthComponent->GetShieldDestroyed() || !HealthComponent->GetShieldExist())
		{
			if (GetMesh())
			{
				UMaterialInterface* myMaterial = GetMesh()->GetMaterial(0);
				if (myMaterial)
				{
					ResultSurface = myMaterial->GetPhysicalMaterial()->SurfaceType;
				}
			}
		}
	}
	return ResultSurface;
}

FName AXiCharacter::GetBoneName()
{
	UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::GetBoneName() MyNameHitBone: %s"), *MyNameHitBone.ToString());
	return MyNameHitBone;
}

void AXiCharacter::SetBoneName(FName NameHitBone)
{
	MyNameHitBone = NameHitBone;
}

USceneComponent* AXiCharacter::GetObjectMesh()
{
	return GetMesh();
}

TArray<UXi_StateEffect*> AXiCharacter::GetAllCurrentEffects()
{
	return Effects;
}

UXi_StateEffect* AXiCharacter::GetActiveEffect(TSubclassOf<UXi_StateEffect> AddEffectClass)
{
	bool bIsSingleEffect = true; //���� ������ ������������?
	int32 j = 0;
	if (Effects.Num() > 0)
	{
		while (j < Effects.Num() && bIsSingleEffect)
		{
			if (Effects[j]->GetClass() == AddEffectClass)
			{
				bIsSingleEffect = false;
				return Effects[j];
			}
			j++;
		}
	}
	return nullptr;
}

void AXiCharacter::RemoveEffect(UXi_StateEffect* RemoveEffect)
{
	Effects.Remove(RemoveEffect);
}

void AXiCharacter::AddEffect(UXi_StateEffect* newEffect)
{
	Effects.Add(newEffect);
}

void AXiCharacter::SwitchStunEffect(bool OnStun)
{
	bIsStun = OnStun;
	if (OnStun)
	{
		//�������� ������ ����������� � ��
	//	CurrentPlayerController = Cast<AXiPlayerController>(GetController());
	//	UnPossessed();
		GetCharacterMovement()->DisableMovement();
		if (AnimCharStun && GetMesh() && GetMesh()->GetAnimInstance())
		{
			GetMesh()->GetAnimInstance()->Montage_Play(AnimCharStun);
		}
		if (CharStunParticleEffect)
		{
			FName NameBoneToAttached(TEXT("head"));
			FVector Loc = FVector(0);
			FRotator Rot = FRotator(90.f,0.f,0.f); // FRotator::ZeroRotator
			CharStunParticleEmitter = UGameplayStatics::SpawnEmitterAttached(CharStunParticleEffect, GetMesh(), NameBoneToAttached, Loc, Rot, FVector(StunEffectParticalScale), EAttachLocation::SnapToTarget, false);
		}
	}
	else
	{
		if (AnimCharStun && GetMesh() && GetMesh()->GetAnimInstance())
		{
			GetMesh()->GetAnimInstance()->Montage_Stop(0.25f, AnimCharStun);
			//GetMesh()->GetAnimInstance()->Montage_Play(AnimCharStun);
		}
		CharStunParticleEmitter->DestroyComponent();
		CharStunParticleEmitter = nullptr;
		GetCharacterMovement()->SetDefaultMovementMode();
//		PossessedBy(CurrentPlayerController);
	}
}

void AXiCharacter::CharDead()
{
	//UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::CharDead()"));
	float TimeAnim = 0.0f;
	int32 rnd = FMath::RandHelper(DeadsAnim.Num());
	if (DeadsAnim.IsValidIndex(rnd) && DeadsAnim[rnd] && GetMesh() && GetMesh()->GetAnimInstance())
	{
	//	UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::CharDead() 2"));
		TimeAnim = DeadsAnim[rnd]->GetPlayLength();
		GetMesh()->GetAnimInstance()->Montage_Play(DeadsAnim[rnd]);
	}

	bIsAlive = false;

	UnPossessed();

	//Timer rag doll
	GetWorldTimerManager().SetTimer(TimerHandle_RagDollTimer, this, &AXiCharacter::EnableRagdoll, TimeAnim, false);

	GetCursorToWorld()->SetVisibility(false);
}

void AXiCharacter::ExhaustedChangeMovement(bool bIsExhausted)
{
	UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::ExhaustedChangeMovement() "));
	if(bIsExhausted)
	{
		UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::ExhaustedChangeMovement() bIsExhausted=true"));
		MovementSprint = false;
		ChangeMovementWalk();
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::ExhaustedChangeMovement() bIsExhausted=false"));
		ChangeMovementWalkToRun();
	}
}

void AXiCharacter::EnableRagdoll()
{
	//UE_LOG(LogTemp, Warning, TEXT("AXiCharacter::EnableRagdoll()"));
	if (GetMesh())
	{
		GetMesh()->SetCollisionEnabled(ECollisionEnabled::PhysicsOnly);
		GetMesh()->SetSimulatePhysics(true);
	}
}

float AXiCharacter::TakeDamage(float DamageAmount, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, AActor* DamageCauser)
{
	float ActualDamage = Super::TakeDamage(DamageAmount, DamageEvent, EventInstigator, DamageCauser);
	if (bIsAlive)
	{
		HealthComponent->ChangeHealthValue(-DamageAmount);
	}

	if (DamageEvent.IsOfType(FRadialDamageEvent::ClassID))
	{
		AProjectileDefault* myProjectile = Cast<AProjectileDefault>(DamageCauser);
		if (myProjectile)
		{
			UTypes::AddEffectBySurfaceType(this, myProjectile->ProjectileSetting.Effect, GetSurfuceType(), MyNameHitBone);
		}
	}

	return ActualDamage;
}

bool AXiCharacter::PickUpShield(int32 NumPickUpShieldCharge)
{
	bool PickUpSuccess = false;
	if (InventoryComponent && HealthComponent)
	{
		if (NumPickUpShieldCharge > 0)
		{
			if (NumPickUpShieldCharge > InventoryComponent->PortableSlots[(int32)EPortableType::ShieldCharge].MaxCout)
			{
				NumPickUpShieldCharge = InventoryComponent->PortableSlots[(int32)EPortableType::ShieldCharge].MaxCout;
			}
			InventoryComponent->PortableSlots[(int32)EPortableType::ShieldCharge].Cout = NumPickUpShieldCharge;
			PickUpSuccess = true;
			// add Broadcast to widget change ShieldCharge
		}
		if (!HealthComponent->GetShieldExist()) //��������� ��� ������������� ���� ���� ������
		{
			if (InventoryComponent->ChangePortableCoutByEnum(EPortableType::ShieldCharge))
			{
				HealthComponent->SetShieldExist(true);
				HealthComponent->LaunchShield();
			}
		}
	}
	return PickUpSuccess;
}

void AXiCharacter::RestartShield(bool NewShieldDestroyed)
{
	if (HealthComponent && InventoryComponent)
	{
		if (NewShieldDestroyed)
		{
			if (!InventoryComponent->ChangePortableCoutByEnum(EPortableType::ShieldCharge))
			{
				HealthComponent->SetShieldExist(false);
				HealthComponent->ClearShieldTimer();
			}
		}
		else
		{
		}
	}
}
