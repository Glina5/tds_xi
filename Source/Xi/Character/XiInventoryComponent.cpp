// Fill out your copyright notice in the Description page of Project Settings.


#include "XiInventoryComponent.h"
#include "../Game/XiGameInstance.h"
#pragma optimize ("", off)

// Sets default values for this component's properties
UXiInventoryComponent::UXiInventoryComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...

//	UE_LOG(LogTemp, Warning, TEXT("Inventory Constract"));

	//if (WeaponSlots.IsValidIndex(0))
	//{
	//	FAmmoSlot AmmoForWeapon;
	//	AmmoSlots2.Init(AmmoForWeapon, WeaponSlots.Num());
	//	for (int8 i = 0; i < WeaponSlots.Num(); i++)
	//	{
	//		UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
	//		if (myGI)
	//		{
	//			if (!WeaponSlots[i].NameItem.IsNone())
	//			{
	//				//AmmoSlots2.init
	//				FWeaponInfo Info;
	//				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
	//					AmmoSlots2[i].WeaponType = Info.WeaponType;
	//				else
	//				{
	//					//WeaponSlots.RemoveAt(i);
	//					//i--;
	//				}
	//			}
	//		}
	//	}
	//}

}


// Called when the game starts
void UXiInventoryComponent::BeginPlay()
{
	Super::BeginPlay();

//	UE_LOG(LogTemp, Warning, TEXT("Inventory BeginPlay"));
	// ...
	
	//Find init weaponsSlots and First Init Weapon

	//From Skillbox first
	//FAmmoSlot AmmoForWeapon;
	//AmmoSlots.Init(AmmoForWeapon, WeaponSlots.Num());
	//for (int8 i = 0; i < WeaponSlots.Num(); i++)
	//{
	//	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
	//	if (myGI)
	//	{
	//		if (!WeaponSlots[i].NameItem.IsNone())
	//		{
	//			FWeaponInfo Info;
	//			if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
	//			{
	//				WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
	//				// �������������� ���������� ������� AmmoSlots
	//				AmmoSlots[i].WeaponType = Info.WeaponType;
	//				if (StartAmmoForCurrentWeapon.IsValidIndex(0))
	//				{
	//					bool bIsFind = false;
	//					int8 j = 0;
	//					while (j<StartAmmoForCurrentWeapon.Num(), !bIsFind)
	//					{
	//						if (StartAmmoForCurrentWeapon[j].WeaponType == AmmoSlots[i].WeaponType)
	//						{
	//							//good weapon have ammo start change
	//							AmmoSlots[i].Cout = StartAmmoForCurrentWeapon[j].Cout;
	//							AmmoSlots[i].MaxCout = StartAmmoForCurrentWeapon[j].MaxCout;
	//							bIsFind = true;
	//						}
	//						j++;
	//					}
	//				}
	//			}
	//			else
	//			{
	//				//WeaponSlots.RemoveAt(i);
	//				//i--;
	//			}
	//		}
	//	}
	//}

//	AllWeaponModules.Init(0, TotalNumberOfWeaponModules);
//	if (AllWeaponModules.IsValidIndex(0))
	{
		UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			FWeaponModulesInfo NewWeaponModule;
			int32 AllWeaponModulesNumber = 0;
			if (myGI->GetWeaponModulesNumber(AllWeaponModulesNumber))
			{
				AllWeaponModules.Init(NewWeaponModule, AllWeaponModulesNumber);
			}
			if (myGI->GetAllWeaponModules(AllWeaponModules))
			{

			}
			//FWeaponModulesInfo ModulesInfo;

			/*if (myGI->GetWeaponModulesNumber(AllWeaponModules[0]))
			{
				if (AllWeaponModules.IsValidIndex(AllWeaponModules[0]))
				{
					int8 j = 1;
					for (j = 1; j < AllWeaponModules[0]; j++)
					{
						AllWeaponModules[j] = 0;
					}
				}
			}*/
		}
	}


	FWeaponSlot WeaponInSlot;
	WeaponSlots.Init(WeaponInSlot, MaxInventorySlotsWeaponAndAmmo);

	FAmmoSlot AmmoForWeapon;
	AmmoSlots.Init(AmmoForWeapon, (int32)EAmmoType::MaxAmmoType);

	FPortableSlot PortableInSlot;
	PortableSlots.Init(PortableInSlot, (int32)EPortableType::MaxPortableType);

	for (uint8 i = 0; i < PortableSlots.Num(); i++)
	{
		PortableSlots[i].PortableType = (EPortableType)i;
	}

	if (StartPortableSlots.IsValidIndex(0))
	{
		bool bIsFind = false;
		int8 j = 0;
		for (j = 0; j < StartPortableSlots.Num(); j++)
		{
			if (StartPortableSlots.IsValidIndex(j))
			{
				//������� ��������� ���������� ������������ �������� ������� ����.
				PortableSlots[(int32)StartPortableSlots[j].PortableType].Cout = StartPortableSlots[j].Cout;
				PortableSlots[(int32)StartPortableSlots[j].PortableType].MaxCout = StartPortableSlots[j].MaxCout;
			}
		}
	}

	for (uint8 i = 0; i < AmmoSlots.Num(); i++)
	{
		AmmoSlots[i].AmmoType = (EAmmoType)i;
	}


	if (StartAmmoForCurrentWeapon.IsValidIndex(0))
	{
		bool bIsFind = false;
		int8 j = 0;
		for (j = 0; j < StartAmmoForCurrentWeapon.Num(); j++)
		{
			if (StartAmmoForCurrentWeapon.IsValidIndex(j))
			{
				//������� ��������� ���������� ������ ������� ����.
				AmmoSlots[(int32)StartAmmoForCurrentWeapon[j].AmmoType].Cout = StartAmmoForCurrentWeapon[j].Cout;
				AmmoSlots[(int32)StartAmmoForCurrentWeapon[j].AmmoType].MaxCout = StartAmmoForCurrentWeapon[j].MaxCout;
			}
		}
	}

	if (StartWeaponSlots.IsValidIndex(0))
	{
		UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			FWeaponInfo Info;
			int8 j = 0;
			for (j = 0; j < StartWeaponSlots.Num(); j++)
			{
				if (StartWeaponSlots.IsValidIndex(j))
				{
					if (myGI->GetWeaponInfoByName(StartWeaponSlots[j].NameItem, Info))
					{
						//������ ������ �� �����.
						WeaponSlots[(int32)Info.WeaponType].NameItem = StartWeaponSlots[j].NameItem;

					}
				}
			}
		}
	}

	//TO Do
	for (uint8 i = 0; i < WeaponSlots.Num(); i++)
	{
		UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (!WeaponSlots[i].NameItem.IsNone())
			{
				FWeaponInfo Info;
				if (myGI->GetWeaponInfoByName(WeaponSlots[i].NameItem, Info))
				{
					if ((uint8)Info.WeaponType == i)
					{
						WeaponSlots[i].AdditionalInfo.WeaponSlotModulesNumber = Info.CurrentWeaponModulesNumber;
						FWeaponModulesInfo NewWeaponModule;
						WeaponSlots[i].AdditionalInfo.PlacingWeaponModules.Init(NewWeaponModule, Info.CurrentWeaponModulesNumber);
						WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex.Init(0, static_cast<int32>(EModulesType::MaxModulesType));
						
						if (AllWeaponModules.IsValidIndex(0))
						{
							int8 j = 0;
							for (j = 0; j < AllWeaponModules.Num(); j++)
							{
								if (AllWeaponModules[j].WeaponCompatibleName == WeaponSlots[i].NameItem)
								{
									if (WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex.IsValidIndex(static_cast<int32>(AllWeaponModules[j].CurrentModuleType)))
									{
										int32 ModuleIndex = 0;
										ModuleIndex = static_cast<int32>(AllWeaponModules[j].CurrentModuleType);
										ModuleIndex = j;
										WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex[static_cast<int32>(AllWeaponModules[j].CurrentModuleType)] = j;
									}
								}
							}
						}

						WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;

						if (AmmoSlots[(int32)Info.AmmoType].Cout > 0)
						{
							WeaponSlots[i].bIsEmpty = false;
						}
						else
						{
							WeaponSlots[i].bIsEmpty = true;
						}

					}
					else
					{
						//�������� ������ ������ ���������� ������ � �������, ���������� �� ���� ������. � �������� ��� � ��� ������. �����������
						if (myGI->GetForciblyWeaponInfoByWeaponType((EWeaponType)i, Info,WeaponSlots[i].NameItem))
						{
							WeaponSlots[i].AdditionalInfo.WeaponSlotModulesNumber = Info.CurrentWeaponModulesNumber;
							FWeaponModulesInfo NewWeaponModule;
							WeaponSlots[i].AdditionalInfo.PlacingWeaponModules.Init(NewWeaponModule, Info.CurrentWeaponModulesNumber);
							WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex.Init(0, static_cast<int32>(EModulesType::MaxModulesType));

							if (AllWeaponModules.IsValidIndex(0))
							{
								int8 j = 0;
								for (j = 0; j < AllWeaponModules.Num(); j++)
								{
									if (AllWeaponModules[j].WeaponCompatibleName == WeaponSlots[i].NameItem)
									{
										if (WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex.IsValidIndex(static_cast<int32>(AllWeaponModules[j].CurrentModuleType)))
										{
											int32 ModuleIndex = 0;
											ModuleIndex = static_cast<int32>(AllWeaponModules[j].CurrentModuleType);
											ModuleIndex = j;
											WeaponSlots[i].AdditionalInfo.CurrentWeaponModulesIndex[static_cast<int32>(AllWeaponModules[j].CurrentModuleType)] = j;
										}
									}
								}
							}

							WeaponSlots[i].AdditionalInfo.Round = Info.MaxRound;
						}
					}
				}
				else
				{
				}
			}
		}
	}

//	MaxSlotsWeapon = WeaponSlots.Num();
	
	OnShowUIWeaponInfo.Broadcast(); //����������� �� ����, ��� ������ ������ ������

	if (WeaponSlots.IsValidIndex(0))
	{
		if (!WeaponSlots[0].NameItem.IsNone())
			OnSwitchWeapon.Broadcast(WeaponSlots[0].NameItem, WeaponSlots[0].AdditionalInfo, 0);
	}

}


// Called every frame
void UXiInventoryComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

//TODO OMG Refactoring need!!!
bool UXiInventoryComponent::IsSwitchWeaponToIndex(int32 ChangeToIndex, int32& CorrectChangeeToIndex, int32 OldIndex, bool bIsForward, bool bIsSelectedWeapon)
{
	bool bIsSuccess = false;
	int8 CorrectIndex = ChangeToIndex;
	if (ChangeToIndex > WeaponSlots.Num() - 1)
		CorrectIndex = 0;
	else
		if (ChangeToIndex < 0)
			CorrectIndex = WeaponSlots.Num() - 1;

	if (CorrectIndex == OldIndex) //���� ����������� ������� �������� � ������ ������� (������� ������ �� �������������)
	{
		if (!bIsSelectedWeapon)
		{
			if (bIsForward)
			{
				CorrectIndex++;
			}
			else
			{
				CorrectIndex--;
			}
			if (CorrectIndex > WeaponSlots.Num() - 1)
				CorrectIndex = 0;
			else
				if (CorrectIndex < 0)
					CorrectIndex = WeaponSlots.Num() - 1;
			OnSelectEmptyWeapon.Broadcast((EWeaponType)CorrectIndex);
		}
		return bIsSuccess;
	}

	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (!WeaponSlots[CorrectIndex].bIsEmpty || (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0))
			{
				//good weapon have ammo start change
			bIsSuccess = true;
			}
			else
			{
				//ADD logic
				bIsSuccess = false;
				// �������� ����������� �� ������ �� ������ � ����� ������
			}
		}
	}

	if (!bIsSuccess)
	{
		if (!bIsSelectedWeapon)
		{
		if (bIsForward)
		{
			return IsSwitchWeaponToIndex(CorrectIndex + 1, CorrectChangeeToIndex, OldIndex, bIsForward);
		}
		else
		{
			return IsSwitchWeaponToIndex(CorrectIndex - 1, CorrectChangeeToIndex, OldIndex, bIsForward);
		}
		}
		else
		{
			//TO DO �������, � ������ �� �������� ������ � ������ �����
			OnSelectEmptyWeapon.Broadcast((EWeaponType)CorrectIndex);
			return bIsSuccess;
		}
	}


	if (bIsSuccess)
	{
		CorrectChangeeToIndex = CorrectIndex;
	}


	return bIsSuccess;

	/*
	if (WeaponSlots.IsValidIndex(CorrectIndex))
	{
		if (!WeaponSlots[CorrectIndex].NameItem.IsNone())
		{
			if (WeaponSlots[CorrectIndex].AdditionalInfo.Round > 0)
			{
				//good weapon have ammo start change
				bIsSuccess = true;
			}
			else
			{
				UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
				if (myGI)
				{
					//check ammoSlots for this weapon
					FWeaponInfo myInfo;
					myGI->GetWeaponInfoByName(WeaponSlots[CorrectIndex].NameItem, myInfo);

					bool bIsFind = false;
					int8 j = 0;
					while (j < AmmoSlots.Num() && !bIsFind)
					{
						if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
						{
							//good weapon have ammo start change
							bIsSuccess = true;
							bIsFind = true;
						}
						j++;
					}
				}
			}
			if (bIsSuccess)
			{
				NewCurrentIndex = CorrectIndex;
				NewIdWeapon = WeaponSlots[CorrectIndex].NameItem;
				NewAdditionalInfo = WeaponSlots[CorrectIndex].AdditionalInfo;
			}
		}
	}

	if (!bIsSuccess)
	{
		if (bIsForward)
		{
			int8 iteration = 0;
			int8 Seconditeration = 0;
			while (iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex + iteration;
				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
						{
							//WeaponGood
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
									NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of right of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
									NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//WeaponGood
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
											NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with amm need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error, TEXT("UXiInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration++;
				}
			}
		}
		else
		{
			int8 iteration = 0;
			int8 Seconditeration = WeaponSlots.Num() - 1;
			while (iteration < WeaponSlots.Num() && !bIsSuccess)
			{
				iteration++;
				int8 tmpIndex = ChangeToIndex - iteration;
				if (WeaponSlots.IsValidIndex(tmpIndex))
				{
					if (!WeaponSlots[tmpIndex].NameItem.IsNone())
					{
						if (WeaponSlots[tmpIndex].AdditionalInfo.Round > 0)
						{
							//WeaponGood
							bIsSuccess = true;
							NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
							NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
							NewCurrentIndex = tmpIndex;
						}
						else
						{
							FWeaponInfo myInfo;
							UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

							myGI->GetWeaponInfoByName(WeaponSlots[tmpIndex].NameItem, myInfo);

							bool bIsFind = false;
							int8 j = 0;
							while (j < AmmoSlots.Num() && !bIsFind)
							{
								if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[tmpIndex].NameItem;
									NewAdditionalInfo = WeaponSlots[tmpIndex].AdditionalInfo;
									NewCurrentIndex = tmpIndex;
									bIsFind = true;
								}
								j++;
							}
						}
					}
				}
				else
				{
					//go to end of LEFT of array weapon slots
					if (OldIndex != Seconditeration)
					{
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood
									bIsSuccess = true;
									NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
									NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
									NewCurrentIndex = Seconditeration;
								}
								else
								{
									FWeaponInfo myInfo;
									UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType && AmmoSlots[j].Cout > 0)
										{
											//WeaponGood
											bIsSuccess = true;
											NewIdWeapon = WeaponSlots[Seconditeration].NameItem;
											NewAdditionalInfo = WeaponSlots[Seconditeration].AdditionalInfo;
											NewCurrentIndex = Seconditeration;
											bIsFind = true;
										}
										j++;
									}
								}
							}
						}
					}
					else
					{
						//go to same weapon when start
						if (WeaponSlots.IsValidIndex(Seconditeration))
						{
							if (!WeaponSlots[Seconditeration].NameItem.IsNone())
							{
								if (WeaponSlots[Seconditeration].AdditionalInfo.Round > 0)
								{
									//WeaponGood, it same weapon do nothing
								}
								else
								{
									FWeaponInfo myInfo;
									UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());

									myGI->GetWeaponInfoByName(WeaponSlots[Seconditeration].NameItem, myInfo);

									bool bIsFind = false;
									int8 j = 0;
									while (j < AmmoSlots.Num() && !bIsFind)
									{
										if (AmmoSlots[j].WeaponType == myInfo.WeaponType)
										{
											if (AmmoSlots[j].Cout > 0)
											{
												//WeaponGood, it same weapon do nothing
											}
											else
											{
												//Not find weapon with amm need init Pistol with infinity ammo
												UE_LOG(LogTemp, Error, TEXT("UXiInventoryComponent::SwitchWeaponToIndex - Init PISTOL - NEED"));
											}
										}
										j++;
									}
								}
							}
						}
					}
					Seconditeration--;
				}
			}
		}
	}

	if (bIsSuccess)
	{
		SetAdditionalInfoWeapon(OldIndex, OldInfo);
		OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex);
		//OnWeaponAmmoAviable.Broadcast()
	}


	return bIsSuccess;
	*/
}

void UXiInventoryComponent::SwitchWeaponToIndex(int32 CorrectChangeToIndex, int32 OldIndex, FAdditionalWeaponInfo OldInfo)
{
	FName NewIdWeapon;
	FAdditionalWeaponInfo NewAdditionalInfo;
	int32 NewCurrentIndex = 0;

	NewCurrentIndex = CorrectChangeToIndex;
	NewIdWeapon = WeaponSlots[CorrectChangeToIndex].NameItem;
	NewAdditionalInfo = WeaponSlots[CorrectChangeToIndex].AdditionalInfo;
	SetAdditionalInfoWeapon(OldIndex, OldInfo);
	OnSwitchWeapon.Broadcast(NewIdWeapon, NewAdditionalInfo, NewCurrentIndex); // �������� ����������� �� ������ �� ������ � ����� ������
	//OnWeaponAmmoAviable.Broadcast()

}

FAdditionalWeaponInfo UXiInventoryComponent::GetAdditionalInfoWeapon(int32 IndexWeapon)
{
	FAdditionalWeaponInfo result;
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		bool bIsFind = false;
		int8 i = 0;
		while (i < WeaponSlots.Num() && !bIsFind)
		{
			if (/*WeaponSlots[i].IndexSlot*/i == IndexWeapon)
			{
				result = WeaponSlots[i].AdditionalInfo;
				bIsFind = true;
			}
			i++;
		}
		if (!bIsFind)
			UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);

	return result;
}

int32 UXiInventoryComponent::GetWeaponIndexSlotByName(FName IdWeaponName)
{
	int32 result = -1;
	int8 i = 0;
	bool bIsFind = false;
	while (i < WeaponSlots.Num() && !bIsFind)
	{
		if (WeaponSlots[i].NameItem == IdWeaponName)
		{
			bIsFind = true;
			result = i; //WeaponSlots[i].IndexSlot
		}
		i++;
	}
	return result;
}


FName UXiInventoryComponent::GetWeaponNameBySlotIndex(int32 indexSlot)
{
	FName result;

	if (WeaponSlots.IsValidIndex(indexSlot))
	{
		result = WeaponSlots[indexSlot].NameItem;
	}
	return result;
}


void UXiInventoryComponent::SetAdditionalInfoWeapon(int32 IndexWeapon, FAdditionalWeaponInfo NewInfo)
{
	//����� �������� ����
	//if (WeaponSlots.IsValidIndex(IndexWeapon))
	//{
	//	bool bIsFind = false;
	//	int8 i = 0;
	//	while (i < WeaponSlots.Num() && !bIsFind)
	//	{
	//		if (/*WeaponSlots[i].IndexSlot*/i == IndexWeapon)
	//		{
	//			WeaponSlots[i].AdditionalInfo = NewInfo;
	//			bIsFind = true;
	//			OnWeaponAdditionalInfoChange.Broadcast(IndexWeapon, NewInfo); // ������� ��� ��������� � �������
	//		}
	//		i++;
	//	}
	//	if (!bIsFind)
	//		UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetAdditionalInfoWeapon - No Found Weapon with index - %d"), IndexWeapon);
	//}
	//else
	//	UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
	if (WeaponSlots.IsValidIndex(IndexWeapon))
	{
		WeaponSlots[IndexWeapon].AdditionalInfo = NewInfo;

		OnWeaponAdditionalInfoChange.Broadcast(WeaponSlots[IndexWeapon].NameItem, NewInfo); // ������� ��� ��������� � �������

		/*if (WeaponSlots[IndexWeapon].bIsEmpty) //�������, ����� ������� ��������� ����������� ��������, �� ����������� ��������������
		{
			FWeaponSlotsWithNoAmmo EmptyWeapon;
			EmptyWeapon.Slots.Add(WeaponSlots[IndexWeapon]);
			OnWeaponAmmoEmpty.Broadcast(EAmmoType::MaxAmmoType, EmptyWeapon);
		}*/

	}
	else
		UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetAdditionalInfoWeapon - Not Correct index Weapon - %d"), IndexWeapon);
}

void UXiInventoryComponent::ChangeWeaponSlotsIsEmpty(EAmmoType TypeAmmo, bool NewStateEmpty)
{
	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
	FWeaponInfo Info;
	if (myGI)
	{
		int8 j = 0;
		for (j = 0; j < WeaponSlots.Num(); j++)
		{
			if (WeaponSlots.IsValidIndex(j))
			{
				if (myGI->GetWeaponInfoByName(WeaponSlots[j].NameItem, Info))
				{
					if (Info.AmmoType == TypeAmmo)
					{
						WeaponSlots[j].bIsEmpty = NewStateEmpty;
						if (WeaponSlots[j].AdditionalInfo.Round <= 0 || NewStateEmpty == false)
						{
						OnWeaponIsEmpty.Broadcast(Info.WeaponType, NewStateEmpty);
						}
					}
				}
			}
		}
	}
}

void UXiInventoryComponent::CheckWeaponIsEmpty(EWeaponType TypeWeapon)
{
	bool bIsWeaponEmpty = false;
	if (WeaponSlots[(int32)TypeWeapon].AdditionalInfo.Round > 0 || !WeaponSlots[(int32)TypeWeapon].bIsEmpty)
	{
		bIsWeaponEmpty = false;
	}
	else
	{
		bIsWeaponEmpty = true;
	}
	OnWeaponIsEmpty.Broadcast(TypeWeapon, bIsWeaponEmpty);
}

TArray<FName> UXiInventoryComponent::GetAvaliableWeaponName()
{
	TArray<FName> AvaliableWeaponName;
		if (WeaponSlots.IsValidIndex(0))
		{
			AvaliableWeaponName.Init("", WeaponSlots.Num());
			int8 j = 0;
			for (j = 0; j < WeaponSlots.Num(); j++)
			{
				if (WeaponSlots.IsValidIndex(j))
				{
					if (!WeaponSlots[j].NameItem.IsNone())
					{
						AvaliableWeaponName[j] = WeaponSlots[j].NameItem;
					}
				}
			}
		}
		return AvaliableWeaponName;
}

TArray<FWeaponModulesInfo> UXiInventoryComponent::GetChosenWeaponModules(FName WeaponName)
{
	FWeaponModulesInfo CurrentWeaponModuleInfo;
	TArray<FWeaponModulesInfo> CurrentWeaponAllModuleInfo;
	int32 WeaponIndexBySlots = -1;
	WeaponIndexBySlots = GetWeaponIndexSlotByName(WeaponName);
	if (WeaponIndexBySlots >= 0)
	{
		CurrentWeaponAllModuleInfo.Init(CurrentWeaponModuleInfo, WeaponSlots[WeaponIndexBySlots].AdditionalInfo.WeaponSlotModulesNumber);
		if (CurrentWeaponAllModuleInfo.Num() > 0 && CurrentWeaponAllModuleInfo.IsValidIndex(0))
		{
			int8 j = 0;
			for (j = 0; j < CurrentWeaponAllModuleInfo.Num(); j++)
			{
				if (WeaponSlots[WeaponIndexBySlots].AdditionalInfo.PlacingWeaponModules.IsValidIndex(j))
				{
					CurrentWeaponAllModuleInfo[j] = WeaponSlots[WeaponIndexBySlots].AdditionalInfo.PlacingWeaponModules[j];
				}
			}
		}
	}
	return CurrentWeaponAllModuleInfo;
}

TArray<FWeaponModulesInfo> UXiInventoryComponent::GetAvaliableWeaponModules(FName WeaponName)
{
	FWeaponModulesInfo CurrentWeaponModuleInfo;
	TArray<FWeaponModulesInfo> CurrentWeaponAllModuleInfo;
	CurrentWeaponAllModuleInfo.Init(CurrentWeaponModuleInfo, static_cast<int32>(EModulesType::MaxModulesType));
	int32 WeaponIndexBySlots = -1;
	WeaponIndexBySlots = GetWeaponIndexSlotByName(WeaponName);
	if (WeaponIndexBySlots >= 0)
	{
		if (CurrentWeaponAllModuleInfo.IsValidIndex(0))
		{
			if (WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex.IsValidIndex(0))
			{
				int8 j = 0;
				for (j = 0; j < WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex.Num(); j++)
				{
					if (WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex[j] != 0)
					{
						if (CurrentWeaponAllModuleInfo.IsValidIndex(j))
						{
							CurrentWeaponAllModuleInfo[j] = AllWeaponModules[WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex[j]];
						}
					}
				}
			}
		}
	}
	return CurrentWeaponAllModuleInfo;
}

bool UXiInventoryComponent::SetModuleToWeaponInCell(FName WeaponName, EModulesType NewModuleType, int32 ModuleCellNumber)
{
	bool result = true;
	int32 WeaponIndexBySlots = -1;
	WeaponIndexBySlots = GetWeaponIndexSlotByName(WeaponName);
	if (WeaponIndexBySlots >= 0)
	{
		if (WeaponSlots.IsValidIndex(WeaponIndexBySlots))
		{
			if (WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex.IsValidIndex(static_cast<int32>(NewModuleType)))
			{
				if (AllWeaponModules.IsValidIndex(WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex[static_cast<int32>(NewModuleType)]))
				{
					if (WeaponSlots[WeaponIndexBySlots].AdditionalInfo.PlacingWeaponModules.IsValidIndex(ModuleCellNumber))
					{
						WeaponSlots[WeaponIndexBySlots].AdditionalInfo.PlacingWeaponModules[ModuleCellNumber] = AllWeaponModules[WeaponSlots[WeaponIndexBySlots].AdditionalInfo.CurrentWeaponModulesIndex[static_cast<int32>(NewModuleType)]];
						// �� ���������, ���������� ���������� ������, ���������� ����� ����������, �� ������ ������� ���������� � �������� ����������������
						SwitchWeaponToIndex(WeaponIndexBySlots, WeaponIndexBySlots, WeaponSlots[WeaponIndexBySlots].AdditionalInfo);
					}
					else result = false;
				}
				else result = false;
			}
			else result = false;
		}
		else result = false;
	}
	else result = false;

	if (result == false)
	{
		UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::SetModuleToWeaponInCell - Something Wrong :D"));
	}

	return result;
}

TArray<FWeaponModulesInfo> UXiInventoryComponent::GetAllWeaponModules()
{
	return AllWeaponModules;
}

/* From Skillbox_first
void UXiInventoryComponent::AmmoSlotChangeValue(EWeaponType TypeWeapon, int32 CoutChangeAmmo) // ���������� �� AXiCharacter::WeaponReloadEnd
{
	bool bIsFind = false;
	int8 i = 0;
	while (i < AmmoSlots.Num() && !bIsFind)
	{
		if (AmmoSlots[i].WeaponType == TypeWeapon)
		{
			AmmoSlots[i].Cout += CoutChangeAmmo;
			if (AmmoSlots[i].Cout > AmmoSlots[i].MaxCout)
				AmmoSlots[i].Cout = AmmoSlots[i].MaxCout;

			OnAmmoChange.Broadcast(AmmoSlots[i].WeaponType, AmmoSlots[i].Cout); // ������� ��� ��������� � �������

			bIsFind = true;
		}
		i++;
	}
}
*/

bool UXiInventoryComponent::AmmoSlotChangeValue(EAmmoType TypeAmmo, int32 CoutChangeAmmo) // ���������� �� AXiCharacter::WeaponReloadEnd
{
	bool bIsEmpty = false;
	bool bWasEmpty = false;
	if (AmmoSlots.IsValidIndex((int32)TypeAmmo))
	{
		if (AmmoSlots[(int32)TypeAmmo].Cout <= 0)
		{
			bWasEmpty = true;
		}
		AmmoSlots[(int32)TypeAmmo].Cout += CoutChangeAmmo;
		if (AmmoSlots[(int32)TypeAmmo].Cout > AmmoSlots[(int32)TypeAmmo].MaxCout)
			AmmoSlots[(int32)TypeAmmo].Cout = AmmoSlots[(int32)TypeAmmo].MaxCout;
		else if (AmmoSlots[(int32)TypeAmmo].Cout <= 0)
		{
			bIsEmpty = true;
			OnAmmoIsEmpty.Broadcast(TypeAmmo, bIsEmpty); // ������� ������ ����������� �������
		}
		if (bWasEmpty && AmmoSlots[(int32)TypeAmmo].Cout > 0)
		{
			OnAmmoIsEmpty.Broadcast(TypeAmmo, !bWasEmpty);
		}

		OnAmmoChange.Broadcast(AmmoSlots[(int32)TypeAmmo].AmmoType, AmmoSlots[(int32)TypeAmmo].Cout); // ������� ��� ��������� � �������
	}
		return bIsEmpty;
}

//void UXiInventoryComponent::InitAmmoWithWeapon(EWeaponType TypeWeapon, FAdditionalWeaponInfo CurrentAdditionalWeaponInfo) //������� ������ ��� �� ����� ��� ����� ���� ��������
//{
//	bool bIsFind = false;
//	int8 i = 0;
//	while (i < AmmoSlots.Num() && !bIsFind)
//	{
//		if (AmmoSlots[i].WeaponType == TypeWeapon)
//		{
//			AmmoSlots[i].Cout = CurrentAdditionalWeaponInfo.Round;
//			bIsFind = true;
//		}
//		i++;
//	}
//}

bool UXiInventoryComponent::GetAmmoForWeapon(EAmmoType TypeAmmo, int32& AviableAmmoForWeapon)
{
	AviableAmmoForWeapon = 0;
	if (AmmoSlots.IsValidIndex((int32)TypeAmmo))
	{
		AviableAmmoForWeapon = AmmoSlots[(int32)TypeAmmo].Cout;
		if (AviableAmmoForWeapon > 0)
		{
			//OnWeaponAmmoAviable.Broadcast(TypeWeapon);//remove not here, only when pickUp ammo this type, or swithc weapon
			return true;
		}

	}
	
	//FWeaponSlotsWithNoAmmo EmptyWeapon;
	//if (GetAllWeaponWithCurrentAmmo(TypeAmmo, EmptyWeapon))
	//{
	//	//TODO ���� �� ������ ����� ������
	//}

//	OnWeaponAmmoEmpty.Broadcast(TypeAmmo, EmptyWeapon);//visual empty ammo slot ������� ��� �������

	UE_LOG(LogTemp, Warning, TEXT("UXiInventoryComponent::CheckAmmoForWeapon - No Ammo"));
	return false;
}

//bool UXiInventoryComponent::GetAllWeaponWithCurrentAmmo(EAmmoType TypeAmmo, FWeaponSlotsWithNoAmmo& WeaponWithCurrentAmmo)
//{
//	bool Return = false;
//	FWeaponInfo Info;
//	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
//	if (WeaponSlots.IsValidIndex(0))
//	{
//		uint8 j = 0;
//		for (j = 0; j < WeaponSlots.Num(); j++)
//		{
//			if (WeaponSlots.IsValidIndex(j))
//			{
//				if (myGI->GetWeaponInfoByName(WeaponSlots[j].NameItem, Info))
//				{
//					if (Info.AmmoType == TypeAmmo)
//					{
//						if (WeaponSlots[j].AdditionalInfo.Round <= 0)
//						{
//							WeaponWithCurrentAmmo.Slots.Add(WeaponSlots[j]);
//							Return = true;
//						}
//					}
//				}
//			}
//		}
//	}
//	return Return;
//}

bool UXiInventoryComponent::CheckCanTakeAmmo(EAmmoType TypeAmmo)
{
	bool result = false;
	if (AmmoSlots.IsValidIndex((int32)TypeAmmo))
	{
		if (AmmoSlots[(int32)TypeAmmo].Cout < AmmoSlots[(int32)TypeAmmo].MaxCout) // && AmmoSlots[(int32)TypeAmmo].AmmoType == TypeAmmo
			result = true;
	}
	return result;
}

bool UXiInventoryComponent::CheckCanTakeWeapon(EWeaponType TypeWeapon)
{
	bool result = false;
	if (WeaponSlots.IsValidIndex((int32)TypeWeapon))
	{
		if (WeaponSlots[(int32)TypeWeapon].NameItem.IsNone())
		{
			result = true;
		}
		else
		{ //���� ����� ������ ��� ����, �� �������� �������! // ����� �������� � �������� ������������� ���������� ������ ����� �� NewWeapon AdditionalInfo.Rounds
			/*FWeaponInfo Info;
			UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
			if (myGI)
			{
				if (myGI->GetWeaponInfoByName(WeaponSlots[(int32)TypeWeapon].NameItem, Info))
				{
					if (CheckCanTakeAmmo(Info.AmmoType))
					{
						AmmoSlotChangeValue(Info.AmmoType, Info.MaxRound);
					}
				}
			}*/
		}
	}
	return result;
}

bool UXiInventoryComponent::SwitchWeaponToInventory(FWeaponSlot NewWeapon, EWeaponType TypeWeapon, FDropItem& DropItemInfo)
{
	bool result = false;

	if (WeaponSlots.IsValidIndex((int32)TypeWeapon) && GetDropItemInfoFromInventory(TypeWeapon, DropItemInfo))
	{
		FWeaponInfo Info;
		UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
		if (myGI)
		{
			if (myGI->GetWeaponInfoByName(NewWeapon.NameItem, Info))
			{
				if (NewWeapon.AdditionalInfo.Round > Info.MaxRound || NewWeapon.AdditionalInfo.Round < 0)// ������ �� ������������� ����� ������
				{
					NewWeapon.AdditionalInfo.Round = Info.MaxRound;
				}
				NewWeapon.bIsEmpty = WeaponSlots[(int32)TypeWeapon].bIsEmpty;
			}
		}
		WeaponSlots[(int32)TypeWeapon] = NewWeapon;
		//����� �� ������ ��������! TO DO
		SwitchWeaponToIndex((int32)TypeWeapon, -1, NewWeapon.AdditionalInfo);



		OnUpdateWeaponSlots.Broadcast((int32)TypeWeapon, NewWeapon, Info); //������ ���� ������ �������
		result = true;
	}
	return result;
}

bool UXiInventoryComponent::TryGetWeaponToInventory(FWeaponSlot NewWeapon)
{
	FWeaponInfo Info;
	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		if (myGI->GetWeaponInfoByName(NewWeapon.NameItem, Info))
		{
			if (WeaponSlots.IsValidIndex((int32)Info.WeaponType))
			{
				if (NewWeapon.AdditionalInfo.Round > Info.MaxRound || NewWeapon.AdditionalInfo.Round < 0)// ������ �� ������������� ����� ������
				{
					NewWeapon.AdditionalInfo.Round = Info.MaxRound;
				}
				WeaponSlots[(int32)Info.WeaponType] = NewWeapon;
				OnUpdateWeaponSlots.Broadcast((int32)Info.WeaponType, NewWeapon, Info); //������ ���� ������ �������
				return true;
			}
			
		}
	}
	//{
	//	if (WeaponSlots.IsValidIndex(indexSlot))
	//	{
	//		WeaponSlots[indexSlot] = NewWeapon;
	//		OnUpdateWeaponSlots.Broadcast(indexSlot, NewWeapon); //������ ���� ������ �������
	//		return true;
	//	}
	//}
	return false;
}

bool UXiInventoryComponent::CheckCanTakeWeaponUpdate(EWeaponType TypeWeapon, FWeaponSlot NewWeaponSlotInfo)
{
	bool result = false;
	if (WeaponSlots.IsValidIndex((int32)TypeWeapon))
	{
		if (!WeaponSlots[(int32)TypeWeapon].NameItem.IsNone() && !NewWeaponSlotInfo.NameItem.IsNone())
		{
			if (NewWeaponSlotInfo.NameItem != WeaponSlots[(int32)TypeWeapon].NameItem)
			{
				result = true;
			}
		}
		else
		{ 
		}
	}
	return result;
}

bool UXiInventoryComponent::GetDropItemInfoFromInventory(EWeaponType TypeWeapon, FDropItem& DropItemInfo)
{
	bool result = false;

	FName DropItemName = GetWeaponNameBySlotIndex((int32)TypeWeapon);

	UXiGameInstance* myGI = Cast<UXiGameInstance>(GetWorld()->GetGameInstance());
	if (myGI)
	{
		result = myGI->GetDropItemInfoByWeaponName(DropItemName, DropItemInfo);
		if (WeaponSlots.IsValidIndex((int32)TypeWeapon))
		{
			DropItemInfo.WeaponInfo.AdditionalInfo = WeaponSlots[(int32)TypeWeapon].AdditionalInfo;
		}
	}

	//Debug
	//return true;
	return result;
}

FPortableSlot UXiInventoryComponent::GetPortableSlotByEnum(EPortableType TypePortable)
{
	return PortableSlots[(int32)TypePortable]; // return (PortableSlots*)[(int32)TypePortable];
}

bool UXiInventoryComponent::ChangePortableCoutByEnum(EPortableType TypePortable)
{
	if (PortableSlots[(int32)TypePortable].Cout > 0)
	{
		PortableSlots[(int32)TypePortable].Cout--;
		return true;
	}
	return false;
}

#pragma optimize ("", on)