// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "../Character/XiHealthComponent.h"
#include "XiCharacterHealthComponent.generated.h"

/**
 * 
 */
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnShieldChange, float, Shield, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldDestroyed, bool, ShieldDestroyed);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnArmorChange, float, Armor, float, Damage);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnStaminaChange, float, Stamina, float, Variation);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnExhaustedChange, bool, Exhausted);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnShieldExist, bool, bShieldExist);

UCLASS()
class XI_API UXiCharacterHealthComponent : public UXiHealthComponent
{
	GENERATED_BODY()
	
public:
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldChange OnShieldChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldDestroyed OnShieldDestroyed;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Armor")
		FOnArmorChange OnArmorChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnStaminaChange OnStaminaChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		FOnExhaustedChange OnExhaustedChange;
	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = "Shield")
		FOnShieldExist OnShieldExist;


	FTimerHandle TimerHandle_CollDownShieldTimer;
	FTimerHandle TimerHandle_ShieldRecoveryRateTimer;

	FTimerHandle TimerHandle_StartChangeStaminaTimer;
	FTimerHandle TimerHandle_CollDownStaminaTimer;
	FTimerHandle TimerHandle_StaminaRecoveryRateTimer;
protected:

	float Shield = 100.0f;
	float Armor = 100.0f;
	float Stamina = 100.0f;

	bool ShieldExist = false;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Armor", meta = (ClampMin = "0.0", ClampMax = "1.0"))
		float ArmorCoefDivision = 0.5f; //������ ������ ������� � ������ ����
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldCoefDamage = CoefDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float CoolDownShieldRecoverTime = 5.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shield")
		float ShieldRecoverRate = 0.1f;
	bool ShieldDestroyed = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float CoolDownStaminaRecoverTime = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaRecoverRate = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaDeclineValue = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina")
		float StaminaDeclineRateValue = 0.1f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stamina", meta = (ClampMin = "0.0", ClampMax = "100.0"))
		float StaminaWalkingValue = 15.0f; //�� ���� �� 100. ����������� ������������� �������.
	bool Exhausted = false;

	void ChangeHealthValue(float ChangeValue) override;

	float GetCurrentShield();

	void SetCurrentShield(float NewShield);


	void ChangeShieldValue(float ChangeValue);

	void CoolDownShieldEnd();

	void RecoveryShield();

	bool GetShieldDestroyed();

	void LaunchShield();
	UFUNCTION(BlueprintCallable, Category = "Shield")
	bool GetShieldExist();

	void SetShieldExist(bool NewShieldExist);
	void ClearShieldTimer();
	
	UFUNCTION(BlueprintCallable, Category = "Armor")
	float GetCurrentArmor();

	UFUNCTION(BlueprintCallable, Category = "Armor")
	void SetCurrentArmor(float NewArmor);

	UFUNCTION(BlueprintCallable, Category = "Armor")
	void ChangeArmorValue(float ChangeValue);

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		float GetCurrentStamina();

	UFUNCTION(BlueprintCallable, Category = "Stamina")
		void SetCurrentStamina(float NewStamina);

	void StartChangeStamina();
	void StopChangeStamina();

	void ChangeStaminaValue();

	void CoolDownStaminaEnd();

	void RecoveryStamina();
};
