// Fill out your copyright notice in the Description page of Project Settings.


#include "../Character/XiHealthComponent.h"

// Sets default values for this component's properties
UXiHealthComponent::UXiHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UXiHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UXiHealthComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UXiHealthComponent::GetCurrentHealth()
{
	return Health;
}

void UXiHealthComponent::SetCurrentHealth(float NewHealth)
{
	if (NewHealth > 100.0f)
	{
		Health = 100.0f;
	}
	else
	{
	Health = NewHealth;
	}
}

void UXiHealthComponent::ChangeHealthValue(float ChangeValue)
{
	if (!Immortal || ChangeValue > 0.0f)
	{
		if (ChangeValue < 0.0f)
		{
			ChangeValue = ChangeValue * CoefDamage;
		}

		Health += ChangeValue;

		if (Health > 100.0f)
		{
			Health = 100.0f;
		}
		else
		{
			if (Health < 0.0f)
			{
				OnDead.Broadcast();
			}
		}

		OnHealthChange.Broadcast(Health, ChangeValue);
	}
}

void UXiHealthComponent::ChangeImmortal(bool NewStateImmortal)
{
	Immortal = NewStateImmortal;
	OnImmortal.Broadcast(Immortal);
}

